const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

let persons = require('./persons.json');
let wow = require('./links.json');


// GET
app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/persons', (req, res) => {
    res.send(persons);
});

app.get('/api/wow', (req, res) => {
    res.send(wow);
});



// POST
app.post('/api/courses', (req, res) => {
    const { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const course = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(course);
    res.send(course);
});

// PUT
app.put('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the giving ID was not found');

    const schema = {
        name: Joi.string().min(3).required()
    };

    const { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    course.name = req.body.name;
    res.send(course);
});

// Delete
app.delete('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the giving ID was not found');

    const index = courses.indexOf(course);
    courses.splice(index, 1);
    res.send(course);
});

//PORT
const port = process.env.PORT || 8081;
app.listen(port, () => console.log(`Listining on port ${port}`));


//Functions
function validateCourse(course) {
    const schema = {
        name: Joi.string().min(3).required()
    };

    return Joi.validate(course, schema);
}