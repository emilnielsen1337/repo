-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `nodeDB`;
CREATE DATABASE `nodeDB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nodeDB`;

DROP TABLE IF EXISTS `elever`;
CREATE TABLE `elever` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navn` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_danish_ci NOT NULL,
  `email` varchar(255) NOT NULL,
  `hold` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hold` (`hold`),
  CONSTRAINT `elever_ibfk_1` FOREIGN KEY (`hold`) REFERENCES `hold` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `elever` (`id`, `navn`, `email`, `hold`) VALUES
(4,	'tobias',	'tobias@tobias.com',	3),
(5,	'mathias',	'mathias@mathias.com',	3),
(7,	'TEST',	'TEST',	3);

DROP VIEW IF EXISTS `elever_full`;
CREATE TABLE `elever_full` (`Elev navn` varchar(255), `Email` varchar(255), `Hold navn` varchar(255), `Fag navn` varchar(255));


DROP TABLE IF EXISTS `fag`;
CREATE TABLE `fag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navn` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `fag` (`id`, `navn`) VALUES
(1,	'GUI'),
(2,	'Database'),
(3,	'Clientside'),
(4,	'Serverside');

DROP TABLE IF EXISTS `hold`;
CREATE TABLE `hold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navn` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `hold` (`id`, `navn`) VALUES
(1,	'h1'),
(2,	'h2'),
(3,	'h3');

DROP TABLE IF EXISTS `hold_fag`;
CREATE TABLE `hold_fag` (
  `hold_id` int(11) NOT NULL,
  `fag_id` int(11) NOT NULL,
  KEY `hold_id` (`hold_id`),
  KEY `fag_id` (`fag_id`),
  CONSTRAINT `hold_fag_ibfk_1` FOREIGN KEY (`hold_id`) REFERENCES `hold` (`id`),
  CONSTRAINT `hold_fag_ibfk_2` FOREIGN KEY (`fag_id`) REFERENCES `fag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `hold_fag` (`hold_id`, `fag_id`) VALUES
(1,	1),
(1,	2),
(2,	3),
(3,	4);

DROP TABLE IF EXISTS `elever_full`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `elever_full` AS select `elever`.`navn` AS `Elev navn`,`elever`.`email` AS `Email`,`hold`.`navn` AS `Hold navn`,`fag`.`navn` AS `Fag navn` from (((`elever` join `hold` on(`elever`.`hold` = `hold`.`id`)) join `hold_fag` on(`hold`.`id` = `hold_fag`.`hold_id`)) join `fag` on(`fag`.`id` = `hold_fag`.`fag_id`));

-- 2019-11-22 09:20:09
