const express = require('express');
const app = express();
const cors = require('cors');
let mysql = require('mysql');

app.use(express.json());
app.use(cors());

// MySQL Connection
var con = mysql.createConnection({
    host: "localhost",
    user: "node",
    password: "node",
    database: "nodeDB"
});

// GET
app.get('/api/elever', (req, res) => {
    let sql = "SELECT * FROM elever";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/fag', (req, res) => {
    let sql = "SELECT * FROM fag";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/hold', (req, res) => {
    let sql = "SELECT * FROM hold";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/hold_fag', (req, res) => {
    let sql = "SELECT * FROM elever_full";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

// POST
app.post('/api/elever', (req, res) => {
    var elev = {
        navn: req.body.name,
        email: req.body.email,
        hold: req.body.hold
    };

    var value = [elev.hold];
    con.query("SELECT * FROM hold WHERE navn = ?", value, function (err, result) { // finder hold id
        if (err) throw err;
        var hid = result[0].id; // gemmer hold id som "hid"

        var values = [
            [elev.navn, elev.email, hid]
        ];

        con.query("INSERT INTO elever (navn, email, hold) VALUES (?)", values, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    });
});

// PUT
app.put('/api/elever/:id', (req, res) => {
    var elev = {
        name: req.body.name,
        email: req.body.email,
        hold: req.body.hold
    };

    var value = [elev.hold];

    con.query("SELECT * FROM hold WHERE navn = ?", value, function (err, result) { // finder hold id

        if (err) throw err;
        var hid = result[0].id; // gemmer hold id som "hid"

        con.query("UPDATE elever SET hold = '" + hid + "' WHERE id = " + req.params.id, function (err, result) {
            if (err) throw err;
            console.log("hold edited: " + result.affectedRows + " - record(s) updated");

        });

        con.query("UPDATE elever SET navn = '" + elev.name + "' WHERE id = " + req.params.id, function (err, result) {
            if (err) throw err;
            console.log("navn edited: " + result.affectedRows + " - record(s) updated");
        });

        con.query("UPDATE elever SET email = '" + elev.email + "' WHERE id = " + req.params.id, function (err, result) {
            if (err) throw err;
            console.log("email edited: " + result.affectedRows + " - record(s) updated");

        });

        res.send(result);
    });
});

// Delete
app.delete('/api/elever/:id', (req, res) => {
    const id = req.params.id;
    con.query("DELETE FROM elever WHERE id = ?", id, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

//PORT
const port = process.env.PORT || 8081;
app.listen(port, () => console.log(`Listening on port ${port}`));