import showElever from './src/components/showElever.vue';
import showFag from './src/components/showFag.vue';
import showHold from './src/components/showHold.vue';
import showHoldFag from './src/components/showHoldFag.vue'
import add from './src/components/add.vue'

export default [
    { path: '/', component: add},
    { path: '/elever', component: showElever },
    { path: '/fag', component: showFag },
    { path: '/hold', component: showHold },
    { path: '/holdfag', component: showHoldFag }
]