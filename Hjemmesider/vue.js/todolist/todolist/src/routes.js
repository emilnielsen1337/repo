import showJSON from './components/showJSON.vue';
import showTodo from './components/showTodo.vue';

export default [
    {path: '/json', component: showJSON },
    {path: '/todo:id', component: showTodo }
]