<?php
   require_once "server.php";
   session_start();
   if($_SERVER["REQUEST_METHOD"] == "POST") 
   {
      $username = mysqli_real_escape_string($conn, $_POST['username']);
      $password = mysqli_real_escape_string($conn, $_POST['password']); 
   
      $checkhash = "SELECT password FROM users WHERE username = '$username'";
      $result = $conn->query($checkhash);
      $row = $result->fetch_assoc();

      if(empty($username) ||  empty($password))
      {
        // header("location: ../login.php");
         exit();
      }

      if(password_verify($password, $row["Password"]))
      {
         $_SESSION['login'] = $username;

         $sql = "select admin from users where username='$username'";
         $result = $conn->query($sql);
         $row = $result->fetch_assoc();
         $IsAdmin = $row['admin'];

         if($IsAdmin === "yes")
         {
            $_SESSION['admin'] = true;
         }
         else
         {
            $_SESSION['admin'] = false;
         }

         header("location: ../shop.php");
      } 
      else 
      {
         $_SESSION['message'] = "Your Login Name or Password is invalid";
         $_SESSION['LastPage'] = "login.php";
         
         header("location: ../status.php");
         exit();
      }
    }
?>