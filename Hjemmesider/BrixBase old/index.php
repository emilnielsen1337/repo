<?php
  require '../php/server.php';
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php 
  if($_SERVER['REQUEST_METHOD'] == 'POST') 
  { 
    if (isset($_POST['login'])) // User login
    {
      require '../php/login.php';
    }
  }
?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>Login - BrixBase</title>
</head>
<body>

    <div>
    <form id="form" action="Scripts/loginscript.php" method="POST"> <!-- SET!!! -->
        <img src="pics/Brixbase.png" alt="Logo" id="logo"><br><br>

        <div>
          <label class="label">Username:</label>

          <input type="text" class="input" name="username">
        </div>

        <div>
          <br><label class="label">Password: </label>

          <input type="password" class="input" name="password"><br><br>

          <button class="button" name="login" type="submit">Login</button>
        </div>
      </form>
</body>
</html>