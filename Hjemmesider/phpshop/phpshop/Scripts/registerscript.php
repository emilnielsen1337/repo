<?php
require_once "Server.php";

if(!isset($_SESSION['login_user']))
{
  header("location: ../shop.php");
  exit();
}

$username = $_POST['username'];
$password = $_POST['password'];
$password2 = $_POST['password2'];
$date = date("d/m/y");
$time = time();


if ($password === $password2) 
{
    $hashpass = password_hash($password, PASSWORD_DEFAULT);

    $sql = "INSERT INTO Users (UserName, Password, SigUpDate, Admin) VALUES ('$username', '$hashpass', NOW(), 'no')";
    if ($conn->query($sql) === TRUE) 
    {
        $_SESSION['message'] = "User created successfully!";
        $_SESSION['LastPage'] = "login.php";
    } 
    else 
    {
        $_SESSION['message'] = "Error: " . $sql . "<br>" . $conn->error;
        $_SESSION['LastPage'] = "register.php";  
    }
} 
else 
{
    $_SESSION['message'] = "Not the same password";
    $_SESSION['LastPage'] = "register.php";
}
$conn->close();
header("location: ../status.php");
exit();
?>

