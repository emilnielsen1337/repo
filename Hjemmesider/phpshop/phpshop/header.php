<?php
 session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="Stylesheets/sidebarnav.css">
    <link rel="stylesheet" href="Stylesheets/Navbarsheet.css">
    <link rel="stylesheet" href="Stylesheets/stylesheet.css">
    <title>PHPShop.dk</title>
</head>
<body>
<div>
    <ul>
        <img src="/Pictures/LogoMini.png" alt="logo" class="logo">
        <li><a href="home.php">Home</a></li>
        <li><a style="float:right" href="shop.php" id="liShop">Profile</a></li>
        <?php
            if(isset($_SESSION['login_user']))
            {
                echo '<li style="float:right"><a class="active" href="/Scripts/logoutscript.php">Logout</a></li>';
                
                if($_SESSION['isadmin'] === true)
                {
                    echo '<li style="float:right"><a class="active" href="register.php">Create</a></li>';
                }
            }
            else
            {
                echo '<li style="float:right"><a class="active" href="login.php">Login</a></li>';
            }
        ?>

        
    </ul>
</div>