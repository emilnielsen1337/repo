import Vue from 'vue';
import VueRouter from 'vue-router';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'homemage',
    meta: {
      title: 'Homepage'
    },
    component: () => import(/* webpackChunkName: "Home" */ '../views/homepage.vue')
  },
  {
    path: '/wowclassic',
    name: 'wowclassic',
    meta: {
      title: 'WoW Classic'
    },
    component: () => import(/* webpackChunkName: "WoW Classic" */ '../views/wow-classic.vue'),
  },
  {
    path: '/lol',
    name: 'leagueoflegends',
    meta: {
      title: 'League of Legends'
    },
    component: () => import(/* webpackChunkName: "League of Legends" */ '../views/leagueoflegends.vue'),
  },
  {
    path: '/mab',
    name: 'mountandblade',
    meta: {
      title: 'M&B'
    },
    component: () => import(/* webpackChunkName: "Mount and Blade" */ '../views/mountandblade.vue'),
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
})



export default router
