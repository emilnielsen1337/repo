let Pi = 3.14159265359;
let currentMode;

let mode = {
    CIRCLE: 'circle',
    SQUARE: 'square',
    TRAPEZE: 'trapeze',
    CONE: 'cone',
    TRIANGLE: 'triangle',
    PENTAGON: 'pentagon',
    HEXAGON: 'hexagon',
    OCTAGON: 'octagon',
    NONAGON: 'nonagon',
    DECAGON: 'decagon'
}
//#region Calc
function Calc() {
    switch (currentMode) {
        case mode.CIRCLE:
            areaCircle();
            break;
        case mode.SQUARE:
            areaSquare();
            break;
        case mode.TRAPEZE:
            areaTrapeze();
            break;
        case mode.CONE:
            areaCone();
            break;
        case mode.TRIANGLE:
            areaTriangle();
            break;
        case mode.PENTAGON:
            areaPentagon();
            break;
        case mode.HEXAGON:
            areaHexagon();
            break;
        case mode.OCTAGON:
            areaOctagon();
            break;
        case mode.NONAGON:
            areaNonagon();
            break;
        case mode.DECAGON:
            areaDecagon();
            break;
    }
}
//#endregion

//#region Area Calculations
function areaCircle() {
    let val1 = document.getElementById("val1").value;
    document.getElementById("display4").value = val1 * val1 * Pi;
}

function areaSquare() {
    let val1 = document.getElementById("val1").value;
    let result;
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");

    canvas.width = val1;
    canvas.height = val1;
    ctx.beginPath();
    ctx.rect(0, 0, val1, val1);
    ctx.stroke();
    result = val1 * val1;

    document.getElementById("display4").value = result;
}

function areaTrapeze() {
    let b1 = document.getElementById("val1").value;
    let b2 = document.getElementById("val2").value;
    let h = document.getElementById("val3").value;
    document.getElementById("display4").value = (b1 + b2) * h / 2;
}

function areaCone() {
    let r = document.getElementById("val1").value;
    let h = document.getElementById("val2").value;
    let s = Math.sqrt(r * r + h * h);
    document.getElementById("display4").value = Pi * r * (r + s);
}

function areaTriangle() {
    let s1 = document.getElementById("val1").value;
    let s2 = document.getElementById("val2").value;
    let s3 = document.getElementById("val3").value;
    let s = (s1 + s2 + s3) / 2;
    document.getElementById("display4").value = Math.sqrt(s * ((s - s1) * (s - s2) * (s - s3)));
}

function areaPentagon() {
    let s = document.getElementById("val1").value;
    let len = document.getElementById("val2").value;
    document.getElementById("display4").value = (5 / 2) * s * len;
}

function areaHexagon() {
    let s = document.getElementById("val1").value;
    document.getElementById("display4").value = ((3 * Math.sqrt(3) *  (s * s)) / 2);
}

function areaOctagon() {
    let s = document.getElementById("val1").value;
    document.getElementById("display4").value = (2 * (1 + Math.sqrt(2)) * s * s);
}

function areaNonagon() {
    let s = document.getElementById("val1").value;
    document.getElementById("display4").value = 6.1818 * (s * s);
}

function areaDecagon() {
    let r = document.getElementById("val1").value;
    document.getElementById("display4").value = (5 * Math.pow(r, 2) * (3 - Math.sqrt(5)) * (Math.sqrt(5) + ((2 * Math.sqrt(5))))/ 4);
}
//#endregion

//#region Button functions
function Circle() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Radius';

        val2.style.display = "none";
        val3.style.display = "none";
    }
    currentMode = mode.CIRCLE;
}

function Square() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Side Length';

        val2.style.display = "none";
        val3.style.display = "none";
    }
    currentMode = mode.SQUARE;
}

function Trapeze() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";
        val2.style.display = "inline";
        val3.style.display = "inline";

        val1.placeholder = 'Base1';
        val2.placeholder = 'Base2';
        val3.placeholder = 'Height';
    }
    currentMode = mode.TRAPEZE;
}

function Cone() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";
        val2.style.display = "inline";

        val1.placeholder = 'Radius';
        val2.placeholder = 'Height';


        val3.style.display = "none";
    }
    currentMode = mode.CONE;
}

function Triangle() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";
        val2.style.display = "inline";
        val3.style.display = "inline";

        val1.placeholder = 'Side 1';
        val2.placeholder = 'Side 2';
        val3.placeholder = 'Side 3';
    }
    currentMode = mode.TRIANGLE;
}

function Pentagon() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";
        val2.style.display = "inline";

        val1.placeholder = 'Side';
        val2.placeholder = 'Length';


        val3.style.display = "none";

    }
    currentMode = mode.PENTAGON;
}

function Hexagon() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Side';

        val2.style.display = "none";
        val3.style.display = "none";

    }
    currentMode = mode.HEXAGON;
}

function Octagon() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Side';

        val2.style.display = "none";
        val3.style.display = "none";

    }
    currentMode = mode.OCTAGON;
}

function Nonagon() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Side';

        val2.style.display = "none";
        val3.style.display = "none";

    }
    currentMode = mode.NONAGON;
}

function Decagon() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    refresh();

    if (val1.style.display === "none") {
        val1.style.display = "inline";

        val1.placeholder = 'Radius';

        val2.style.display = "none";
        val3.style.display = "none";

    }
    currentMode = mode.DECAGON;
}
//#endregion

function refresh() {
    let val1 = document.getElementById("val1");
    let val2 = document.getElementById("val2");
    let val3 = document.getElementById("val3");

    val1.value = '';
    val2.value = '';
    val3.value = '';

    val1.style.display = 'none';
    val2.style.display = 'none';
    val3.style.display = 'none';
}