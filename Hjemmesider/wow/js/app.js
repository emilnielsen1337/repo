data = {
    "classes": [{
        "warrior": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-warrior-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-warriors-best-addons-and-macros"},

        "paladin": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-paladin-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-paladins-best-addons-and-macros"},

        "rogue": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-rogue-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-rogues-best-addons-and-macros"},

        "hunter": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-hunter-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-hunters-best-addons-and-macros"},

        "priest": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-warrior-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-priests-best-addons-and-macros"},

        "warlock": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-warlock-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-warlocks-best-addons-and-macros"},

        "druid": {
            "lvling": "https://www.icy-veins.com/wow-classic/classic-druid-leveling-guide",
            "addons": "https://www.icy-veins.com/wow-classic/classic-guide-to-druids-best-addons-and-macros"}
    }],

    "gathering": [{
        "mining": {"link": "https://www.icy-veins.com/wow-classic/classic-mining-profession-and-leveling-guide"},

        "herbalism": {"link": "https://www.icy-veins.com/wow-classic/classic-herbalism-profession-and-leveling-guide"},

        "skinning": {"link": "https://www.icy-veins.com/wow-classic/classic-skinning-profession-guide"},

        "fishing": {"link": "https://www.icy-veins.com/wow-classic/classic-fishing-profession-guide"},
    }],

    "crafting": [{
        "alchemy": {"link": "https://www.icy-veins.com/wow-classic/classic-alchemy-profession-and-leveling-guide"},

        "blacksmithing": {"link": "https://www.icy-veins.com/wow-classic/classic-blacksmithing-profession-guide"},

        "enchanting": {"link": "https://www.icy-veins.com/wow-classic/classic-enchanting-profession-and-leveling-guide"},

        "engineering": {"link": "https://www.icy-veins.com/wow-classic/classic-engineering-profession-guide"},

        "leatherworking": {"link": "https://www.icy-veins.com/wow-classic/classic-leatherworking-profession-guide"},

        "tailoring": {"link": "https://www.icy-veins.com/wow-classic/classic-tailoring-profession-and-leveling-guide"},

        "cooking": {"link": "https://www.icy-veins.com/wow-classic/classic-cooking-profession-guide"},

        "first aid": {"link": "https://www.icy-veins.com/wow-classic/classic-first-aid-profession-and-leveling-guide"}
    }],

    "talent": {"link": "http://www.classicwowtalents.appspot.com"},

    "bis": {"link": "http://www.wowclassicbis.com/"},

    "buygold": {"link": "https://www.igvault.com/WOW-Classic-Gold/Ashbringer-EU/Alliance.html"}
}








function frame(location) {
    $('#iframe').attr('src', location);
}