//import 'requirejs';
const requirejs = require('requirejs');
const assert = requirejs('assert');

let tal1 = 4;
let tal2 = 1;

console.log("tal 1 = 1");
console.log("tal 2 = 2");
console.log("Undefined = true");
console.log("  ");

console.log("TestAdd");
console.log("assert.equal(3, tal1 + tal2)");
facitAdd = assert.equal(3, tal1 + tal2);
console.log(facitAdd);
console.log("  ");

console.log("TestSub");
console.log("assert.equal(1, tal1 - tal2)");
facitSub = assert.equal(1, tal1 - tal2);
console.log(facitSub);
console.log("  ");

console.log("TestDiv");
console.log("assert.equal(2, tal1 / tal2)");
facitDiv = assert.equal(2, tal1 / tal2);
console.log(facitDiv);
console.log("  ");

console.log("TestMul");
console.log("assert.equal(2, tal1 * tal2)");
facitMul = assert.equal(2, tal1 * tal2);
console.log(facitMul);
console.log("  ");

console.log("TestRem");
console.log("assert.equal(1, 11 % 2)");
facitRem = assert.equal(1, 11 % 2);
console.log(facitRem);
