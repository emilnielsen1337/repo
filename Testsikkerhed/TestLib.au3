;; Testlib
;;
;; AutoIT functions for scripted software testing of Windows applications
;;

#include-once

;; Options
Opt("MustDeclareVars", 1)

Func my_log($str)
   ConsoleWrite($str & @CRLF)
EndFunc


Func my_wait($win)
   local $res = WinWait($win, "", 10)

   if $res=0 Then
	  my_fail("Window: " & $win & " did not show up")
	  Exit
   Else
	  my_log("Window: " & $win & " did show up")
	  sleep(3000)
   EndIf

EndFunc

Func my_fail($str)
     my_log("TEST FAILED:" & $str)
  EndFunc

Func my_setup()
   my_log("Script Start")
EndFunc

Func my_teardown()
   my_log("Script Done: SUCCES")
EndFunc
