#include "TestLib.au3"

my_setup()

Opt("SendKeyDownDelay",20)

Run("calc.exe")

WinWaitActive("[CLASS:ApplicationFrameWindow]", "", 10)
Sleep(1000)

If WinExists("[CLASS:ApplicationFrameWindow]") = 0 Then Exit

ControlSend("[CLASS:ApplicationFrameWindow]", "","Windows.UI.Core.CoreWindow1", '123456789')
Sleep(500)

ControlSend("[CLASS:ApplicationFrameWindow]", "","Windows.UI.Core.CoreWindow1", '+',1) ;1 means send raw key
Sleep(500)

ControlSend("[CLASS:ApplicationFrameWindow]", "","Windows.UI.Core.CoreWindow1", '123456789')
Sleep(500)

Send("{ENTER}")

Sleep(500)

WinActivate("[CLASS:ApplicationFrameWindow]")
Send("^c")


MsgBox(0,"","Result: " & ClipGet())
my_teardown()

