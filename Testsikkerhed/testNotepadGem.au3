#include "TestLib.au3"

my_setup()

local $filename = "Helloworld.txt"
local $fullname = @MyDocumentsDir & "\" & $filename

; FileDelete($fullname) ;; kan udkommenteres

run ("notepad.exe")
local $notepad_win = "Unavngivet - Notesblok"
my_wait($notepad_win)

send("Hej Notepad")
sleep(2000)

local $notepad_win = "*Unavngivet - Notesblok"
WinClose($notepad_win)

local $save_win = "Notesblok"
my_wait($save_win)

sleep(2000)

ControlClick($save_win, "", "&Gem")

local $save_as_win = "Gem som"
my_wait($save_as_win)

send($filename & "{ENTER}")

if FileExists($fullname) then
   my_log($fullname & " exists")
   local $confirm_win = "Bekræft Gem som"
   my_wait($confirm_win)
   ControlClick($confirm_win, "", "&Ja")
EndIf

WinClose($notepad_win)

my_teardown()

