init:
	.include "m168def.inc"
	;.device ATmega168
	.org 0x34
	rjmp main

main:
	ldi R16, 0xFF
	out DDRB, R16

loop:
	ldi R17, 128
	ldi R18, 150
	ldi R19, 41

	sbi PINB, 5
	rcall delay

	rjmp loop

delay:
	dec R17
	brne delay

	dec R18
	brne delay
	-
	dec R19
	brne delay
	ret