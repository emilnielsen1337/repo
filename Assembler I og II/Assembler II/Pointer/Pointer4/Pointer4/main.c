#include "Uart.h"

int main(void)
{
	RS232Init();;
	
	char cData[] = {"Pointers are easy as"};
	char *pcData = NULL;

	int iData[] = {1,2,3};
	int *piData = NULL;
	
	printf("cData har %d bytesize elementer\n", sizeof(cData));
	printf("iData har %d bytesize elementer\n", sizeof(iData));
	
	// cData indeholder adressen p� f�rste element i arrayet.
	pcData = cData;
	//Og nu peger pcData p� samme element
	
	//Forl�kke, af char array
	printf("cData \n");
	for(int i = 0; i < sizeof(cData); i++)
	{
		printf("&cData[%d] = %d, cData[%d] = %c\n", i, &cData[i],  i, *pcData++);
	}

	// pointer til test af int arrayet
	int *ip = iData;

	//forl�kke, af int array
	printf("piData \n");
	for(int i = 0; i < sizeof(iData) / sizeof(int); i++)
	{
		piData = &iData[i];
		
		printf("&iData[%d] = 0x%x, iData[%d] = %d\n", i, piData,  i, *ip++);
	}

	exit(0);
}
