#include "Uart.h"

int main(void)
{
	
	RS232Init();
	
	int tal1 = 17;
	int tal2 = 25;
	int resultat = 0;
	
	
	// Kalder Plus funktionen og kopierer automatisk de to variabler over p� stakken.
	resultat = Plus(tal1, tal2);
	
	// viser at tal1 og tal2 stadig er de samme;
	printf("tal 1 = %d, tal2 = %d. Resultatet af Plus = %d\n", tal1, tal2, resultat);
	
	// Kalder pPlus funktionen og kopierer automatisk adresserne p� de to variabler
	// over p� stakken.
	resultat = ptrPlus(&tal1, &tal2);
	
	// viser at tal1 og tal2 blev modificeret i ptrPlus funktionen.
	printf("tal 1 = %d, tal2 = %d. Resultatet af pPlus = %d\n", tal1, tal2, resultat);
	
	exit(0);
	
}

int Plus(int t1, int t2)
{
	// Bytter om p� de to variabler (Kopierne)
	int tmp = t1;
	t1 = t2;
	t1 = tmp;
	
	tmp = t1 + t2;
	
	return tmp;
}

int ptrPlus(int *t1, int *t2)
{
	// Bytter om p� de to variabler der peges til (Originalerne!)
	int tmp = *t1;
	*t1 = *t2;
	*t2 = tmp;
	
	tmp = *t1 + *t2;
	
	return tmp;
}
