        #include "Uart.h"
        #include "stdio.h"
		
		// Opretter en datastruktur som kan indeholde 3 simple typer
        struct Point3D
        {
	        int X;
	        int Y;
	        int Z;
        };

		//void doStuff(struct Point3D punkt);
		void ptrDoStuff(struct Point3D *punkt);

        int main(void)
        {
	        RS232Init();

	        // Opretter en variabel af typen Point3D
	        struct Point3D punkt1;

	        // Tilskriver de enkelte variabler en af gangen.
	        punkt1.X = 10;
	        punkt1.Y = 2;
	        punkt1.Z = 0;
	        
	        // her er et eksempel p� hvordan du ogs� kan oprette
	        // og tilskrive en struct med det samme
	        // struct Point3D newPoint = {20, 7, 10};
	        // R�kkef�lgen er            X, Y,  Z  -> Alts� som de er listet i structen.
	        
	        //udskriver v�rdier inden vi kalder doStuff
	        printf("X = %d, Y = %d, Z = %d\n",punkt1.X, punkt1.Y, punkt1.Z);

	        //Overf�rer en lokal kopi af en point3D
	        //doStuff(punkt1);
			ptrDoStuff(&punkt1);
	        
	        //udskriver v�rdier efter vi har kaldt doStuff
	        printf("X = %d, Y = %d, Z = %d\n",punkt1.X, punkt1.Y, punkt1.Z);
	        
	        while(1);
        }

        void doStuff(struct Point3D punkt)
        {
	        // Dette er en lokal kopi, s� vores �ndringer g�lder kun herinde...
	        printf("�ndrer i den lokale kopi\n");
	        punkt.X--;
	        punkt.Y++;
	        punkt.Z++;
        }
		
		void ptrDoStuff(struct Point3D *punkt)
		{
			printf("�ndre i originalen");
			punkt->X--;
			punkt->Y++;
			punkt->Z++;
		}
