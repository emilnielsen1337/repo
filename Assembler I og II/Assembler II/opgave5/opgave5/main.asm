.include "m168def.inc"

	.def	LED=r16	; reg 16 kaldes "LED".
	.def	KEY=r17	; reg 17 kaldes "KEY".
	.org	0x0000	; "rjmp START" placeres p� adresse
					; 0000h (reset-adressen).
	rjmp	START	; spring til labellen "START".
	.org	0x0034	; programmet placeres fra adresse
					; 0034h.
START:	
	ser	LED			; "LED" f�r v�rdien 11111111b.
	clr	KEY			; "KEY" f�r v�rdien 00000000b.
	out	DDRD,KEY	; PORTD bliver til input.
	out	DDRB,LED	; PORTB bliver til output.
SCAN:	
	sbis	PIND,3	; Hvis bit2 i Port D = 1 spring over
					; linien "rjmp LEDOFF".
	rjmp	LEDOFF	; spring til labellen "LEDOFF".
	sbi	PORTB, 3       ; bit nummer "4" i PORTB 
					; s�ttes til 1.
	rjmp	SCAN	; spring til labellen "scan".
LEDOFF:	
	cbi	PORTB,3		; bit nummer "4" i PORTB 
					; nulstilles.
	rjmp	SCAN	; spring til labellen "scan".
