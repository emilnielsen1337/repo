#include "Uart.h"

int main(void)
{
	RS232Init();               // Init UART
	
	int *i_ptr;		// int pointer
	char *c_ptr;	// char pointer
	int tal = 0XCC33;
	
	i_ptr = &tal;	// S�tter ipointer = adressen p� tal
	c_ptr = i_ptr;	// c_ptr peger nu p� samme adr som i_ptr.
	
	// Udskriver informationer omkring i_prt.
	//printf("i_ptr");
	//printf("i_ptr adr=%x, adr i_ptr peger p�=%x, indhold af det i_ptr peger p�=%x\n", &i_ptr, i_ptr, *i_ptr);
	//printf("c_ptr");
	//printf("c_ptr adr=%x, adr c_ptr peger p�=%x, indhold af det c_ptr peger p�=%x\n", &c_ptr, c_ptr, *c_ptr);
	printf("c_ptr adr=%x, adr c_ptr peger p�=%x, indhold af det c_ptr peger p�=%x\n", &c_ptr, c_ptr, *c_ptr);
	

	exit(0);
}
