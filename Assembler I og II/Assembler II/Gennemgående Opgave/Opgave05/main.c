#include "Arduino.h"
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include "UART.h"

int main(void)
{
	RS232Init();

	uint16_t adc_read(uint8_t adc_input) 	// L�s en enkelt sample
	{
		ADMUX = adc_input;		// V�lg input
		ADMUX |= (0<<REFS1) | (1<<REFS0);
		ADCSRA |= 0x40;
		while ((ADCSRA & 0x10)==0);   // Vent til konvertering er afsluttet
		ADCSRA |= 0x10;
		return  ADCW;			//Returner m�leresultat
	}



	void adc_init()       //Ops�t registre, if�lge manual side 255
	{
		ADCSRA = (1<<ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	}


	int main(void)
	{
		adc_init();
		RS232Init();
		double A;
		char p[20];

		while (1)
		{
			A = adc_read(0);
			sprintf(p, "%04d\n\r", (int)A);
			printf(p);
			_delay_ms(200);
		}
	}

	
}
