/*****************************************************
Project 	:Opgave01
Version 	:1.0
Date    	: 02.03.2018
Author 	: Lars Thise Pedersen
Chip type      : ATmega168
Program type   : Application
AVR Core Clock frequency: 16,000000 MHz
*****************************************************/

#define F_CPU 16000000UL  // 16 MHz krystal på boarded !!

#include <avr/io.h> 	// avr header file for IO ports
#include <util/delay.h>	// functions for delay loop

int main(void)
{
	DDRB  = 0xFF;		// Alle bit i Port B sættes som udgang
	PORTB = 0xFF;		// Sæt “1” på alle bit I Port B

	while(1)
	{
		_delay_ms(500);		// 100ms Pause.. !!!  Max 200
		PORTB = ~PORTB;  	// Inverter alle 8 bit I Port B
		_delay_ms(500);
	}
	return 1;
}
