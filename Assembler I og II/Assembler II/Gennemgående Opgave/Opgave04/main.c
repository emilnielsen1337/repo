#define F_CPU 16000000UL  // 16 MHz
#include <avr/io.h> // avr header file for IO ports
#include <stdio.h>
#include <util/delay.h>

static int uart_putchar(char c, FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL,
_FDEV_SETUP_WRITE);

static int uart_putchar(char c, FILE *stream)
{
	if (c == '\n')
	uart_putchar('\r', stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

void init_uart() {
	UCSR0A=0x00;
	UCSR0B=0x18; 	 // Transmit  & Receive
	UCSR0C=0x06;  	// 8 databit
	UBRR0H=0x00;
	UBRR0L=0x67;  	// 9600 Baud
}

int main(void){
	init_uart();
	stdout = &mystdout;
	int x =0;
	while(1){
		_delay_ms(200);
		printf(" Hello World !   %i \n",x);
		_delay_ms(200);
		UDR0 = 'Z';
	}
	return 1;
}
