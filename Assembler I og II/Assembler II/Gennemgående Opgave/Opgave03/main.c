/*****************************************************
Project :Opgave03
Version :1.0
Date    : 02.03.2018
Author  : Lars Thise Pedersen
Chip type      : ATmega168
Program type   : Application
AVR Core Clock frequency: 16,000000 MHz
*****************************************************/
#define F_CPU 16000000UL  // 16 MHz

#include <avr/io.h> // avr header file for IO ports
#include <stdio.h>
#include <util\delay.h>

char ch;	// Global variabel til udveksling af data

char get_char(void)	//Funktion til h�ndtering modtagelse af data
{
	while (!(UCSR0A & (1<<RXC0))); // Vent til der er modtaget en karakter.
	return UDR0;	// Returner den modtagne karakter til den kaldende funktion
}

void put_char(char c)  //Funktion til h�ndtering af afsendelse af data.
{
	while (!(UCSR0A & (1<< UDRE0))); //Vent til transmitter er ledig
	UDR0 = c;	// L�g data i transmit register
}

void init_uart()	// Se kapitel 19 side170 .....  i Atmel Manual.
{
	UCSR0B=0x00;	// Stop al kommunikation. Sluk Transmitter og Reciever
	UCSR0A=0x00;	// 0- stil alle flag
	UCSR0C=0x06;	// Asynkron, 8 databit, 1 stopbit  (Side 190 - 193)
	UBRR0H=0x00;	// Ops�tning af Baudrate (Side 197)
	UBRR0L=0x19;	// Ops�tning af Baudrate (Side 197)
	UCSR0B=0x18; 	// T�nd for Transmitter og Receiver.
}


int main(void)
{
	init_uart();
	while(1)
	{
		ch = get_char();
		put_char(ch);
	}
	return 1;
}
