/*****************************************************
Project :Opgave02
Version :1.0
Date    : 02.03.2018
Author  : Lars Thise Pedersen
Chip type	   : ATmega168
Program type : Application
AVR Core Clock frequency: 16,000000 MHz
*****************************************************/

#define F_CPU 16000000UL  	// 16 MHz

#include <avr/io.h> 	// avr header file for IO ports
#include <util/delay.h>	// functions for delay loops



int main(void)
{
	DDRC  = 0x00; 	// Alle bit i Port C s�ttes som input
	DDRB  = 0xFF;	// Alle bit i Port B s�ttes som output
	PORTB = 0x00;	// Alle bit p� Port B s�ttes til �0�
	DDRD = 0x00;

while(1) {
	if (PIND & (0x04)) // Hvis Port C bit 0 er forbundet til +5v s�..
	{
		_delay_ms(100);
		PORTB=  (1<<PB5);    	// PORTB.5 = high. Bit 5 = �1�
		_delay_ms(100);
		PORTB &= ~(1<<PB5);    	// PORTB.5 = NOT PORTB.5. Bit 5 inverteres.
	}
	else	      // Ellers, hvis Port C bit 0 er forbundet til GND s�..
	{
		_delay_ms(500);
		PORTB |=  (1<<PB5);    	// PORTB.5 = high. Bit 5 = �1�
		_delay_ms(500);
		PORTB &= ~(1<<PB5);    	// PORTB.5 = NOT PORTB.5. Bit 5 inverteres.
	}
  }
}