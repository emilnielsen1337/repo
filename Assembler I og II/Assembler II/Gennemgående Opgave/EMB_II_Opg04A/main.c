#include "Arduino.h"
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include "UART.h"

int main(void)
{
	RS232Init();

	while (1)
	{
		for (int i = 65;i<128;i++)
		{
			printf("Decimal til ASCII %c = %d \n", i, i);
			_delay_ms(100);
		}
	}
}
