﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace Xmlserializer
{
    class Program
    {
        static string xmlPath = @"D:\repo\XML\Opgave01\skole.xml";
        static string xmlnewPath = @"D:\repo\XML\Opgave01\skolenew.xml";
        static string xmlTilmelding = @"D:\repo\XML\Xmlserializer\Xmlserializer\classes.xml";
        static string stylesheet = @"D:\repo\XML\Xmlserializer\Xmlserializer\stylesheet.xsl";
        static string outputFile = @"D:\repo\XML\Xmlserializer\Xmlserializer\Done.html";
        public static void Main(string[] args)
        {
            XmlSerializer xml = new XmlSerializer(typeof(BrixSchool));
            FileStream file = new FileStream(xmlPath, FileMode.Open);
            XmlReader reader = XmlReader.Create(file);
            BrixSchool brix = (BrixSchool)xml.Deserialize(reader);
            file.Close();

            XmlDocument tilmeldingsfil = new XmlDocument();

            XmlReader Tilmeldingsreader = XmlReader.Create(xmlTilmelding);
            tilmeldingsfil.Load(Tilmeldingsreader);

            var list = tilmeldingsfil.GetElementsByTagName("student");
            for(int i = 0; i < list.Count; i++)
            {
                var strId = list[i].Attributes.GetNamedItem("id").Value;
                var strName = list[i].Attributes.GetNamedItem("name").Value;

                BrixSchoolStudent student = new BrixSchoolStudent();
                student.name = strName;
                student.id = strId;

                brix.students.Add(student);

            }

            XmlWriter writer = XmlWriter.Create(xmlnewPath);
            xml.Serialize(writer, brix);
            writer.Close();

            XslCompiledTransform xslt = new XslCompiledTransform();

            //Compile the style sheet.
            xslt.Load(stylesheet);

            // Execute the XSLT transform.
            xslt.Transform(xmlnewPath, outputFile);
        }


    }
}
