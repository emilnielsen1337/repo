﻿using System;
using System.Xml;
using System.Xml.Schema;

class XPathValidation
{
    private static string xsd = @"D:\Google Drive\Skole\XML\Opgave01\xmlvalidator\techschema.xsd";
    private static string xml = @"D:\Google Drive\Skole\XML\Opgave01\xmlvalidator\techcollege.xml";
    private static string ns = "http://www.w3.org/2001/XMLSchema";

    static void Main()
    {

        try
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(ns, xsd);
            settings.ValidationType = ValidationType.Schema;

            XmlReader reader = XmlReader.Create(xml, settings);
            XmlDocument document = new XmlDocument();
            document.Load(reader);

            ValidationEventHandler eventHandler = new ValidationEventHandler(ValidationEventHandler);

            // the following call to Validate succeeds.
            document.Validate(eventHandler);
            Console.WriteLine("Validated!!");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    static void ValidationEventHandler(object sender, ValidationEventArgs e)
    {
        switch (e.Severity)
        {
            case XmlSeverityType.Error:
                Console.WriteLine("Error: {0}", e.Message);
                break;
            case XmlSeverityType.Warning:
                Console.WriteLine("Warning {0}", e.Message);
                break;
        }

    }
}