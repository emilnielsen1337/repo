﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Transform player;
    public Text scoreText;

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = GameManager.score.ToString();
    }
}
