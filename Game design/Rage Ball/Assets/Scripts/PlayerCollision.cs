﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;
    public GameManager gameManager;

    void OnCollisionEnter(Collision collisionInfo)
    {
        if(collisionInfo.collider.tag == "Obstacle") {
            GameObject player = GameObject.Find("Player");
            player.GetComponent<Rigidbody>().freezeRotation = false;
            movement.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
            
        }
    }
}
