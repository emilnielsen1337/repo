﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Opgave1.Data.Migrations
{
    public partial class addudlåninger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Personale",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    navn = table.Column<string>(nullable: true),
                    stilling = table.Column<string>(nullable: true),
                    PersonerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Personale", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Personale_Personale_PersonerId",
                        column: x => x.PersonerId,
                        principalTable: "Personale",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Udlåninger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BogTitel = table.Column<string>(nullable: true),
                    Låner = table.Column<string>(nullable: true),
                    UdlåningsDato = table.Column<string>(nullable: true),
                    AfleveringsDato = table.Column<string>(nullable: true),
                    BogId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Udlåninger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Udlåninger_Boeger_BogId",
                        column: x => x.BogId,
                        principalTable: "Boeger",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Personale_PersonerId",
                table: "Personale",
                column: "PersonerId");

            migrationBuilder.CreateIndex(
                name: "IX_Udlåninger_BogId",
                table: "Udlåninger",
                column: "BogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Personale");

            migrationBuilder.DropTable(
                name: "Udlåninger");
        }
    }
}
