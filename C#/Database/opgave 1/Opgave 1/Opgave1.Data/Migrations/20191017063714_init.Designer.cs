﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Opgave1.Data;

namespace Opgave1.Data.Migrations
{
    [DbContext(typeof(BibliotekContext))]
    [Migration("20191017063714_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BibliotekApp.Domain.Bog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ForfatterId");

                    b.Property<int>("GenreId");

                    b.Property<string>("ISBN");

                    b.Property<string>("Titel");

                    b.HasKey("Id");

                    b.HasIndex("ForfatterId");

                    b.HasIndex("GenreId");

                    b.ToTable("Boeger");
                });

            modelBuilder.Entity("BibliotekApp.Domain.Forfatter", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Navn");

                    b.HasKey("Id");

                    b.ToTable("Forfattere");
                });

            modelBuilder.Entity("BibliotekApp.Domain.Genre", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Navn");

                    b.HasKey("Id");

                    b.ToTable("Genrer");
                });

            modelBuilder.Entity("BibliotekApp.Domain.Bog", b =>
                {
                    b.HasOne("BibliotekApp.Domain.Forfatter", "Forfatter")
                        .WithMany("Boeger")
                        .HasForeignKey("ForfatterId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BibliotekApp.Domain.Genre", "Genre")
                        .WithMany("Boeger")
                        .HasForeignKey("GenreId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
