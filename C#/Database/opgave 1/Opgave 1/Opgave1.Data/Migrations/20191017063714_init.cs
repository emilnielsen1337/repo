﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Opgave1.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Forfattere",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Navn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forfattere", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genrer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Navn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genrer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Boeger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ISBN = table.Column<string>(nullable: true),
                    Titel = table.Column<string>(nullable: true),
                    ForfatterId = table.Column<int>(nullable: false),
                    GenreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boeger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Boeger_Forfattere_ForfatterId",
                        column: x => x.ForfatterId,
                        principalTable: "Forfattere",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Boeger_Genrer_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genrer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Boeger_ForfatterId",
                table: "Boeger",
                column: "ForfatterId");

            migrationBuilder.CreateIndex(
                name: "IX_Boeger_GenreId",
                table: "Boeger",
                column: "GenreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Boeger");

            migrationBuilder.DropTable(
                name: "Forfattere");

            migrationBuilder.DropTable(
                name: "Genrer");
        }
    }
}
