﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BibliotekApp.Domain;
using Opgave1.Domain;

namespace Opgave1.Data
{
    public class BibliotekContext : DbContext
    {
        public DbSet<Bog> Boeger { get; set; }
        public DbSet<Genre> Genrer { get; set; }
        public DbSet<Forfatter> Forfattere { get; set; }

        public DbSet<Personale> Personale{ get; set; }
        public DbSet<Udlåninger> Udlåninger { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = (local); Database=BibliotekData;Integrated Security=SSPI;");
        }
    }
}
