﻿using BibliotekApp.Domain;
using Opgave1.Data;
using Opgave1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekApp.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            OpretForfatter();
        }
        private static void OpretForfatter()
        {
            Personale nypersonale = new Personale { navn = "Bente", stilling = "bibliotekar" };
            Forfatter nyforfatter = new Forfatter { Navn = "Henrik" };
            Udlåninger nyudlåning = new Udlåninger { BogTitel = "Hans og Grete", Låner = "En eller anden", UdlåningsDato = "10/10-19", AfleveringsDato = "11/11-19" };
            using (var context = new BibliotekContext())
            {
                context.Personale.Add(nypersonale);

                context.Udlåninger.Add(nyudlåning);

                context.Forfattere.Add(nyforfatter);
                context.SaveChanges();
            }
        }
    }
}
