﻿using Opgave1.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BibliotekApp.Domain
{
    public class Bog
    {

        public Bog()
        {
            Udlåninger = new List<Udlåninger>();
        }

        public int Id { get; set; }
        public string ISBN { get; set; }
        public string Titel { get; set; }
        public Forfatter Forfatter { get; set; }
        public int ForfatterId { get; set; }
        public Genre Genre { get; set; }
        public int GenreId { get; set; }
        public List<Udlåninger> Udlåninger { get; set; }
    }
}
