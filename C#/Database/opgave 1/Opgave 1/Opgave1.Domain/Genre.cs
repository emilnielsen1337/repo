﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekApp.Domain
{
    public class Genre
    {
        public Genre()
        {
            Boeger = new List<Bog>();
        }
        public int Id { get; set; }
        public string Navn { get; set; }
        public List<Bog> Boeger { get; set; }
    }
}
