﻿using BibliotekApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave1.Domain
{
    public class Udlåninger
    {
        
        public int Id { get; set; }
        public string BogTitel { get; set; }

        public string Låner { get; set; }

        public string UdlåningsDato { get; set; }

        public string AfleveringsDato { get; set; }

        public Bog Bog  { get; set; }
    }
}
