﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave1.Domain
{
    public class Personale
    {
        public int Id { get; set; }
        public string navn { get; set; }
        public string stilling { get; set; }

        public Personale Personer { get; set; }
    }
}
