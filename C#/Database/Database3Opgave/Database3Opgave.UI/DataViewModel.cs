﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database3Opgave.domain;
using Database3Opgave.data;
using Microsoft.EntityFrameworkCore;
using System.Collections.ObjectModel;
using System.ComponentModel;


namespace Database3Opgave.UI
{
    public class DataViewModel : INotifyPropertyChanged
    {
        //private ObservableCollection<Player> _Players;
        public List<Player> Players;
        public List<Race> Races;
        public List<PClass> PClasses;
        public List<WeaponType> WeaponTypes;
        public List<ArmorType> ArmorTypes;



        public DataViewModel()
        {
            //_Players = new ObservableCollection<Player>();
            //Players = new List<Player>();

            GetEntities();


            //CreatePClass();
            //CreateRace();
            //CreateArmorType();
            //CreateWeaponType();
            //CreatePlayer();
        }


        public void GetEntities()
        {
            var context = new PlayerInfoContext();

            context.Players.Load();
            Players = context.Players.ToList();
            context.Races.Load();
            Races = context.Races.ToList();
            PClasses = context.PClasses.ToList();
            WeaponTypes = context.WeaponTypes.ToList();
            ArmorTypes = context.ArmorTypes.ToList();

        }
        private static void CreatePlayer()
        {
            Player newPlayer = new Player { Name = "Bjoern", Damage = 0, Armor = 0, Life = 0, RaceId = 3, PClassId = 3 };
            using (var context = new PlayerInfoContext())
            {
                context.Players.Add(newPlayer);
                context.SaveChanges();
            }
        }
        private static void CreatePClass()
        {
            PClass newPClass = new PClass { Name = "Rogue", Life = 5, Damage = 3 };
            using (var context = new PlayerInfoContext())
            {
                context.PClasses.Add(newPClass);
                context.SaveChanges();
            }
        }
        private static void CreateRace()
        {
            Race newRace = new Race { Name = "Elf", Damage = 3, Life = 4 };
            using (var context = new PlayerInfoContext())
            {
                context.Races.Add(newRace);
                context.SaveChanges();
            }
        }
        private static void CreateArmorType()
        {
            ArmorType newArmorType = new ArmorType { Name = "Leggins of Stuff", Type = "Leather", Armor = 3 };
            using (var context = new PlayerInfoContext())
            {
                context.ArmorTypes.Add(newArmorType);
                context.SaveChanges();
            }
        }
        private static void CreateWeaponType()
        {
            WeaponType newWeaponType = new WeaponType { Name = "Dr.Oetkers PizzaBurger", Type = "FoodWeapon", Damage = 2 };
            using (var context = new PlayerInfoContext())
            {
                context.WeaponTypes.Add(newWeaponType);
                context.SaveChanges();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
