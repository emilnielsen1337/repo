﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Database3Opgave.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DataViewModel dvModel;
        public MainWindow()
        {
            InitializeComponent();
            dvModel = new DataViewModel();
            DataContext = dvModel;
            InfoDataGrid.ItemsSource = dvModel.Players;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            winCreate win = new winCreate(this, dvModel);
            win.ShowDialog();

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            winUpdate win = new winUpdate();
            win.ShowDialog();
        }

        public void refreshList()
        {
            InfoDataGrid.ItemsSource = dvModel.Players;
        }

    }
}
