﻿using Database3Opgave.data;
using Database3Opgave.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Database3Opgave.UI
{
    /// <summary>
    /// Interaction logic for winCreate.xaml
    /// </summary>
    public partial class winCreate : Window
    {
        private DataViewModel dvModel;
        private MainWindow MainWin;
        public winCreate(MainWindow main, DataViewModel dvModel)
        {
            InitializeComponent();
            //Set references
            this.dvModel = dvModel;
            MainWin = main;

            //Update comboboxes with values from database
            //Races
            foreach (var item in dvModel.PClasses)
            {
                cbClass.Items.Add(item.Name);
            }
            cbClass.SelectedIndex = 0;

            foreach (var item in dvModel.Races)
            {
                cbRace.Items.Add(item.Name);
            }
            cbRace.SelectedIndex = 0;

            foreach (var item in dvModel.ArmorTypes)
            {
                cbArmor.Items.Add(item.Name);
            }
            cbArmor.SelectedIndex = 0;

            foreach (var item in dvModel.WeaponTypes)
            {
                cbWeapon.Items.Add(item.Name);
            }
            cbWeapon.SelectedIndex = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var name = tbName.Text;
            
            int raceLife = 0;
            int classLife = 0;
            int chosenRace = cbRace.SelectedIndex + 1;
            int chosenClass = cbClass.SelectedIndex + 1;

            foreach (var item in dvModel.Races)
            {
                if (item.Id == chosenRace)
                    raceLife = item.Life;
            }

            foreach (var item in dvModel.PClasses)
            {
                if (item.Id == chosenClass)
                    classLife = item.Life;
            }

            Player newPlayer = new Player { Name = name, Damage = 0, Armor = 0, Life = classLife + raceLife, RaceId = chosenRace, PClassId = chosenClass };

            using (var context = new PlayerInfoContext())
            {
                context.Players.Add(newPlayer);
                context.SaveChanges();

                dvModel.Players = context.Players.ToList();
                MainWin.refreshList();

                this.Close();
            }

        }
    }
}
