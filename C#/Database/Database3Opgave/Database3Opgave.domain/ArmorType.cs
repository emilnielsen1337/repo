﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database3Opgave.domain
{
    public class ArmorType
    {
        public ArmorType()
        {
            Equippeds = new List<Equipped>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Armor { get; set; }
        public List<Equipped> Equippeds { get; set; }
    }
}
