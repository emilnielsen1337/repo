﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database3Opgave.domain
{
    public class Player
    {
        public Player()
        {
            Equippeds = new List<Equipped>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public Race Race { get; set; }
        public int RaceId { get; set; }
        public PClass PClass { get; set; }
        public int Life { get; set; }
        public int PClassId { get; set; }
        public int Damage { get; set; }
        public int Armor { get; set; }
        public List<Equipped> Equippeds { get; set; }
    }
}
