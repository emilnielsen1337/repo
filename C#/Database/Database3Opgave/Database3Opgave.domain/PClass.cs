﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database3Opgave.domain
{
    public class PClass
    {
        public PClass()
        {
            Players = new List<Player>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Life { get; set; }
        public int Damage { get; set; }
        public List<Player> Players { get; set; }
    }
}
