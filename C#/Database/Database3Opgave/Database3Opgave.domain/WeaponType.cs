﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database3Opgave.domain
{
    public class WeaponType
    {
        public WeaponType()
        {
            Equippeds = new List<Equipped>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Damage { get; set; }
        public List<Equipped> Equippeds { get; set; }
    }
}
