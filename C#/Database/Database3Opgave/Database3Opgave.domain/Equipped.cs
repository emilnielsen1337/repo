﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Database3Opgave.domain
{
    public class Equipped
    {
        public int Id { get; set; }
        public Player Player { get; set; }
        public int PlayerId { get; set; }
        public WeaponType WeaponType { get; set; }
        public int WeaponTypeId { get; set; }
        public ArmorType ArmorType { get; set; }
        public int ArmorTypeId { get; set; }
    }
}
