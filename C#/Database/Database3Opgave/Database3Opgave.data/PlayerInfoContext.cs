﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Database3Opgave.domain;

namespace Database3Opgave.data
{
    public class PlayerInfoContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<PClass> PClasses { get; set; }
        public DbSet<Equipped> Equippeds { get; set; }
        public DbSet<WeaponType> WeaponTypes { get; set; }
        public DbSet<ArmorType> ArmorTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Server=localhost; Database = PlayerData; User Id=SA; Password=Test1234");
            optionsBuilder.UseSqlServer("Data Source = (local); Database=PlayerData;Integrated Security=SSPI;");
        }
    }
}
