﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database3Opgave.data.Migrations
{
    public partial class init2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtraLife",
                table: "PClasses");

            migrationBuilder.AddColumn<int>(
                name: "Life",
                table: "Players",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Life",
                table: "Players");

            migrationBuilder.AddColumn<int>(
                name: "ExtraLife",
                table: "PClasses",
                nullable: false,
                defaultValue: 0);
        }
    }
}
