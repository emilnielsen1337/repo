﻿using System.Collections.Generic;

namespace Pizzaria
{
    class Pizza : Product
    {
        public int id { get; set; }

        public string name { get; set; }

        public  List<string> ingredients { get; set; }

        public string size { get; set; }
        public double price { get; set; }

        /// <summary>
        /// Pizza Constructor ( Constructs Pizza :P )
        /// </summary>
        /// <param name="name">Name of Pizza</param>
        /// <param name="ingredients">List of Ingredients in Pizza</param>
        /// <param name="nr">Pizza ID</param>
        /// <param name="price">Pizza base price</param>
        /// <param name="size">Size of Pizza</param>
        public Pizza(string name, List<string> ingredients, int id, double price, string size)
        {
            this.name = name;
            this.ingredients = ingredients;
            this.id = id;
            this.price = price;
            this.size = size;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
