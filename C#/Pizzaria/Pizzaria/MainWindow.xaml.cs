﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace Pizzaria
{
    public partial class MainWindow : Window
    {
        double cartPrice = 0;
        int cartPizza = 0;
        int cartDrinks = 0;
        bool discountPizza = false;

        List<Pizza> Menu = new List<Pizza>();
        List<Drinks> DrinksMenu = new List<Drinks>();
        List<string> Cart = new List<string>();



        public MainWindow()
        {
            InitializeComponent();
            cb_Menu.SelectedValue = "Margherita";

            // Pizzas
            List<string> Margherita = new List<string>()
            {
                "Tomato sauce",
                "Mozzarella",
                "Oregano"
            };

            List<string> Crudo = new List<string>()
            {
                "Tomato sauce",
                "Mozzarella",
                "Parma ham"
            };

            List<string> Frutti_di_Mare = new List<string>()
            {
                "Tomato sauce",
                "Seafood",
            };



            // Add to menu
            Menu.Add(new Pizza("Margherita", Margherita, 1, 7, "Normal"));
            Menu.Add(new Pizza("Crudo", Crudo, 2, 8.5, "Normal"));
            Menu.Add(new Pizza("Frutti di Mare", Frutti_di_Mare, 3, 10, "Normal"));
            DrinksMenu.Add(new Drinks("Cola", 1, 3, "Medium"));
            DrinksMenu.Add(new Drinks("Fanta", 2, 3, "Medium"));
            DrinksMenu.Add(new Drinks("Sprite", 3, 3, "Medium"));
        }

        //Get selected pizza
        private void cb_Menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            lb_Ingredients.ItemsSource = Menu[cb_Menu.SelectedIndex].ingredients;

            if (rb_Normal.IsChecked == true)
            {
                lbl_Price.Content = "Price: $" + Menu[cb_Menu.SelectedIndex].price;
            }
            else if (rb_Family.IsChecked == true)
            {
                lbl_Price.Content = "Price: $" + Menu[cb_Menu.SelectedIndex].price * 2;
            } else if (rb_Small.IsChecked == true)
            {
                lbl_Price.Content = "Price: $" + Menu[cb_Menu.SelectedIndex].price / 2;
            }
        }

        //Set pizza size to normal
        private void rb_Normal_Click(object sender, RoutedEventArgs e)
        {
            if (cb_Menu.SelectedItem != null)
                cb_Menu_SelectionChanged(sender, null);
        }

        //Set pizza size to family
        private void rb_Family_Click(object sender, RoutedEventArgs e)
        {
            if (cb_Menu.SelectedItem != null)
                cb_Menu_SelectionChanged(sender, null);
        }

        private void rb_Small_Click(object sender, RoutedEventArgs e)
        {
            if (cb_Menu.SelectedItem != null)
                cb_Menu_SelectionChanged(sender, null);
        }

        private void addCustomPizza_Click(object sender, RoutedEventArgs e)
        {
            double customPizzaPrice = 0;
            cartPizza += 1;

            if (cb_Dough.Text == "Double Dough, $1" && discountPizza == false)
            {
                lb_Cart.Items.Add("Custom Pizza, " + cb_Dough.Text + "(free), " + cb_sauce.Text);
                customPizzaPrice += 1;
            } else if (cb_Dough.Text == "Deep Pan, $2")
            {
                lb_Cart.Items.Add("Custom Pizza, " + cb_Dough.Text + "(free), " + cb_sauce.Text);
                customPizzaPrice += 2;
            } else if (cb_Dough.Text == "Gluten Free, $1")
            {
                lb_Cart.Items.Add("Custom Pizza, " + cb_Dough.Text + "(free), " + cb_sauce.Text);
                customPizzaPrice += 1;
            } else
            {
                lb_Cart.Items.Add("Custom Pizza, " + cb_Dough.Text + ", " + cb_sauce.Text);
            }


            if (cb_Ham.IsChecked == true)
            {
                lb_Cart.Items.Add("- Ham $1.5");
                customPizzaPrice += 1.5;
            }
            if (cb_Pepperoni.IsChecked == true)
            {
                lb_Cart.Items.Add("- Pepperoni $1.5");
                customPizzaPrice += 1.5;
            }
            if(cb_Bacon.IsChecked == true)
            {
                lb_Cart.Items.Add("- Bacon $1.5");
                customPizzaPrice += 1.5;
            }
            if (cb_Mushrooms.IsChecked == true)
            {
                lb_Cart.Items.Add("- Mushrooms $1.5");
                customPizzaPrice += 1.5;
            }
            if (cb_Shrimp.IsChecked == true)
            {
                lb_Cart.Items.Add("- Shrimp $1.5");
                customPizzaPrice += 1.5;
            }
            if (cb_cheeseExtra.IsChecked == true)
            {
                lb_Cart.Items.Add("- Extra Cheese $1");
                customPizzaPrice += 1;
            }
            if (cb_Oregano.IsChecked == true)
            {
                lb_Cart.Items.Add("- Oregano $0.5");
                customPizzaPrice += 0.5;
            }
            if (cb_Garlic.IsChecked == true)
            {
                lb_Cart.Items.Add("- Garlic $0.5");
                customPizzaPrice += 0.5;
            }

            if(rb_Normal.IsChecked == true)
            {
                cartPrice += customPizzaPrice;
                lbl_Price.Content = "Price: $" + cartPrice;
            } else if(rb_Family.IsChecked == true)
            {
                cartPrice += customPizzaPrice * 2;
                lbl_Price.Content = "Price: $" + cartPrice;
            } else if (rb_Small.IsChecked == true)
            {
                cartPrice += customPizzaPrice;
                lbl_Price.Content = "Price: $" + cartPrice / 2;
            }

            if (cartDrinks == 2 && cartPizza == 2)
            {
                discountPizza = true;
                MessageBox.Show("The dough on your next Pizza you order is now free!");
            }
            else
            {
                discountPizza = false;
            }

            tb_finalPrice.Text = "Final Price: $" + cartPrice.ToString();
        }

        private void btn_AddToCart_Click(object sender, RoutedEventArgs e)
        {
            cartPizza += 1;
            lb_Cart.Items.Add(Menu[cb_Menu.SelectedIndex].name + " - $" + Menu[cb_Menu.SelectedIndex].price.ToString());
            cartPrice += Menu[cb_Menu.SelectedIndex].price;
            tb_finalPrice.Text = "Final Price: $" + cartPrice.ToString();

            if (cartDrinks == 2 && cartPizza == 2)
            {
                discountPizza = true;
                MessageBox.Show("The dough on your next Pizza you order is now free!");
            }
            else
            {
                discountPizza = false;
            }
        }

        private void btn_addDrink_Click(object sender, RoutedEventArgs e)
        {
            cartDrinks += 1;
            if(rb_SmallDrink.IsChecked == true)
            {
                DrinksMenu[cb_Drinks.SelectedIndex].size = "Small";
                DrinksMenu[cb_Drinks.SelectedIndex].price = 2;
                cartPrice += 2;

                lb_Cart.Items.Add(DrinksMenu[cb_Drinks.SelectedIndex].name + ", " + DrinksMenu[cb_Drinks.SelectedIndex].size + ", $" + DrinksMenu[cb_Drinks.SelectedIndex].price);
            } else if (rb_Medium.IsChecked == true)
            {
                DrinksMenu[cb_Drinks.SelectedIndex].size = "Medium";
                DrinksMenu[cb_Drinks.SelectedIndex].price = 3;
                cartPrice += 3;
                lb_Cart.Items.Add(DrinksMenu[cb_Drinks.SelectedIndex].name + ", " + DrinksMenu[cb_Drinks.SelectedIndex].size + ", $" + DrinksMenu[cb_Drinks.SelectedIndex].price);
            } else if (rb_Large.IsChecked == true)
            {
                DrinksMenu[cb_Drinks.SelectedIndex].size = "Large";
                DrinksMenu[cb_Drinks.SelectedIndex].price = 4;
                cartPrice += 4;

                lb_Cart.Items.Add(DrinksMenu[cb_Drinks.SelectedIndex].name + ", " + DrinksMenu[cb_Drinks.SelectedIndex].size + ", $" + DrinksMenu[cb_Drinks.SelectedIndex].price);
            }
            
            if(cartDrinks == 2 && cartPizza == 2)
            {
                discountPizza = true;
                MessageBox.Show("The dough on your next Pizza you order is now free!");
            } else
            {
                discountPizza = false;
            }

            tb_finalPrice.Text = "Final Price: $" + cartPrice.ToString();
        }




    }

}
