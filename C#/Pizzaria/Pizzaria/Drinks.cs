﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class Drinks : Product
    {
        public int id { get; set; }

        public string name { get; set; }

        public string size { get; set; }
        public double price { get; set; }


        /// <summary>
        /// Drinks
        /// </summary>
        /// <param name="name">Drink name</param>
        /// <param name="nr">Drink ID</param>
        /// <param name="price">Drink Price</param>
        /// <param name="size">Drink Size</param>
        public Drinks(string name, int id, double price, string size)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.size = size;
        }

    }
}
