﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    interface Product
    {
        int id { get; set; }

        string name { get; set; }

        string size { get; set; }

        double price { get; set; }


    }
}
