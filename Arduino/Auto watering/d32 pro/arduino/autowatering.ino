#include <HttpClient.h>

#include <HTTPClient.h>

#include <ArduinoHttpClient.h>

#include <WiFi.h>


int led = 12;
int moistureSensor = 32;
int pump = 23;

const char* ssid = "Tennisskoven 21#2";
const char* password = "12345678";

void setup() {
  pinMode(led, OUTPUT);
  pinMode(pump, OUTPUT);
  pinMode(moistureSensor, INPUT);
  Serial.begin(115200);
  wifiConnect();
}

void loop() {
  watering();
  apiPost();
  delay(30000);
}

void wifiConnect() {
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    delay(4000);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.print(WiFi.localIP());
    Serial.println("");
  }

void watering() {
  Serial.println("Watering");
  int moist = analogRead(moistureSensor);
  Serial.println(moist);

  if (moist > 1500) {
      digitalWrite(pump, HIGH);
      delay(15000);
      digitalWrite(pump, LOW);
    }
  }

void apiPost() {
    Serial.println("apiPost");
    int moist = analogRead(moistureSensor);
    int httpCode = 0;
    String jsonString;

    if(moist > 1500) {
        jsonString = "{\"watering\": \"On\", \"value\": \"";
      } else {
          jsonString = "{\"watering\": \"Off\", \"value\": \"";
        }
    jsonString += moist;
    jsonString += "\"}";
    Serial.println(jsonString);
    
    HTTPClient http;
    http.begin("http://192.168.1.15:8081/data");
    http.addHeader("Content-Type", "application/json");
    httpCode = http.POST(jsonString);

    if(httpCode == 200) {
      Serial.println("Ok");
        } else {
            Serial.print("Error status code: ");
            Serial.println(httpCode);
          }

    Serial.println("Post ended");
    delay(3000);
    http.end();

  }
