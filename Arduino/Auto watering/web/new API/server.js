const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');


const app = express();
app.use(cors())


//require("routes.js")(app);
// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

require("./routes/routes.js")(app);
// set port, listen for requests
const port = 8081;
app.listen(port, () => {
  console.log("Server is running on port: " + port);
});