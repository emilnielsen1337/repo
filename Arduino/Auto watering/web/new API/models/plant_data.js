const sql = require("./db.js");

// constructor
const Water = function(data) {
  this.watering = data.watering;
  this.value = data.value;
  this.time = data.time;
};

Water.create = (newData, result) => {
  console.log(newData);
  sql.query("INSERT INTO plant_data SET ?", newData, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created watering data: ", { id: res.insertId, ...newData });
    result(null, { id: res.insertId, ...newData });
  });
};

Water.findById = (dataId, result) => {
  sql.query(`SELECT * FROM plant_data WHERE id = ${dataId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found watering data: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Water.getAll = result => {
  sql.query("SELECT * FROM plant_data", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("customers: ", res);
    result(null, res);
  });
};

Water.remove = (id, result) => {
  sql.query("DELETE FROM plant_data WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted watering data with id: ", id);
    result(null, res);
  });
};

Water.removeAll = result => {
  sql.query("DELETE FROM plant_data", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} plant data`);
    result(null, res);
  });
};

module.exports = Water;