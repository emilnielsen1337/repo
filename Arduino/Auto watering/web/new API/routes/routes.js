module.exports = app => {
    const Watering_data = require("../controllers/watering_data.controller.js");
  
    // Create a new Customer
    app.post("/data", Watering_data.create);
  
    // Retrieve all Customers
    app.get("/data", Watering_data.findAll);
  
    // Retrieve a single Customer with customerId
    app.get("/data/:dataId", Watering_data.findOne);
  
    // Delete a Customer with customerId
    app.delete("/data/:dataId", Watering_data.delete);
  
    // Create a new Customer
    app.delete("/data", Watering_data.deleteAll);
  };