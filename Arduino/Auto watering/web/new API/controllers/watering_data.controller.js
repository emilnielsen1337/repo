const Water = require("../models/plant_data.js");



// Create and Save a new Customer
exports.create = (req, res) => {
  let date = new Date();
    let timeAdded = date.getDate() + '-' + 
    (date.getMonth()+1) + '-' +
    date.getFullYear() + ' / ' + 
    date.getHours() + ':' +
    date.getMinutes() + ':' +
    date.getSeconds();

    console.log(req.body);
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create a Customer
    const wateringdata = new Water({
      watering: req.body.watering,
      value: req.body.value,
      time: timeAdded
    });
  
    // Save Customer in the database
    Water.create(wateringdata, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the data."
        });
      else res.send(data);
    });
  };

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
  Water.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving data."
        });
      else res.send(data);
    });
  };

// Find a single Customer with a customerId
exports.findOne = (req, res) => {
  Water.findById(req.params.dataId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found data with id ${req.params.dataId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving data with id " + req.params.dataId
          });
        }
      } else res.send(data);
    });
  };

// Delete a Customer with the specified customerId in the request
exports.delete = (req, res) => {
  Water.remove(req.params.dataId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found data with id ${req.params.dataId}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete data with id " + req.params.dataId
          });
        }
      } else res.send({ message: `Data was deleted successfully!` });
    });
  };

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  Water.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all the data."
        });
      else res.send({ message: `All the data were deleted successfully!` });
    });
  };