const express = require('express');
const app = express();
const cors = require('cors');
const fs = require('fs');

app.use(express.json());
app.use(cors());

// GET
app.get('/api/plant', (req, res) => {
    fs.readFile('./data.json', function read(err, data) {
        if (err) {
            console.log(err);
        }
        res.send(data);
    });
    });


// POST
app.post('/api/plant', (req, res) => {
    let date = new Date();
    let timeAdded = date.getDate() + '-' + 
    (date.getMonth()+1) + '-' +
    date.getFullYear() + ' / ' + 
    date.getHours() + ':' +
    date.getMinutes() + ':' +
    date.getSeconds();

    var newLine = {
        identifier: req.body.identifier,
        value: req.body.value,
        pump: req.body.pump,
        time: timeAdded
    };

    let jsonString = `{"identifier": "${newLine.identifier}","value": "${newLine.value}","pump": "${newLine.pump}","time": "${newLine.time}"}`;

    fs.readFile('data.json', function read(err, data) {
        if (err) {
            console.log("Error:" + err);
        }
        
        data = jsonString;
        var database = JSON.parse(data);
        let jsonData = JSON.stringify(database);
        let dataString = 

        fs.writeFile('data.json', jsonData, function (err) {
            if (err) throw err;
            console.log(`Added: ${JSON.stringify(newLine)}`);
        });
        res.send(newLine);
    });


});

//PORT
const port = process.env.PORT || 8081;
app.listen(port, () => console.log(`Listening on port ${port}`));