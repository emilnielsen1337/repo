import random
from tkinter import *

try: 
    minGuessNumber = 1
    MaxGuessNumber = 9
    i = 0
    highscore = 0
    Guesses = 3
    file_highscore = []
    logIndex = 0
    Player = ''
    highscorePath = 'Z:\\repo\\Hjemmearbejde\\Guess the number\\highscore.txt'
    number = random.randint(minGuessNumber, MaxGuessNumber)

    def Guess(numberGuess):
        global Guesses, number, i , logIndex, highscore, Player
        Guesses -= 1
        i + 1
        logIndex += 1

        if (numberGuess == number):
            Guesses = Guesses + 3
            highscore += 1
            number = random.randint(minGuessNumber, MaxGuessNumber)
            log.insert(i, str(logIndex) + ': ' + "Right! Guess next number")
            lbl_Guesses.config(text = 'No of Guesses: ' + str(Guesses))
            lbl_rightGuesses.config(text = 'Right Guesses: ' + str(highscore))
        elif(numberGuess > number):
            log.insert(i, str(logIndex) + ': ' + "Wrong - number is smaller!")
            lbl_Guesses.config(text = 'No of Guesses: ' + str(Guesses))
        else:
            log.insert(i, str(logIndex) + ': ' + "Wrong - number is higher!")
            lbl_Guesses.config(text = 'No of Guesses: ' + str(Guesses))

        if(Guesses <= 0):
            GameOver(Guesses)

    def GameOver(Guesses):
        log.insert(Guesses, "Game Over")
        btn_1.config(state = DISABLED)
        btn_2.config(state = DISABLED)
        btn_3.config(state = DISABLED)
        btn_4.config(state = DISABLED)
        btn_5.config(state = DISABLED)
        btn_6.config(state = DISABLED)
        btn_7.config(state = DISABLED)
        btn_8.config(state = DISABLED)
        btn_9.config(state = DISABLED)
        saveHighscore()
        readHighscore()

    def GameRestart():
        global Guesses, logIndex, number, highscore
        Guesses = 3
        highscore = 0
        logIndex = 0
        number = random.randint(minGuessNumber, MaxGuessNumber)
        btn_1.config(state = NORMAL)
        btn_2.config(state = NORMAL)
        btn_3.config(state = NORMAL)
        btn_4.config(state = NORMAL)
        btn_5.config(state = NORMAL)
        btn_6.config(state = NORMAL)
        btn_7.config(state = NORMAL)
        btn_8.config(state = NORMAL)
        btn_9.config(state = NORMAL)
        log.delete(0, 'end')
        lbl_Guesses.config(text = 'No of Guesses: ' + str(Guesses))

    def saveHighscore():
        global highscore, Player, file_highscore
        score = []

        with open(highscorePath) as file:
            line = file.read()
            lineSplit = line.split(':', 1)
            score.append(lineSplit[0])
            score.append(lineSplit[1])

        if(int(score[1]) < highscore):
            with open(highscorePath, 'w+') as file:
                file.truncate()
                file.write(Player)
                file.write(': ')
                file.write(str(highscore))

    def readHighscore():
        global file_highscore
        with open(highscorePath, 'r+') as file:
            line = file.read()
            lbl_highscore.config(text = 'Highscore: ' + line)


    def PlayerWindow():
        player = Tk()
        player.title('Player')
        player.geometry('135x85')
        player.configure(bg="black")
        player.resizable(width=False, height=False)

        lbl_playername = Label(player, text='Player Name:', font=('bold', 14), bg="black", fg="white")
        lbl_playername.grid(row=0, column=0, sticky=N)

        input_player = Entry(player)
        input_player.grid(row=1, column=0, sticky=SW, padx=5)

        btn_submit = Button(player, text='Submit', width=12, bg="black", fg="white", command= lambda: PlayerWindowSubmit(input_player.get()))
        btn_submit.grid(row=2, column=0, pady=5)

        def PlayerWindowSubmit(name):
            global Player 
            Player = name
            player.destroy()

    PlayerWindow()
    
    app = Tk()
    app.title('Guess the number')
    app.geometry('650x500')
    app.resizable(width=False, height=False)
    app.configure(bg="black")

    # Labels
    lbl_rightGuesses = Label(app, text='Right Guesses: 0', font=('bold', 14), bg="black", fg="white")
    lbl_rightGuesses.grid(row=0, column=0, sticky=W)

    lbl_Guesses = Label(app, text='No of Guesses: 3', font=('bold', 14), bg="black", fg="white")
    lbl_Guesses.grid(row=0, column=2, sticky=E,)

    lbl_highscore = Label(app, text='Highscore:', font=('bold', 12), bg="black", fg="white")
    lbl_highscore.grid(row=6, column=0, sticky=S)

    # Buttons
    btn_1 = Button(app, text='1', width=12, command= lambda: Guess(1), bg="black", fg="white")
    btn_1.grid(row=3, column=0, sticky=E, pady=5)

    btn_2 = Button(app, text='2', width=12, command= lambda: Guess(2), bg="black", fg="white")
    btn_2.grid(row=3, column=1, pady=5)

    btn_3 = Button(app, text='3', width=12, command= lambda: Guess(3), bg="black", fg="white")
    btn_3.grid(row=3, column=2, pady=5)

    btn_4 = Button(app, text='4', width=12, command= lambda: Guess(4), bg="black", fg="white")
    btn_4.grid(row=4, column=0, sticky=E, pady=5)

    btn_5 = Button(app, text='5', width=12, command= lambda: Guess(5), bg="black", fg="white")
    btn_5.grid(row=4, column=1, pady=5)

    btn_6 = Button(app, text='6', width=12, command= lambda: Guess(6), bg="black", fg="white")
    btn_6.grid(row=4, column=2, pady=5)

    btn_7 = Button(app, text='7', width=12, command= lambda: Guess(7), bg="black", fg="white")
    btn_7.grid(row=5, column=0, sticky=E, pady=5)

    btn_8 = Button(app, text='8', width=12, command= lambda: Guess(8), bg="black", fg="white")
    btn_8.grid(row=5, column=1, pady=5)

    btn_9 = Button(app, text='9', width=12, command= lambda: Guess(9), bg="black", fg="white")
    btn_9.grid(row=5, column=2, pady=5)

    btn_restart = Button(app, text='Restart', width=12, command=lambda: GameRestart(), bg="black", fg="white")
    btn_restart.grid(row=1, column=0, sticky=E)

    # Game log (Listbox)
    log = Listbox(app, width=35, height=17, border=0, font=('bold', 12), bg="black", fg="white")
    log.grid(row=6, column=1, columnspan=1, rowspan=1, pady=5)

    readHighscore()
    app.mainloop()


except KeyboardInterrupt:
    pass