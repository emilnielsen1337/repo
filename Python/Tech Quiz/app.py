def spm():
    import os
    import json
    with open('quiz.json') as json_file:
        data = json.load(json_file)

    points = 0
    quizNumber = 0

    while quizNumber < len(data):
        os.system("cls")
        print(f'Question: {quizNumber}')
        print(f'Points: {points} \n')
        print(data[quizNumber]['spm'], '\n')
        print(data[quizNumber]['a'])
        print(data[quizNumber]['b'])
        print(data[quizNumber]['c'])
        print(data[quizNumber]['d'], '\n')

        user = input('answer = ')

        try:
            ans = data[quizNumber][user]

            if ans is not None and user == data[quizNumber]['answer']:
                points += 1
                quizNumber += 1
            else:
                quizNumber += 1
        except KeyError:
            print( 'Incorrect input')


    os.system("cls")
    print(f'Score = {points} points!!!')