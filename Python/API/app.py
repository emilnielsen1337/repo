import os
import urllib.request, json
with urllib.request.urlopen("http://localhost:3000/api/courses") as url:
    courses = json.loads(url.read().decode())

import urllib.request, json
with urllib.request.urlopen("http://localhost:3000/api/persons") as url:
    persons = json.loads(url.read().decode())

def writeFile(data, path):
    if os.path.exists(path):
        os.remove(path)
    file = open(path, 'w+')
    i = 0
    while i < len(data):
        file.write(str(data[i]))
        file.write('\n')
        i += 1
    file.close()

def log(data):
    i = 0
    while i < len(data):
        print(data[i])
        i += 1