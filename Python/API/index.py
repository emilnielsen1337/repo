from app import *


'''log(data)
writeFile(data)
'''

while True:
    userInput = int(input('View courses or persons \n 1. courses \n 2. persons \n'))
    if userInput == 1:
        log(courses)
        printToFile = str(input('Print json data to new file? yes or no \n'))
        if printToFile == 'yes':
            writeFile(courses, 'courses.json')
            print('courses done')
    elif userInput == 2:
        log(persons)
        printToFile = str(input('Print json data to new file? yes or no \n'))
        if printToFile == 'yes':
            writeFile(persons, 'persons.json')
            print('persons done')
    else:
        print('Incorrect input')
    print('Resetting')
