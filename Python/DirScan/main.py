import os
import math
from tkinter import *
from collections import Counter

size = 0

try:
    
    def Clear():
        file_list.delete(0, END)
        file_entry.delete(0, END)
        size_label2.config(text = "")

    def Scan():
        i = 1
        path = file_entry.get()
        os.chdir(path)
        size = 0
        for file in os.scandir(path):
            size += os.path.getsize(file)
            file_size = str(math.ceil(os.path.getsize(file) / 1024))
            file_final = file.name + " - " + file_size + " KB"
            file_list.insert(i, file_final)
            i + 1
        size_label2.config(text = str(math.ceil(size / 1024)) + " KB")
        
    # Create window object
    app = Tk()
    app.title('DirScan')
    app.geometry('950x520')
    app.resizable(width=False, height=False)
    app.configure(bg="black")

    # Parts
    file_text = StringVar()

    file_label = Label(app, text='Directory Path:', font=('bold', 14), bg="black", fg="white")
    file_label.grid(row=0, column=0, sticky=W, padx=15)

    file_entry = Entry(app, textvariable=file_text, width=40, bg="black", fg="white", insertbackground="white")
    file_entry.grid(row=0, column=0, sticky=E)

    size_label = Label(app, text='Directory Size:', font=('bold', 14), bg="black", fg="white")
    size_label.grid(row=1, column=0, sticky=NW, padx=15)

    size_label2 = Label(app, text=size, font=('bold', 14), bg="black", fg="white")
    size_label2.grid(row=1, column=1, sticky=NW)

    # Checkbox
    subDir = IntVar()
    Checkbutton(app, text="Subdirectories", variable=subDir, bg="black", fg="red").grid(row=0, column=1, sticky=W)

    
    # File List (Listbox)
    file_list = Listbox(app, height=21, width=100, border=0, font=('bold', 12), bg="black", fg="white")
    file_list.grid(row=1, column=0, columnspan=3, rowspan=6, sticky=N, pady=40, padx=15)

    # Create scrollbar
    scrollbar = Scrollbar(app)
    scrollbar.grid(row=2, column=2, sticky=E)
   
    # Set scroll to listbox
    file_list.configure(yscrollcommand=scrollbar.set)
    scrollbar.configure(command=file_list.yview)

    # Buttons
    submit_btn = Button(app, text='Go', width=12, command=Scan, bg="black", fg="white")
    submit_btn.grid(row=0, column=1, pady=20, sticky=E)

    clear_btn = Button(app, text='Clear', width=12, command=Clear, bg="black", fg="white")
    clear_btn.grid(row=0, column=2, pady=20, sticky=W)

    # Start program
    app.mainloop()

except KeyboardInterrupt:
    pass