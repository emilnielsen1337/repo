﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndengradsligningCalc
{
    public partial class Form1 : Form
    {
        LigningsData AndengradsLigning_Obj = new LigningsData();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void button1_Click(object sender, EventArgs e)
        {

            TextBox[] ABCText = { textBoxA, textBoxB, textBoxC };

            Dictionary<string, double> ABC = new Dictionary<string, double>();

            Label[] Labels = { labelRod1, labelRod2, labelTopX, labelTopY };

            ABC.Add("A", ConvertToDouble(textBoxA));
            ABC.Add("B", ConvertToDouble(textBoxB));
            ABC.Add("C", ConvertToDouble(textBoxC));

            //AndengradsLigning_Obj.A = ConvertToDouble(textBoxA);
            //AndengradsLigning_Obj.B = ConvertToDouble(textBoxB);
            //AndengradsLigning_Obj.C = ConvertToDouble(textBoxC);

            //AndengradsLigning_Obj.D = Math.Pow(AndengradsLigning_Obj.B, 2) - 4 * AndengradsLigning_Obj.A * AndengradsLigning_Obj.C;



            double D = ABC["B"] * ABC["B"] - 4 * ABC["A"] * ABC["C"];

            double[] Rod = new double[2];


            if (D == 0)
            {
                Rod[0] = -ABC["B"] / (2.0 * ABC["A"]);
                Rod[1] = Rod[0];

                for (int i = 0; i < 2; i++)
                {
                    Labels[i].Text = Convert.ToString(Rod[i]);
                }

            } else if (D > 0)
            {
                Rod[0] = (-ABC["B"] + Math.Sqrt(D)) / (ABC["A"] * 2);
                Rod[1] = (-ABC["B"] - Math.Sqrt(D)) / (ABC["A"] * 2);

                labelRod1.Text = Convert.ToString(Rod[0]);
                labelRod2.Text = Convert.ToString(Rod[1]);

            } else
            {
                labelRod1.Text = "Ingen løsning";
                labelRod2.Text = "Ingen løsning";
            }
            double TopX = -ABC["B"] / 2 * ABC["A"];
            double TopY = -D / 4 / ABC["A"];

            Labels[2].Text = Convert.ToString(TopX);
            Labels[3].Text = Convert.ToString(TopY);
        }
        double ConvertToDouble(TextBox TB)
        {
            double output = 0;
            double.TryParse(TB.Text, out output);

            return output;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            textBoxA.ResetText();
            textBoxB.ResetText();
            textBoxC.ResetText();

            labelTopX.ResetText();
            labelTopY.ResetText();
            labelRod1.ResetText();
            labelRod2.ResetText();
        }
    }
}
