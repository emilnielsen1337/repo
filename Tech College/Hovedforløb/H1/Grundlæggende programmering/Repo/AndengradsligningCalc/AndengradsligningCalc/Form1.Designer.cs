﻿namespace AndengradsligningCalc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTopYText = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelRod1 = new System.Windows.Forms.Label();
            this.buttonUdregn = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.labelRod2Text = new System.Windows.Forms.Label();
            this.labelTopXText = new System.Windows.Forms.Label();
            this.labelRod1Text = new System.Windows.Forms.Label();
            this.labelRod2 = new System.Windows.Forms.Label();
            this.labelTopX = new System.Windows.Forms.Label();
            this.labelTopY = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "A:";
            // 
            // textBoxA
            // 
            this.textBoxA.Location = new System.Drawing.Point(12, 78);
            this.textBoxA.Name = "textBoxA";
            this.textBoxA.Size = new System.Drawing.Size(174, 22);
            this.textBoxA.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "B:";
            // 
            // textBoxB
            // 
            this.textBoxB.Location = new System.Drawing.Point(12, 151);
            this.textBoxB.Name = "textBoxB";
            this.textBoxB.Size = new System.Drawing.Size(174, 22);
            this.textBoxB.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "C:";
            // 
            // textBoxC
            // 
            this.textBoxC.Location = new System.Drawing.Point(12, 235);
            this.textBoxC.Name = "textBoxC";
            this.textBoxC.Size = new System.Drawing.Size(174, 22);
            this.textBoxC.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(298, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 29);
            this.label4.TabIndex = 7;
            this.label4.Text = "Toppunkt:";
            // 
            // labelTopYText
            // 
            this.labelTopYText.AutoSize = true;
            this.labelTopYText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopYText.Location = new System.Drawing.Point(301, 128);
            this.labelTopYText.Name = "labelTopYText";
            this.labelTopYText.Size = new System.Drawing.Size(34, 20);
            this.labelTopYText.TabIndex = 8;
            this.labelTopYText.Text = "Y =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(303, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 31);
            this.label6.TabIndex = 9;
            this.label6.Text = "Rødderne:";
            // 
            // labelRod1
            // 
            this.labelRod1.AutoSize = true;
            this.labelRod1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRod1.Location = new System.Drawing.Point(375, 243);
            this.labelRod1.Name = "labelRod1";
            this.labelRod1.Size = new System.Drawing.Size(64, 20);
            this.labelRod1.TabIndex = 10;
            this.labelRod1.Text = "VALUE";
            // 
            // buttonUdregn
            // 
            this.buttonUdregn.Location = new System.Drawing.Point(12, 303);
            this.buttonUdregn.Name = "buttonUdregn";
            this.buttonUdregn.Size = new System.Drawing.Size(84, 34);
            this.buttonUdregn.TabIndex = 11;
            this.buttonUdregn.Text = "Udregn";
            this.buttonUdregn.UseVisualStyleBackColor = true;
            this.buttonUdregn.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(102, 303);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(84, 34);
            this.buttonReset.TabIndex = 12;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // labelRod2Text
            // 
            this.labelRod2Text.AutoSize = true;
            this.labelRod2Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRod2Text.Location = new System.Drawing.Point(300, 270);
            this.labelRod2Text.Name = "labelRod2Text";
            this.labelRod2Text.Size = new System.Drawing.Size(68, 20);
            this.labelRod2Text.TabIndex = 13;
            this.labelRod2Text.Text = "Rod 2 =";
            // 
            // labelTopXText
            // 
            this.labelTopXText.AutoSize = true;
            this.labelTopXText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopXText.Location = new System.Drawing.Point(299, 102);
            this.labelTopXText.Name = "labelTopXText";
            this.labelTopXText.Size = new System.Drawing.Size(35, 20);
            this.labelTopXText.TabIndex = 14;
            this.labelTopXText.Text = "X =";
            // 
            // labelRod1Text
            // 
            this.labelRod1Text.AutoSize = true;
            this.labelRod1Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRod1Text.Location = new System.Drawing.Point(301, 240);
            this.labelRod1Text.Name = "labelRod1Text";
            this.labelRod1Text.Size = new System.Drawing.Size(68, 20);
            this.labelRod1Text.TabIndex = 15;
            this.labelRod1Text.Text = "Rod 1 =";
            // 
            // labelRod2
            // 
            this.labelRod2.AutoSize = true;
            this.labelRod2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRod2.Location = new System.Drawing.Point(375, 273);
            this.labelRod2.Name = "labelRod2";
            this.labelRod2.Size = new System.Drawing.Size(64, 20);
            this.labelRod2.TabIndex = 16;
            this.labelRod2.Text = "VALUE";
            // 
            // labelTopX
            // 
            this.labelTopX.AutoSize = true;
            this.labelTopX.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopX.Location = new System.Drawing.Point(340, 105);
            this.labelTopX.Name = "labelTopX";
            this.labelTopX.Size = new System.Drawing.Size(64, 20);
            this.labelTopX.TabIndex = 17;
            this.labelTopX.Text = "VALUE";
            // 
            // labelTopY
            // 
            this.labelTopY.AutoSize = true;
            this.labelTopY.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopY.Location = new System.Drawing.Point(341, 131);
            this.labelTopY.Name = "labelTopY";
            this.labelTopY.Size = new System.Drawing.Size(64, 20);
            this.labelTopY.TabIndex = 18;
            this.labelTopY.Text = "VALUE";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelTopY);
            this.Controls.Add(this.labelTopX);
            this.Controls.Add(this.labelRod2);
            this.Controls.Add(this.labelRod1Text);
            this.Controls.Add(this.labelTopXText);
            this.Controls.Add(this.labelRod2Text);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonUdregn);
            this.Controls.Add(this.labelRod1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelTopYText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxC);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxA);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelTopYText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelRod1;
        private System.Windows.Forms.Button buttonUdregn;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label labelRod2Text;
        private System.Windows.Forms.Label labelTopXText;
        private System.Windows.Forms.Label labelRod1Text;
        private System.Windows.Forms.Label labelRod2;
        private System.Windows.Forms.Label labelTopX;
        private System.Windows.Forms.Label labelTopY;
    }
}

