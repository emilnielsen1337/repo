﻿using System;
using System.Diagnostics;

namespace HandyTools
{
    static class ToolBox
    {
        static DateTime StartingTime; //Stores the starting time for the timer

        static ToolBox()
        {
            Debug.WriteLine("ToolBox V1 Loaded..");
            StartingTime = DateTime.Now;
        }

        /// <summary>
        /// Writes something to the user
        /// </summary>
        /// <param name="string">A string to write to the user</param>
        /// <param name="DisableLineEnter">Set true to net enter a line down after text</param>
        static public void Write(string @string, bool DisableLineEnter = false)
        {
            if (DisableLineEnter)
            {
                Console.Write(@string);
            }
            else
            {
                Console.WriteLine(@string);
            }
        }

        /// <summary>
        /// Writes something to debugconsole
        /// </summary>
        /// <param name="string">What to write</param>
        /// <param name="DisableLineEnter">True = NOT going a line down</param>
        static public void debug(string @string, bool DisableLineEnter = false)
        {
            if (DisableLineEnter)
            {
                Debug.Write(@string);
            }
            else
            {
                Debug.WriteLine(@string);
            }
        }

        /// <summary>
        /// Waits until the user presses enter and returns what the user wrote
        /// </summary>
        /// <param name="WriteToUser">A message to write to the user</param>
        /// <returns>What the user wrote</returns>
        static public string GetUserInput(string WriteToUser = "")
        {
            if (WriteToUser != "")
            {
                Write(WriteToUser + " ", true);
            }
            return Console.ReadLine();
        }

        /// <summary>
        /// Waits for the user to type yes or no and returns a bool accordingly
        /// </summary>
        /// <param name="Question">The question to ask to user</param>
        /// <returns></returns>
        static public bool GetUserInputYesNo(string Question = "")
        {
            if (Question != "")
            {
                Write(Question + ": ", true);
            }

            while (true)
            {
                string InputText = Console.ReadLine().ToLower();

                if (InputText != "")
                {
                    switch (InputText[0])
                    {
                        case 'y':
                            return true;

                        case 'n':
                            return false;

                        default:
                            break;
                    }
                }
                Write("Please type yes or no....");
            }
        }

        /// <summary>
        /// Starts a timer. You can get the time with GetTimeGone()
        /// </summary>
        static public void StartTimer()
        {
            StartingTime = DateTime.Now;
        }

        /// <summary>
        /// Gets the time that has elapsed since StartTimer() was called
        /// </summary>
        /// <returns>The time that has elapsed in ms, or seconds if its higer the 1000</returns>
        static public string GetTimeGone()
        {
            DateTime TimeEnd = DateTime.Now;
            double timeElapsed = Math.Round(TimeEnd.Subtract(StartingTime).TotalMilliseconds);
            string Time;

            if (timeElapsed < 1000)
            {
                Time = timeElapsed.ToString() + " Miliseconds";
            }
            else
            {
                Time = (timeElapsed / 1000).ToString() + " Seconds";
            }
            return Time;
        }

        /// <summary>
        /// Converts a Value from one range to another. Exsample( Value=5 Range:0-10 New Range:0-100 Output=50 )
        /// </summary>
        /// <param name="Value">Number to convert from a range</param>
        /// <param name="RangeInStart">First number in the Values range</param>
        /// <param name="RangeInStop">Last number in the Values range</param>
        /// <param name="RangeOutStart">First number in the new range</param>
        /// <param name="RangeOutStop">Last number in tne new range</param>
        /// <returns>The new Value</returns>
        static public double Map(double Value, double RangeInStart, double RangeInStop, double RangeOutStart, double RangeOutStop)
        {
            return (Value - RangeInStart) * (RangeOutStop - RangeOutStart) / (RangeInStop - RangeInStart) + RangeOutStart;
        }
    }
}