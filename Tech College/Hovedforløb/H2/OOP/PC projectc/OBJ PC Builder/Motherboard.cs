﻿using System.IO;
using System.Windows.Forms;

namespace OBJ_PC_Builder
{
    class Motherboard : Hardware
    {
        public Motherboard(string brand, string name, string type, double price, int maxram, string chipset, CpuSockets cpu_socket, RAMSocket ram_socket, string pictureName)
            : base(brand, name, type, price, pictureName)
        {
            MaxRAM = maxram;
            Chipset = chipset;
            CPU_Socket = cpu_socket;
            RAM_Socket = ram_socket;


        }
        public CpuSockets CPU_Socket { get; protected set; }
        public RAMSocket RAM_Socket { get; protected set; }
        public int MaxRAM { get; protected set; }
        public string Chipset { get; protected set; }
    }
}