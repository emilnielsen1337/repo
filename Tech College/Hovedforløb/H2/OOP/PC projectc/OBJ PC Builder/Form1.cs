﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OBJ_PC_Builder
{
    public partial class Form1 : Form
    {
        List<Hardware> Cases = new List<Hardware>();
        private void CreateCases()
        {
            Cases.Add(new Case("Corsair", "Obsidian 500D", "Mid Tower", 1911, 3, 5, true, "Black"));
            Cases.Add(new Case("NZXT", "H500", "Mid Tower", 699, 2, 4, true, "Black"));
            Cases.Add(new Case("Thermaltake", "Level 20 GT RGB", "Full Tower", 2165, 3, 9, true, "Steel grey"));
            Cases.Add(new Case("Cooler Master", "MasterBox Lite 5", "Mid Tower", 450, 1, 3, true, "Black"));
        }

        List<Hardware> CPUs = new List<Hardware>();
        private void CreateCPU()
        {
            CPUs.Add(new CPU("Intel", "i5", "7400", 1549, 4, 4, 3000, Hardware.CpuSockets.LGA1151, "Intel i5 7000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "7500", 1686, 4, 4, 3400, Hardware.CpuSockets.LGA1151, "Intel i5 7000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "8400", 1849, 6, 6, 2800, Hardware.CpuSockets.LGA1151, "Intel i5 7000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "8500", 1997, 4, 4, 3000, Hardware.CpuSockets.LGA1151, "Intel i5 8000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "8600", 1899, 6, 6, 3100, Hardware.CpuSockets.LGA1151, "Intel i5 8000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "8600K", 2190, 6, 6, 3600, Hardware.CpuSockets.LGA1151, "Intel i5 8000k.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "9600K", 2290, 6, 6, 3700, Hardware.CpuSockets.LGA1151, "Intel i5 9000.bmp"));
            CPUs.Add(new CPU("Intel", "i5", "8700K", 3290, 6, 12, 3700, Hardware.CpuSockets.LGA1151, "Intel i5 8000k.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "8700", 2749, 6, 12, 3200, Hardware.CpuSockets.LGA1151, "Intel i7 8000.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "7800X", 3149, 6, 12, 3500, Hardware.CpuSockets.LGA1151, "Intel i7 7000x.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "7820X", 3699, 6, 12, 3600, Hardware.CpuSockets.LGA1151, "Intel i7 7000x.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "8086K", 3599, 6, 12, 4000, Hardware.CpuSockets.LGA1151, "Intel i7 8086k.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "9700K", 3349, 8, 8, 3400, Hardware.CpuSockets.LGA1151, "Intel i7 9000k.bmp"));
            CPUs.Add(new CPU("Intel", "i7", "9800X", 4990, 8, 16, 3800, Hardware.CpuSockets.LGA2066, "Intel i7 9000x.bmp"));

            CPUs.Add(new CPU("AMD", "Ryzen3", "1300X", 849, 4, 4, 3500, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen3", "2200G", 843, 4, 4, 3500, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1400", 1188, 4, 8, 3200, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1500X", 1396, 4, 8, 3600, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1600", 1570, 6, 12, 3200, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1600X", 1712, 6, 12, 3600, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2400G", 1341, 4, 8, 3600, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2600", 1329, 6, 12, 3900, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2600X", 1799, 6, 12, 4250, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1700", 1863, 8, 16, 3000, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1700X", 2679, 8, 16, 3400, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1800X", 2947, 8, 16, 3600, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen7", "2700", 2404, 8, 16, 4100, Hardware.CpuSockets.AM4));
            CPUs.Add(new CPU("AMD", "Ryzen7", "2700X", 2690, 8, 16, 4350, Hardware.CpuSockets.AM4));
        }

        List<Hardware> GPUs = new List<Hardware>();
        private void CreateGPUs()
        {
            GPUs.Add(new GPU("AMD", "RX", "Vega 64", 4972, 1247, 1546, 8));
            GPUs.Add(new GPU("AMD", "RX", "Vega 56", 3645, 1156, 1471, 8));
            GPUs.Add(new GPU("AMD", "RX", "580", 2624, 1247, 1340, 8));
            GPUs.Add(new GPU("AMD", "RX", "570", 1500, 1168, 1244, 8));

            GPUs.Add(new GPU("Nvidia", "GTX", "1050Ti", 1520, 4, 1290, 1392));
            GPUs.Add(new GPU("Nvidia", "GTX", "1060 3GB", 1899, 3, 1594, 1809));
            GPUs.Add(new GPU("Nvidia", "GTX", "1060 6GB", 2549, 6, 1594, 1809));
            GPUs.Add(new GPU("Nvidia", "GTX", "1070", 3404, 8, 1582, 1771));
            GPUs.Add(new GPU("Nvidia", "GTX", "1080", 5699, 8, 1607, 1733));
            GPUs.Add(new GPU("Nvidia", "GTX", "1080TI", 6999, 11, 1480, 1600));
            GPUs.Add(new GPU("Nvidia", "RTX", "2070", 5049, 8, 1410, 1620));
            GPUs.Add(new GPU("Nvidia", "RTX", "2080", 6989, 8, 1515, 1710));
            GPUs.Add(new GPU("Nvidia", "RTX", "2080TI", 10499, 11, 1350, 1545));
        }

        List<Hardware> PSUs = new List<Hardware>();
        private void CreatePSUs()
        {
            PSUs.Add(new PSU("Corsair", "CX500", "Non-Modular", 500, 500));
            PSUs.Add(new PSU("Corsair", "CX650", "Non-Modular", 650, 523));
            PSUs.Add(new PSU("Corsair", "CX450M", "Semi-Modular", 450, 446));
            PSUs.Add(new PSU("Corsair", "CX750M", "Semi-Modular", 750, 748));
            PSUs.Add(new PSU("Corsair", "CX850M", "Semi-Modular", 850, 780));
            PSUs.Add(new PSU("Corsair", "RM1000i", "Modular", 1000, 1559));
            PSUs.Add(new PSU("Corsair", "AX1200i", "Modular", 1200, 2731));
            PSUs.Add(new PSU("Corsair", "AX1600i", "Modular", 1600, 3772));
        }

        List<Hardware> RAM = new List<Hardware>();
        private void CreateRAM()
        {
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 349, 4, 1, 1600, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 593, 8, 1, 1600, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 1212, 16, 2, 1600, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 696, 8, 1, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 1524, 16, 2, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 2715, 32, 2, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 5456, 64, 4, 2400, Hardware.RAMSocket.DDR4));

            RAM.Add(new RAM("Corsair", "Value S", "DDR3", 239, 4, 1, 1333, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("Corsair", "Vengeance", "DDR3", 409, 8, 1, 1600, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("Corsair", "Vengeance Pro", "DDR3", 899, 16, 1, 1600, Hardware.RAMSocket.DDR3));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 549, 8, 1, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 1099, 16, 1, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 1999, 32, 2, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 3443, 64, 4, 2400, Hardware.RAMSocket.DDR4));
            RAM.Add(new RAM("Corsair", "Vengeance LPX LED", "DDR4", 4299, 64, 4, 2666, Hardware.RAMSocket.DDR4));
        }

        List<Hardware> CPUCooling = new List<Hardware>();
        private void CreateCPUCooling()
        {
            CPUCooling.Add(new CPUCooling("Cooler Master", "ML120L", "120mm", 469, 1, true, true, "CoolerMasterML120L.jpg"));
            CPUCooling.Add(new CPUCooling("Cooler Master", "ML240R", "120mm", 958, 2, true, true, "CoolerMasterML240R.jpg"));
            CPUCooling.Add(new CPUCooling("Cooler Master", "ML360R", "120mm", 1145, 3, true, true, "CoolerMasterML360R.jpg"));

            CPUCooling.Add(new CPUCooling("Cooler Master", "H412R", "90mm", 229, 1, false, false, "CoolerMasterH412R.jpg"));
            CPUCooling.Add(new CPUCooling("Cooler Master", "Hyper212", "120mm", 336, 2, false, true, "CoolerMasterHyper212.jpg"));
            CPUCooling.Add(new CPUCooling("Cooler Master", "MA620P", "120mm", 538, 2, false, true, "CoolerMasterMA620P.jpg"));
        }

        public Form1()
        {
            InitializeComponent();
            CreateCPU();
            SetComboBox(CB_CPU, CPUs);
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            PB_Case.Image = CPUs[CB_CPU.SelectedIndex].Picture;
        }

        private void SetComboBox(ComboBox box, List<Hardware> hardware)
        {
            box.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (var item in hardware)
            {
                box.Items.Add(item);
            }
        }
    }
}
