﻿namespace OBJ_PC_Builder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PB_Case = new System.Windows.Forms.PictureBox();
            this.PB_CPU = new System.Windows.Forms.PictureBox();
            this.PB_CPUCoolor = new System.Windows.Forms.PictureBox();
            this.PB_Motherboard = new System.Windows.Forms.PictureBox();
            this.PB_Ram = new System.Windows.Forms.PictureBox();
            this.PB_GPU = new System.Windows.Forms.PictureBox();
            this.PB_PSU = new System.Windows.Forms.PictureBox();
            this.CB_CPU = new System.Windows.Forms.ComboBox();
            this.CP_CPUCooler = new System.Windows.Forms.ComboBox();
            this.CB_Motherboard = new System.Windows.Forms.ComboBox();
            this.CB_RAM = new System.Windows.Forms.ComboBox();
            this.CB_PSU = new System.Windows.Forms.ComboBox();
            this.CB_GPU = new System.Windows.Forms.ComboBox();
            this.CB_case = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Case)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPUCoolor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Motherboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Ram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_GPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PSU)).BeginInit();
            this.SuspendLayout();
            // 
            // PB_Case
            // 
            this.PB_Case.Location = new System.Drawing.Point(12, 12);
            this.PB_Case.Name = "PB_Case";
            this.PB_Case.Size = new System.Drawing.Size(670, 630);
            this.PB_Case.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Case.TabIndex = 0;
            this.PB_Case.TabStop = false;
            // 
            // PB_CPU
            // 
            this.PB_CPU.Location = new System.Drawing.Point(688, 12);
            this.PB_CPU.Name = "PB_CPU";
            this.PB_CPU.Size = new System.Drawing.Size(100, 100);
            this.PB_CPU.TabIndex = 1;
            this.PB_CPU.TabStop = false;
            // 
            // PB_CPUCoolor
            // 
            this.PB_CPUCoolor.Location = new System.Drawing.Point(688, 118);
            this.PB_CPUCoolor.Name = "PB_CPUCoolor";
            this.PB_CPUCoolor.Size = new System.Drawing.Size(100, 100);
            this.PB_CPUCoolor.TabIndex = 2;
            this.PB_CPUCoolor.TabStop = false;
            // 
            // PB_Motherboard
            // 
            this.PB_Motherboard.Location = new System.Drawing.Point(688, 224);
            this.PB_Motherboard.Name = "PB_Motherboard";
            this.PB_Motherboard.Size = new System.Drawing.Size(100, 100);
            this.PB_Motherboard.TabIndex = 3;
            this.PB_Motherboard.TabStop = false;
            // 
            // PB_Ram
            // 
            this.PB_Ram.Location = new System.Drawing.Point(688, 330);
            this.PB_Ram.Name = "PB_Ram";
            this.PB_Ram.Size = new System.Drawing.Size(100, 100);
            this.PB_Ram.TabIndex = 4;
            this.PB_Ram.TabStop = false;
            // 
            // PB_GPU
            // 
            this.PB_GPU.Location = new System.Drawing.Point(688, 542);
            this.PB_GPU.Name = "PB_GPU";
            this.PB_GPU.Size = new System.Drawing.Size(100, 100);
            this.PB_GPU.TabIndex = 6;
            this.PB_GPU.TabStop = false;
            // 
            // PB_PSU
            // 
            this.PB_PSU.Location = new System.Drawing.Point(688, 436);
            this.PB_PSU.Name = "PB_PSU";
            this.PB_PSU.Size = new System.Drawing.Size(100, 100);
            this.PB_PSU.TabIndex = 5;
            this.PB_PSU.TabStop = false;
            // 
            // CB_CPU
            // 
            this.CB_CPU.FormattingEnabled = true;
            this.CB_CPU.Location = new System.Drawing.Point(794, 12);
            this.CB_CPU.Name = "CB_CPU";
            this.CB_CPU.Size = new System.Drawing.Size(121, 21);
            this.CB_CPU.TabIndex = 7;
            this.CB_CPU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CP_CPUCooler
            // 
            this.CP_CPUCooler.FormattingEnabled = true;
            this.CP_CPUCooler.Location = new System.Drawing.Point(794, 118);
            this.CP_CPUCooler.Name = "CP_CPUCooler";
            this.CP_CPUCooler.Size = new System.Drawing.Size(121, 21);
            this.CP_CPUCooler.TabIndex = 8;
            this.CP_CPUCooler.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_Motherboard
            // 
            this.CB_Motherboard.FormattingEnabled = true;
            this.CB_Motherboard.Location = new System.Drawing.Point(794, 224);
            this.CB_Motherboard.Name = "CB_Motherboard";
            this.CB_Motherboard.Size = new System.Drawing.Size(121, 21);
            this.CB_Motherboard.TabIndex = 9;
            this.CB_Motherboard.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_RAM
            // 
            this.CB_RAM.FormattingEnabled = true;
            this.CB_RAM.Location = new System.Drawing.Point(794, 330);
            this.CB_RAM.Name = "CB_RAM";
            this.CB_RAM.Size = new System.Drawing.Size(121, 21);
            this.CB_RAM.TabIndex = 10;
            this.CB_RAM.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_PSU
            // 
            this.CB_PSU.FormattingEnabled = true;
            this.CB_PSU.Location = new System.Drawing.Point(794, 436);
            this.CB_PSU.Name = "CB_PSU";
            this.CB_PSU.Size = new System.Drawing.Size(121, 21);
            this.CB_PSU.TabIndex = 11;
            this.CB_PSU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_GPU
            // 
            this.CB_GPU.FormattingEnabled = true;
            this.CB_GPU.Location = new System.Drawing.Point(794, 542);
            this.CB_GPU.Name = "CB_GPU";
            this.CB_GPU.Size = new System.Drawing.Size(121, 21);
            this.CB_GPU.TabIndex = 12;
            this.CB_GPU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_case
            // 
            this.CB_case.FormattingEnabled = true;
            this.CB_case.Location = new System.Drawing.Point(561, 12);
            this.CB_case.Name = "CB_case";
            this.CB_case.Size = new System.Drawing.Size(121, 21);
            this.CB_case.TabIndex = 13;
            this.CB_case.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 661);
            this.Controls.Add(this.CB_case);
            this.Controls.Add(this.CB_GPU);
            this.Controls.Add(this.CB_PSU);
            this.Controls.Add(this.CB_RAM);
            this.Controls.Add(this.CB_Motherboard);
            this.Controls.Add(this.CP_CPUCooler);
            this.Controls.Add(this.CB_CPU);
            this.Controls.Add(this.PB_GPU);
            this.Controls.Add(this.PB_PSU);
            this.Controls.Add(this.PB_Ram);
            this.Controls.Add(this.PB_Motherboard);
            this.Controls.Add(this.PB_CPUCoolor);
            this.Controls.Add(this.PB_CPU);
            this.Controls.Add(this.PB_Case);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PB_Case)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPUCoolor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Motherboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Ram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_GPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PSU)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PB_Case;
        private System.Windows.Forms.PictureBox PB_CPU;
        private System.Windows.Forms.PictureBox PB_CPUCoolor;
        private System.Windows.Forms.PictureBox PB_Motherboard;
        private System.Windows.Forms.PictureBox PB_Ram;
        private System.Windows.Forms.PictureBox PB_GPU;
        private System.Windows.Forms.PictureBox PB_PSU;
        private System.Windows.Forms.ComboBox CB_CPU;
        private System.Windows.Forms.ComboBox CP_CPUCooler;
        private System.Windows.Forms.ComboBox CB_Motherboard;
        private System.Windows.Forms.ComboBox CB_RAM;
        private System.Windows.Forms.ComboBox CB_PSU;
        private System.Windows.Forms.ComboBox CB_GPU;
        private System.Windows.Forms.ComboBox CB_case;
    }
}

