﻿namespace OBJ_PC_Builder
{
    class Case : Hardware
    {
        public Case(string brand, string name, string type, double price, int fansIncluded, int hddBays, bool sidewindow, string color, string pictureName)
            :base(brand, name, type, price, pictureName)
        {          
            FansIncluded = fansIncluded;
            HDDBays = hddBays;
            Sidewindow = sidewindow;
            Color = color;
        }
        public int FansIncluded;
        public int HDDBays;
        public bool Sidewindow;
        public string Color;
    }
}
