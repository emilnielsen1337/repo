﻿namespace OBJ_PC_Builder
{
    class PSU : Hardware
    {
        public PSU(string brand, string name, string type, int wattage, double price, string pictureName)
            :base(brand, name, type, price, pictureName)
        {
            wattage = Wattage;
        }

        public override string ToString()
        {
            return $"{Brand} {Name} {Wattage} - {Price}kr";
        }

        public int Wattage { get; protected set; }
    }

}