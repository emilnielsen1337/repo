﻿using System;
using System.Diagnostics;
using System.Drawing;


namespace OBJ_PC_Builder
{
    class Hardware
    {

        public Hardware(string brand, string name, string type, double price, string pictureName)
        {
            Brand = brand;
            Name = name;
            Type = type;
            Price = price;

            try
            {
                Picture = new Bitmap(GetLocation() + pictureName);
            }
            catch (Exception exc)
            {
                Debug.WriteLine("Cant load " + pictureName);
                Debug.WriteLine(exc.Message);
            }
        }

        public Hardware(string brand, string name, string type, double price)
        {
            Brand = brand;
            Name = name;
            Type = type;
            Price = price;
        }

        public enum CpuSockets
        {
            AM4,
            LGA1151,
            LGA2066
        }

        public enum RAMSocket
        {
            DDR3,
            DDR4
        }

        public double Price { get; protected set; }
        public string Name { get; protected set; }
        public string Brand { get; protected set; }
        public string Type { get; protected set; }
        public Bitmap Picture { get; protected set; }

        public override string ToString()
        {
            return $"{Brand} {Name} {Type} - {Price}kr";
        }

        protected string GetLocation()
        {
            string ExeLocation = System.Reflection.Assembly.GetEntryAssembly().Location;
            string Name = AppDomain.CurrentDomain.FriendlyName;

            return ExeLocation.Remove(ExeLocation.Length - Name.Length, Name.Length) + @"Pics\";
        }
    }
}
