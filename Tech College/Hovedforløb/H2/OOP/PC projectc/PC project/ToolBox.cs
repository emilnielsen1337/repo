﻿using System;
using System.Diagnostics;

namespace HandyTools
{
    static class ToolBox
    {
        static private char[] Numbers =
        {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        };

        public static string YesNoRespond { get; set; } = "Please type yes or no....";
        public static bool DebugMode { get; set; } = false;

        static ToolBox()
        {
            Debug.WriteLine("ToolBox V2 Loaded..");
        }

        /// <summary>
        /// Writes A string to the User
        /// </summary>
        /// <param name="string">The string to write</param>
        static public void Write(string @string)
        {
            if (DebugMode == false)
            {
                Console.WriteLine(@string);
            }
            else
            {
                Debug.WriteLine(@string);
            }
        }

        /// <summary>
        /// Writes A string to the User
        /// </summary>
        /// <param name="string">The string to write</param>
        /// <param name="Lines">Amount of lines to drop down</param>
        static public void Write(string @string, int Lines)
        {
            if (DebugMode == false)
            {
                Console.Write(@string);

                if (Lines < 0) { Lines = 0; }

                for (int i = 0; i < Lines; i++)
                {
                    if (DebugMode == false)
                    {
                        Console.WriteLine();
                    }
                }
            }
            else
            {
                Debug.Write(@string);
            }
        }

        /// <summary>
        /// Writes out each line in a array to the user
        /// </summary>
        /// <param name="Array"></param>
        static public void Write(string[] Array)
        {
            foreach (string item in Array)
            {
                Write(item);
            }
        }

        /// <summary>
        /// Writes A string to the Debug console
        /// </summary>
        /// <param name="string">The string to write</param>
        static public void debug(string @string)
        {
            Debug.WriteLine(@string);
        }

        /// <summary>
        /// Writes A string to the Debug console
        /// </summary>
        /// <param name="string">The string to write</param>
        /// <param name="Lines">Amount of lines to drop down</param>
        static public void debug(string @string, int Lines)
        {
            Debug.WriteLine(@string);

            if (Lines < 0) { Lines = 0; }

            for (int i = 0; i < Lines; i++)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Writes out each line in a array to the Debug console
        /// </summary>
        /// <param name="Array"></param>
        static public void debug(string[] Array)
        {
            foreach (string item in Array)
            {
                debug(item);
            }
        }

        /// <summary>
        /// Waits until the user presses enter and returns what the user wrote
        /// </summary>
        /// <returns>What the user wrote</returns>
        static public string GetUserInput()
        {
            return Console.ReadLine();
        }

        /// <summary>
        /// Get string input from user
        /// </summary>
        /// <param name="Question">String to write before user input</param>
        /// <returns></returns>
        static public string GetUserInput(string Question)
        {
            Write(Question + " ", 0);
            return Console.ReadLine();
        }

        /// <summary>
        /// Waits for the user to type yes or no and returns a bool accordingly
        /// </summary>
        /// <param name="Question">The question to ask to user</param>
        /// <returns></returns>
        static public bool GetUserInputYesNo()
        {
            while (true)
            {
                string InputText = Console.ReadLine().ToLower();

                if (InputText != "")
                {
                    switch (InputText[0])
                    {
                        case 'y':
                            return true;

                        case 'n':
                            return false;

                        default:
                            break;
                    }
                }
                Write(YesNoRespond);
            }
        }

        /// <summary>
        /// Converts a Value from one range to another. Exsample( Value=5 Range:0-10 New Range:0-100 Output=50 )
        /// </summary>
        /// <param name="Value">Number to convert from a range</param>
        /// <param name="RangeInStart">First number in the Values range</param>
        /// <param name="RangeInStop">Last number in the Values range</param>
        /// <param name="RangeOutStart">First number in the new range</param>
        /// <param name="RangeOutStop">Last number in tne new range</param>
        /// <returns>The new Value</returns>
        static public double Map(double Value, double RangeInStart, double RangeInStop, double RangeOutStart, double RangeOutStop)
        {
            return (Value - RangeInStart) * (RangeOutStop - RangeOutStart) / (RangeInStop - RangeInStart) + RangeOutStart;
        }

        /// <summary>
        /// Gets plain int from the input string and removes misplaced characters
        /// </summary>
        /// <param name="Str">The string to convert</param>
        /// <returns></returns>
        static public int GetIntFrom(string Str)
        {
            int Out;
            if (!int.TryParse(Str, out Out))
            {
                string newString = "";
                foreach (char ch in Str)
                {
                    if (CheckForNumber(ch))
                    {
                        newString += ch;
                    }
                }
                if (newString == "")
                {
                    Debug.WriteLine("WARNING cant convert " + Str + " to int!!!");
                    return 0;
                }
                Out = GetIntFrom(newString);
            }
            return Out;
        }

        static private bool CheckForNumber(char In)
        {
            foreach (char ch in Numbers)
            {
                if (In == ch)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class EpicTimer
    {
        private DateTime StartingTime; //Stores the starting time for the timer

        public string Mili { get; set; } = " Miliseconds";
        public string Sec { get; set; } = " Seconds";
        public string Min { get; set; } = " Minutes";
        public string TimeToLong { get; set; } = "Time out off range";

        public EpicTimer()
        {
            StartingTime = DateTime.Now;
            ToolBox.debug("Timer created and started at " + StartingTime);
        }

        /// <summary>
        /// Starts a timer. You can get the time with GetTimeGone()
        /// </summary>
        public void StartTimer()
        {
            StartingTime = DateTime.Now;
            ToolBox.debug("Timer reset at " + StartingTime);
        }

        /// <summary>
        /// Gets the time that has elapsed since StartTimer() was called
        /// </summary>
        /// <returns>The time that has elapsed in ms, or seconds if its higer the 1000</returns>
        public string GetTimeGone()
        {
            DateTime TimeEnd = DateTime.Now;
            double timeElapsed = Math.Round(TimeEnd.Subtract(StartingTime).TotalMilliseconds);
            string Time = TimeToLong;

            if (timeElapsed < 1000)
            {
                Time = timeElapsed.ToString() + Mili;
            }
            
            if (timeElapsed >= 1000 && timeElapsed < 60000)
            {
                Time = Math.Round(timeElapsed / 1000, 1).ToString() + Sec;
            }

            if (timeElapsed >= 60000)
            {
                Time = Math.Round(timeElapsed / 60000, 1).ToString() + Min;
            }

            return Time;
        }

        /// <summary>
        /// Gets total miliseconds elapsed 
        /// </summary>
        /// <returns></returns>
        public double GetTimeMili()
        {
            DateTime TimeEnd = DateTime.Now;
            return TimeEnd.Subtract(StartingTime).TotalMilliseconds;
        }
    }
}