﻿namespace PC_project
{
    abstract class GPU : Hardware
    {
        public int DRAM { get; protected set; }
        public double BaseMhz { get; protected set; }
        public double BoostMhz { get; protected set; }
    }

    #region AMD Radeon
    class ASUSRXVEGA64 : GPU
    {
        public ASUSRXVEGA64()
        {
            Brand = "AMD";
            Name = "RX";
            Type = "Vega 64";
            DRAM = 8;
            BaseMhz = 1247;
            BoostMhz = 1546;
            Price = 4972;
        }
    }

    class ASUSRXVEGA56 : GPU
    {
        public ASUSRXVEGA56()
        {
            Brand = "AMD";
            Name = "RX";
            Type = "Vega 56";
            DRAM = 8;
            BaseMhz = 1156;
            BoostMhz = 1471;
            Price = 3645;
        }
    }

    class ASUSRX580 : GPU
    {
        public ASUSRX580()
        {
            Brand = "AMD";
            Name = "RX";
            Type = "580";
            DRAM = 8;
            BaseMhz = 1247;
            BoostMhz = 1340;
            Price = 2624;
        }
    }

    class ASUSRX570 : GPU
    {
        public ASUSRX570()
        {
            Brand = "AMD";
            Name = "RX";
            Type = "570";
            DRAM = 8;
            BaseMhz = 1168;
            BoostMhz = 1244;
            Price = 2624;
        }
    }
    #endregion
    #region Nvidia

    class ASUSGTX1050Ti : GPU
    {
        public ASUSGTX1050Ti()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1050Ti";
            DRAM = 4;
            BaseMhz = 1290;
            BoostMhz = 1392;
            Price = 1520;
        }
    }

    class ASUSGTX1060_3GB : GPU
    {
        public ASUSGTX1060_3GB()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1060";
            DRAM = 3;
            BaseMhz = 1594;
            BoostMhz = 1809;
            Price = 1899;
        }
    }

    class ASUSGTX1060_6GB : GPU
    {
        public ASUSGTX1060_6GB()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1060";
            DRAM = 6;
            BaseMhz = 1594;
            BoostMhz = 1809;
            Price = 2549;
        }
    }

    class ASUSGTX1070 : GPU
    {
        public ASUSGTX1070()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1070";
            DRAM = 8;
            BaseMhz = 1582;
            BoostMhz = 1771;
            Price = 3404;
        }
    }

    class ASUSGTX1080 : GPU
    {
        public ASUSGTX1080()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1080";
            DRAM = 8;
            BaseMhz = 1607;
            BoostMhz = 1733;
            Price = 5699;
        }
    }

    class ASUSGTX1080Ti : GPU
    {
        public ASUSGTX1080Ti()
        {
            Brand = "Nvidia";
            Name = "GTX";
            Type = "1080Ti";
            DRAM = 11;
            BaseMhz = 1480;
            BoostMhz = 1582;
            Price = 6999;
        }
    }

    class ASUSRTX2070 : GPU
    {
        public ASUSRTX2070()
        {
            Brand = "Nvidia";
            Name = "RTX";
            Type = "2070";
            DRAM = 8;
            BaseMhz = 1410;
            BoostMhz = 1620;
            Price = 5049;
        }
    }

    class ASUSRTX2080 : GPU
    {
        public ASUSRTX2080()
        {
            Brand = "Nvidia";
            Name = "RTX";
            Type = "2080";
            DRAM = 8;
            BaseMhz = 1515;
            BoostMhz = 1710;
            Price = 6989;
        }
    }

    class ASUSRTX2080Ti : GPU
    {
        public ASUSRTX2080Ti()
        {
            Brand = "Nvidia";
            Name = "RTX";
            Type = "2080Ti";
            DRAM = 11;
            BaseMhz = 1350;
            BoostMhz = 1545;
            Price = 10499;
        }
    }
    #endregion
}
