﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HandyTools;

namespace PC_project
{
    class Program
    {
        static int ItemsPerPage = 5;
        static Dictionary<string, Hardware[]> AllHardware = new Dictionary<string, Hardware[]>();

        static Hardware[] CPUs =
        {
            new Ryzen3_1300x(),
            new Ryzen3_2200G(),
            new Ryzen5_1400(),
            new Ryzen5_1600(),
            new Ryzen5_1600x(),
            new Ryzen5_2400G(),
            new Ryzen5_2600(),
            new Ryzen5_2600x(),
            new Ryzen7_1700(),
            new Ryzen7_1700x(),
            new Ryzen7_1800x(),
            new Ryzen7_2700(),
            new Ryzen7_2700x(),
            new Intel5_7400(),
            new Intel5_7500(),
            new Intel5_8400(),
            new Intel5_8500(),
            new Intel5_8600(),
            new Intel5_8600k(),
            new Intel5_9600k(),
            new Intel7_7800x(),
            new Intel7_7820x(),
            new Intel7_8086k(),
            new Intel7_8700(),
            new Intel7_8700k(),
            new Intel7_9700k(),
            new Intel7_9800x()
        };
        static Hardware[] GPUs =
        {
            new ASUSGTX1050Ti(),
            new ASUSGTX1060_3GB(),
            new ASUSGTX1060_6GB(),
            new ASUSGTX1070(),
            new ASUSGTX1080(),
            new ASUSGTX1080Ti(),
            new ASUSRTX2070(),
            new ASUSRTX2080(),
            new ASUSRTX2080Ti(),
            new ASUSRX570(),
            new ASUSRX580(),
            new ASUSRXVEGA56(),
            new ASUSRXVEGA64()
        };

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                ToolBox.Write("S: Shop", 2);
                ToolBox.Write("E: EXIT");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.S:
                        Shop();
                        break;
                    case ConsoleKey.E:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        private static void Shop()
        {
            bool Shopping = true;
            do
            {
                Console.Clear();
                ToolBox.Write("1: Select CPUs", 2);
                ToolBox.Write("2: Select GPU", 2);
                ToolBox.Write("E: EXIT");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.D1:
                        SelectItem(CPUs);
                        break;

                    case ConsoleKey.D2:
                        SelectItem(GPUs);
                        break;

                    case ConsoleKey.E:
                        Shopping = false;
                        break;
                }
            } while (Shopping);
        }

        private static Hardware SelectItem(Hardware[] Items)
        {
            int selectedItem = 0;

            int start = 0;
            int end = ItemsPerPage;

            while (true)
            {
                ConsoleKey keyInfo = Console.ReadKey().Key;
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Clear();

                if (keyInfo == ConsoleKey.Enter)
                {
                    Debug.WriteLine(Items[selectedItem].GetFullName());
                    return Items[selectedItem];
                }

                if (keyInfo == ConsoleKey.UpArrow && selectedItem > 0)
                {
                    selectedItem--;
                    if (selectedItem < start)
                    {
                        selectedItem = start;
                    }
                }
                else if (keyInfo == ConsoleKey.DownArrow && selectedItem < Items.Length - 1)
                {
                    selectedItem++;
                    if (selectedItem > end - 1)
                    {
                        selectedItem = end - 1;
                    }
                }
                else if (keyInfo == ConsoleKey.RightArrow)
                {
                    GoPageForward(Items, ref selectedItem, ref start, ref end);
                }
                else if (keyInfo == ConsoleKey.LeftArrow)
                {
                    GoPageBackwards(ref selectedItem, ref start, ref end);
                }

                for (int i = start; i < end; i++)
                {
                    if (selectedItem == i)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Gray;
                    }
                    else
                    {
                        Console.ResetColor();
                    }

                    ToolBox.Write(Items[i].GetFullName(), 2);
                }
            }
        }

        private static void GoPageForward(Hardware[] Items, ref int selectedItem, ref int start, ref int end)
        {
            start += ItemsPerPage;
            end += ItemsPerPage;
            selectedItem += ItemsPerPage;
            if (selectedItem > Items.Length - 1)
            {
                selectedItem = Items.Length - 1;
            }

            if (end > Items.Length)
            {
                end = Items.Length;
                if (start > end)
                {
                    start -= ItemsPerPage;
                }
            }
        }

        private static void GoPageBackwards(ref int selectedItem, ref int start, ref int end)
        {
            start -= ItemsPerPage;
            end -= ItemsPerPage;
            selectedItem -= ItemsPerPage;
            if (selectedItem < 0)
            {
                selectedItem = 0;
            }

            if (end < ItemsPerPage)
            {
                end = ItemsPerPage;
                if (start < 0)
                {
                    start = 0;
                }
            }
        }
    }
}
