﻿using System.Collections.Generic;

namespace PC_project
{
    class CPUKøling : Hardware
    {
        protected List<CpuSockets> CPUSockets = new List<CpuSockets>();
        public int Fans { get; protected set; }
        public bool Vandkøling { get; protected set; }
        public bool RGB { get; protected set; }

        protected virtual void AllSocket()
        {
            CPUSockets.Add(CpuSockets.AM4);
            CPUSockets.Add(CpuSockets.LGA1151);
            CPUSockets.Add(CpuSockets.LGA2066);
        }
    }

    class CoolerMasterML120L : CPUKøling
    {
        public CoolerMasterML120L()
        {
            AllSocket();
            Fans = 1;
            Vandkøling = true;
            RGB = true;
            Brand = "Cooler Master";
            Name = "ML120L";
            Type = "1x 120mm radiator";
            Price = 469;
        }
    }

    class CoolerMasterML240L : CPUKøling
    {
        public CoolerMasterML240L()
        {
            AllSocket();
            Fans = 2;
            Vandkøling = true;
            RGB = true;
            Brand = "Cooler Master";
            Name = "ML240L";
            Type = "2x 120mm radiator";
            Price = 958;
        }
    }

    class CoolerMasterML360R : CPUKøling
    {
        public CoolerMasterML360R()
        {
            AllSocket();
            Fans = 3;
            Vandkøling = true;
            RGB = true;
            Brand = "Cooler Master";
            Name = "ML360R";
            Type = "3x 120mm radiator";
            Price = 1145;
        }
    }

    class CoolerMasterML360R : CPUKøling
    {
        public CoolerMasterML360R()
        {
            AllSocket();
            Fans = 3;
            Vandkøling = true;
            RGB = true;
            Brand = "Cooler Master";
            Name = "ML360R";
            Type = "3x 120mm radiator";
            Price = 1145;
        }
    }

}
