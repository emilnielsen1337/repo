﻿namespace PC_project
{
    class Hardware
    {
        public enum CpuSockets
        {
            AM4,
            LGA1151,
            LGA2066
        }

        public enum RAMSocket
        {
            DDR3,
            DDR4
        }



        public double Price
        {
            get;
            set;
        }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }

        /// <summary>
        /// Gets the full name and price of the item
        /// </summary>
        /// <returns></returns>
        public virtual string GetFullName()
        {
            return $"{Brand} {Name} {Type}\n{Price}kr";
        }
    }
}
