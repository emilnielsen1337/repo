﻿namespace PC_project
{
    abstract class Motherboard : Hardware
    {
        public CpuSockets CPU_Socket { get; protected set; }
        public RAMSocket RAM_Socket { get; protected set; }
        public int MaxRAM { get; protected set; }
        public string Chipset { get; protected set; }
    }

    #region LGA1151
    class GigabyteH310 : Motherboard
    {
        public GigabyteH310()
        {
            CPU_Socket = CpuSockets.LGA1151;
            Chipset = "Intel H310";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "Gigabyte";
            Name = "H310 D3";
            Type = "ATX";
            Price = 677;
        }
    }

    class AsusPrimeZ370 : Motherboard
    {
        public AsusPrimeZ370()
        {
            CPU_Socket = CpuSockets.LGA1151;
            Chipset = "Intel Z370";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "Asus";
            Name = "Prime Z370-P II";
            Type = "ATX";
            Price = 1041;
        }
    }

    class MSIZ390PRO : Motherboard
    {
        public MSIZ390PRO()
        {
            CPU_Socket = CpuSockets.LGA1151;
            Chipset = "Intel Z390";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "Asus";
            Name = "MSI Z390-A PRO";
            Type = "ATX";
            Price = 1142;
        }
    }
    #endregion
    #region LGA2066
    class MSIX299PLUS : Motherboard
    {
        public MSIX299PLUS()
        {
            CPU_Socket = CpuSockets.LGA2066;
            Chipset = "Intel X299 Express";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 128;
            Brand = "MSI";
            Name = "MSI X299 SLI Plus";
            Type = "ATX";
            Price = 1891;
        }
    }

    class ASUSPrimeX299A : Motherboard
    {
        public ASUSPrimeX299A()
        {
            CPU_Socket = CpuSockets.LGA2066;
            Chipset = "Intel X299 Express";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 128;
            Brand = "ASUS";
            Name = "ASUS Prime X299-A";
            Type = "ATX";
            Price = 2336;
        }
    }

    class ASUSROGX299E : Motherboard
    {
        public ASUSROGX299E()
        {
            CPU_Socket = CpuSockets.LGA2066;
            Chipset = "Intel X299 Express";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 128;
            Brand = "ASUS";
            Name = "ASUS ROG Strix X299-E Gaming";
            Type = "ATX";
            Price = 3154;
        }
    }
    #endregion
    #region AM-4
    class ASUSROGB350F : Motherboard
    {
        public ASUSROGB350F()
        {
            CPU_Socket = CpuSockets.AM4;
            Chipset = "AMD B350";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "ASUS";
            Name = "ASUS ROG Strix B350-F Gaming";
            Type = "ATX";
            Price = 988;
        }
    }

    class MSIB450GAMING : Motherboard
    {
        public MSIB450GAMING()
        {
            CPU_Socket = CpuSockets.AM4;
            Chipset = "AMD B450";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "MSI";
            Name = "MSI B450 GAMING PRO CARBON AC";
            Type = "ATX";
            Price = 1214;
        }
    }

    class ASUSVIIHERO : Motherboard
    {
        public ASUSVIIHERO()
        {
            CPU_Socket = CpuSockets.AM4;
            Chipset = "AMD X470";
            RAM_Socket = RAMSocket.DDR4;
            MaxRAM = 64;
            Brand = "ASUS";
            Name = "ASUS ROG CROSSHAIR VII HERO";
            Type = "ATX";
            Price = 2475;
        }
    }
    #endregion      
}