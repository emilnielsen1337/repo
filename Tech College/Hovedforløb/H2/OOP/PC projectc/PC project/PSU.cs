﻿namespace PC_project
{
    abstract class PSU : Hardware
    {
        public int Wattage { get; protected set; }

        public override string GetFullName()
        {
            return $"{Brand} {Name} {Type} {Wattage.ToString()}W\n{Price}kr";
        }
    }
    #region Corsair
    class Corsair_CX500 : PSU
    {
        public Corsair_CX500()
        {
            Wattage = 500;
            Price = 500;
            Brand = "Corsair";
            Name = "CX500";
            Type = "Non-Modular";
        }
    }

    class Corsair_CX650 : PSU
    {
        public Corsair_CX650()
        {
            Wattage = 650;
            Price = 523;
            Brand = "Corsair";
            Name = "CX650";
            Type = "Non-Modular";
        }
    }

    class Corsair_CX450M : PSU
    {
        public Corsair_CX450M()
        {
            Wattage = 450;
            Price = 446;
            Brand = "Corsair";
            Name = "CX450M";
            Type = "Semi-Modular";
        }
    }

    class Corsair_CX750M : PSU
    {
        public Corsair_CX750M()
        {
            Wattage = 750;
            Price = 748;
            Brand = "Corsair";
            Name = "CX750m";
            Type = "Semi-Modular";
        }
    }

    class Corsair_CX850M : PSU
    {
        public Corsair_CX850M()
        {
            Wattage = 850;
            Price = 780;
            Brand = "Corsair";
            Name = "CX850m";
            Type = "Semi-Modular";
        }
    }

    class Corsair_RM1000i : PSU
    {
        public Corsair_RM1000i()
        {
            Wattage = 1000;
            Price = 1559;
            Brand = "Corsair";
            Name = "RM1000i";
            Type = "Fully-Modular";
        }
    }

    class Corsair_AX1200i : PSU
    {
        public Corsair_AX1200i()
        {
            Wattage = 1200;
            Price = 2731;
            Brand = "Corsair";
            Name = "AX1200i";
            Type = "Fully-Modular";
        }
    }

    class Corsair_AX1600i : PSU
    {
        public Corsair_AX1600i()
        {
            Wattage = 1600;
            Price = 3772;
            Brand = "Corsair";
            Name = "AX1600i";
            Type = "Fully-Modular";
        }
    }
    #endregion
}
