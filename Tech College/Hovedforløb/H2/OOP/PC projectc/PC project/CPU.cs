﻿namespace PC_project
{
    abstract class CPU : Hardware
    {
        public int CoreCount { get; protected set; }
        public int ThreadCount { get; protected set; }
        public double Mhz { get; protected set; }
        public CpuSockets Socket { get; protected set; }
    }

    #region Intel
    class Intel : CPU
    {

    }

    class Intel5_7400 : Intel
    {
        public Intel5_7400()
        {
            CoreCount = 4;
            ThreadCount = 4;
            Mhz = 3000;
            Socket = CpuSockets.LGA1151;
            Price = 1549;
            Name = "i5";
            Type = "7400";
            Brand = "Intel";
        }
    }

    class Intel5_7500 : Intel
    {
        public Intel5_7500()
        {
            CoreCount = 4;
            ThreadCount = 4;
            Mhz = 3400;
            Socket = CpuSockets.LGA1151;
            Price = 1686;
            Name = "i5";
            Type = "7500";
            Brand = "Intel";
        }
    }

    class Intel5_8400 : Intel
    {
        public Intel5_8400()
        {
            CoreCount = 6;
            ThreadCount = 6;
            Mhz = 2800;
            Socket = CpuSockets.LGA1151;
            Price = 1849;
            Name = "i5";
            Type = "8400";
            Brand = "Intel";
        }
    }

    class Intel5_8600 : Intel
    {
        public Intel5_8600()
        {
            CoreCount = 6;
            ThreadCount = 6;
            Mhz = 3100;
            Socket = CpuSockets.LGA1151;
            Price = 1899;
            Name = "i5";
            Type = "8600";
            Brand = "Intel";

        }
    }

    class Intel5_8600k : Intel
    {
        public Intel5_8600k()
        {
            CoreCount = 6;
            ThreadCount = 6;
            Mhz = 3600;
            Socket = CpuSockets.LGA1151;
            Price = 2190;
            Name = "i5";
            Type = "8600k";
            Brand = "Intel";
        }
    }

    class Intel5_8500 : Intel
    {
        public Intel5_8500()
        {
            CoreCount = 6;
            ThreadCount = 6;
            Mhz = 3000;
            Socket = CpuSockets.LGA1151;
            Price = 1997;
            Name = "i5";
            Type = "8500";
            Brand = "Intel";
        }
    }

    class Intel5_9600k : Intel
    {
        public Intel5_9600k()
        {
            CoreCount = 6;
            ThreadCount = 6;
            Mhz = 3700;
            Socket = CpuSockets.LGA1151;
            Price = 2290;
            Name = "i5";
            Type = "9600k";
            Brand = "Intel";
        }
    }

    class Intel7_9800x : Intel
    {
        public Intel7_9800x()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 3800;
            Socket = CpuSockets.LGA2066;
            Price = 4990;
            Name = "i7";
            Type = "9800x";
            Brand = "Intel";
        }
    }

    class Intel7_9700k : Intel
    {
        public Intel7_9700k()
        {
            CoreCount = 8;
            ThreadCount = 8;
            Mhz = 3600;
            Socket = CpuSockets.LGA1151;
            Price = 3349;
            Name = "i5";
            Type = "9700k";
            Brand = "Intel";
        }
    }

    class Intel7_8700k : Intel
    {
        public Intel7_8700k()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3700;
            Socket = CpuSockets.LGA1151;
            Price = 3290;
            Name = "i5";
            Type = "8700k";
            Brand = "Intel";
        }
    }

    class Intel7_8700 : Intel
    {
        public Intel7_8700()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3200;
            Socket = CpuSockets.LGA1151;
            Price = 2749;
            Name = "i7";
            Type = "8700";
            Brand = "Intel";
        }
    }

    class Intel7_8086k : Intel
    {
        public Intel7_8086k()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 4000;
            Socket = CpuSockets.LGA1151;
            Price = 3599;
            Name = "i7";
            Type = "8086k";
            Brand = "Intel";
        }
    }

    class Intel7_7800x : Intel
    {
        public Intel7_7800x()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3500;
            Socket = CpuSockets.LGA2066;
            Price = 3149;
            Name = "i7";
            Type = "7800x";
            Brand = "Intel";
        }
    }

    class Intel7_7820x : Intel
    {
        public Intel7_7820x()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3600;
            Socket = CpuSockets.LGA2066;
            Price = 3699;
            Name = "i7";
            Type = "7820";
            Brand = "Intel";
        }
    }
    #endregion
    #region AMD
    class AMD : CPU
    {

    }

    class Ryzen5_2600 : AMD
    {
        public Ryzen5_2600()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3900;
            Socket = CpuSockets.AM4;
            Price = 1329;
            Name = "Ryzen 5";
            Type = "2600";
            Brand = "AMD";
        }
    }

    class Ryzen5_2600x : AMD
    {
        public Ryzen5_2600x()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 4250;
            Socket = CpuSockets.AM4;
            Price = 1799;
            Name = "Ryzen 5";
            Type = "2600x";
            Brand = "AMD";
        }
    }

    class Ryzen5_2400G : AMD
    {
        public Ryzen5_2400G()
        {
            CoreCount = 4;
            ThreadCount = 8;
            Mhz = 3600;
            Socket = CpuSockets.AM4;
            Price = 1341;
            Name = "Ryzen 5";
            Type = "2400G";
            Brand = "AMD";
        }
    }

    class Ryzen5_1600 : AMD
    {
        public Ryzen5_1600()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3200;
            Socket = CpuSockets.AM4;
            Price = 1570;
            Name = "Ryzen 5";
            Type = "1600";
            Brand = "AMD";
        }
    }

    class Ryzen5_1600x : AMD
    {
        public Ryzen5_1600x()
        {
            CoreCount = 6;
            ThreadCount = 12;
            Mhz = 3600;
            Socket = CpuSockets.AM4;
            Price = 1712;
            Name = "Ryzen 5";
            Type = "1600x";
            Brand = "AMD";
        }
    }

    class Ryzen5_1400 : AMD
    {
        public Ryzen5_1400()
        {
            CoreCount = 4;
            ThreadCount = 8;
            Mhz = 3200;
            Socket = CpuSockets.AM4;
            Price = 1188;
            Name = "Ryzen 5";
            Type = "1400";
            Brand = "AMD";
        }
        class Ryzen5_1500x : AMD
        {
            public Ryzen5_1500x()
            {
                CoreCount = 4;
                ThreadCount = 8;
                Mhz = 3600;
                Socket = CpuSockets.AM4;
                Price = 1396;
                Name = "Ryzen 5";
                Type = "1500x";
                Brand = "AMD";
            }
        }
    }

    class Ryzen3_2200G : AMD
    {
        public Ryzen3_2200G()
        {
            CoreCount = 4;
            ThreadCount = 4;
            Mhz = 3500;
            Socket = CpuSockets.AM4;
            Price = 843;
            Name = "Ryzen 3";
            Type = "2200G";
            Brand = "AMD";
        }
    }

    class Ryzen3_1300x : AMD
    {
        public Ryzen3_1300x()
        {
            CoreCount = 4;
            ThreadCount = 4;
            Mhz = 3500;
            Socket = CpuSockets.AM4;
            Price = 849;
            Name = "Ryzen 3";
            Type = "1300x";
            Brand = "AMD";
        }
    }

    class Ryzen7_2700x : AMD
    {
        public Ryzen7_2700x()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 4350;
            Socket = CpuSockets.AM4;
            Price = 2690;
            Name = "Ryzen 7";
            Type = "2700x";
            Brand = "AMD";
        }
    }

    class Ryzen7_2700 : AMD
    {
        public Ryzen7_2700()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 4100;
            Socket = CpuSockets.AM4;
            Price = 2404;
            Name = "Ryzen 7";
            Type = "2700";
            Brand = "AMD";
        }
    }

    class Ryzen7_1700 : AMD
    {
        public Ryzen7_1700()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 3000;
            Socket = CpuSockets.AM4;
            Price = 1863;
            Name = "Ryzen 7";
            Type = "1700";
            Brand = "AMD";
        }
    }

    class Ryzen7_1700x : AMD
    {
        public Ryzen7_1700x()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 3400;
            Socket = CpuSockets.AM4;
            Price = 2679;
            Name = "Ryzen 7";
            Type = "1700x";
            Brand = "AMD";
        }
    }

    class Ryzen7_1800x : AMD
    {
        public Ryzen7_1800x()
        {
            CoreCount = 8;
            ThreadCount = 16;
            Mhz = 3600;
            Socket = CpuSockets.AM4;
            Price = 2947;
            Name = "Ryzen 7";
            Type = "1800x";
            Brand = "AMD";
        }
    }
    #endregion
}
