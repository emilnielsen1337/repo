﻿namespace PC_project
{
    abstract class RAM : Hardware
    {
        public int Capacity { get; protected set; }
        public double Mhz { get; protected set; }
        public RAMSocket Socket { get; protected set; }

    }
    #region HyperX
    class HyperXFuryDDR3_4GB : RAM
    {
        public HyperXFuryDDR3_4GB()
        {
            Capacity = 4;
            Mhz = 1600;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "HyperX";
            Name = "Fury";
            Price = 349;
        }
    }

    class HyperXFuryDDR3_8GB : RAM
    {
        public HyperXFuryDDR3_8GB()
        {
            Capacity = 8;
            Mhz = 1600;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "HyperX";
            Name = "Fury";
            Price = 593;
        }
    }

    class HyperXFuryDDR3_16GB : RAM
    {
        public HyperXFuryDDR3_16GB()
        {
            Capacity = 16;
            Mhz = 1600;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "HyperX";
            Name = "Fury";
            Price = 1212;
        }
    }

    class HyperXFuryDDR4_8GB : RAM
    {
        public HyperXFuryDDR4_8GB()
        {
            Capacity = 8;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "HyperX";
            Name = "Fury";
            Price = 549;
        }
    }

    class HyperXFuryDDR4_16GB : RAM
    {
        public HyperXFuryDDR4_16GB()
        {
            Capacity = 16;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "HyperX";
            Name = "Fury";
            Price = 1524;
        }
    }

    class HyperXFuryDDR4_32GB : RAM
    {
        public HyperXFuryDDR4_32GB()
        {
            Capacity = 32;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "HyperX";
            Name = "Fury";
            Price = 2715;
        }
    }

    class HyperXFuryDDR4_64GB : RAM
    {
        public HyperXFuryDDR4_64GB()
        {
            Capacity = 64;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "HyperX";
            Name = "Fury";
            Price = 5456;
        }
    }
    #endregion
    #region Corsair
    class CorsairValueSDDR3_4GB : RAM
    {
        public CorsairValueSDDR3_4GB()
        {
            Capacity = 4;
            Mhz = 1333;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "Corsair";
            Name = "Value S";
            Price = 239;
        }
    }

    class CorsairVengeanceDDR3_8GB : RAM
    {
        public CorsairVengeanceDDR3_8GB()
        {
            Capacity = 8;
            Mhz = 1600;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "Corsair";
            Name = "Vengeance";
            Price = 409;
        }
    }

    class CorsairVengeanceProDDR3_16GB : RAM
    {
        public CorsairVengeanceProDDR3_16GB()
        {
            Capacity = 16;
            Mhz = 1600;
            Socket = RAMSocket.DDR3;
            Type = "DDR3";
            Brand = "Corsair";
            Name = "Vengeance Pro";
            Price = 459;
        }
    }

    class CorsairVengeanceLPXDDR4_8GB : RAM
    {
        public CorsairVengeanceLPXDDR4_8GB()
        {
            Capacity = 8;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "Corsair";
            Name = "Vengeance LPX";
        }
    }

    class CorsairVengeanceLPXDDR4_16GB : RAM
    {
        public CorsairVengeanceLPXDDR4_16GB()
        {
            Capacity = 16;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "Corsair";
            Name = "Vengeance LPX";
            Price = 1099;
        }
    }

    class CorsairVengeanceLPXDDR4_32GB : RAM
    {
        public CorsairVengeanceLPXDDR4_32GB()
        {
            Capacity = 32;
            Mhz = 2400;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "Corsair";
            Name = "Vengeance LPX";
            Price = 1999;
        }
    }

    class CorsairVengeanceLPXDDR4_64GB : RAM
    {
        public CorsairVengeanceLPXDDR4_64GB()
        {
            Capacity = 64;
            Mhz = 3600;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "Corsair";
            Name = "Vengeance LPX";
            Price = 7499;
        }
    }

    class CorsairVengeanceLEDDDR4_64GB : RAM
    {
        public CorsairVengeanceLEDDDR4_64GB()
        {
            Capacity = 64;
            Mhz = 2666;
            Socket = RAMSocket.DDR4;
            Type = "DDR4";
            Brand = "Corsair";
            Name = "Vengeance LPX";
            Price = 4299;
        }
    }
}
       #endregion 