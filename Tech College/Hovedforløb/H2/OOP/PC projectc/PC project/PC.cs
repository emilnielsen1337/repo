﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_project
{
    class PC
    {
        public CPU cpu;
        public GPU gpu;
        public RAM ram;
        public PSU psu;
        public double Price;

        public PC(CPU inCPU, GPU inGPU, RAM inRAM, PSU inPSU)
        {
            cpu = inCPU;
            gpu = inGPU;
            ram = inRAM;
            psu = inPSU;
        }
    }
}
