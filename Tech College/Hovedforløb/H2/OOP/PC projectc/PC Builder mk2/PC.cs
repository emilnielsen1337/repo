﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Builder_mk2
{
    class PC
    {
        private List<Hardware> PCparts = new List<Hardware>();

        public PC()
        {

        }

        public void AddComponent(Hardware component)
        {
            Hardware ToRemove = null;
            foreach (var item in PCparts)
            {
                if (item.GetType() == component.GetType())
                {
                    ToRemove = item;
                    Debug.WriteLine("Removed " + item);
                }
            }

            if (ToRemove != null)
            {
                PCparts.Remove(ToRemove);
            }

            PCparts.Add(component);
        }

        public double GetFullPrice()
        {
            double price = 0;

            foreach (var item in PCparts)
            {
                price += item.Price;
            }
            return price;
        }
    }
}