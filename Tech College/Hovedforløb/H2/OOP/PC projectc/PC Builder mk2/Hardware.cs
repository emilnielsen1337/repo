﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace PC_Builder_mk2
{
    class Hardware
    {
        public double Price { get; protected set; }
        public string Name { get; protected set; }
        public string Brand { get; protected set; }
        public string Type { get; protected set; }

        private string PicturePath;

        public Bitmap Picture
        {
            get
            {
                try
                {
                    return new Bitmap(PicturePath);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine("WARNING Cant load : " + PicturePath);
                    Debug.WriteLine(exc.Message);
                    return null;
                }
            }
            protected set { }
        }

        public Hardware(string brand, string name, string type, double price, string pictureName)
        {
            Brand = brand;
            Name = name;
            Type = type;
            Price = price;
            string path = GetLocation() + pictureName + ".bmp";

            if (File.Exists(path))
            {
                PicturePath = path;
            }
            else
            {
                Debug.WriteLine("Cant find " + path);
            }

        }

        public enum CpuSockets
        {
            AM4,
            LGA1151,
            LGA2066
        }

        public enum RAMSocket
        {
            DDR3,
            DDR4
        }

        public override string ToString()
        {
            return $"{Brand} {Name} {Type} - {Price}kr";
        }

        protected string GetLocation()
        {
            string ExeLocation = System.Reflection.Assembly.GetEntryAssembly().Location;
            string Name = AppDomain.CurrentDomain.FriendlyName;

            return ExeLocation.Remove(ExeLocation.Length - Name.Length, Name.Length) + @"Pics\";
        }
    }
}
