﻿using System.IO;
using System.Windows.Forms;

namespace PC_Builder_mk2
{
    class Motherboard : Hardware, ShopItems
    {
        public Motherboard(string brand, string name, string type, double price, int maxram, string chipset, CpuSockets cpu_socket, RAMSocket ram_socket, string pictureName)
            : base(brand, name, type, price, pictureName)
        {
            MaxRAM = maxram;
            Chipset = chipset;
            CPU_Socket = cpu_socket;
            RAM_Socket = ram_socket;


        }
        public CpuSockets CPU_Socket { get; protected set; }
        public RAMSocket RAM_Socket { get; protected set; }
        public int MaxRAM { get; protected set; }
        public string Chipset { get; protected set; }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"CPU Socket: {CPU_Socket.ToString()}\n" +
                $"RAM Socket: {RAM_Socket.ToString()}\n" +
                $"Chipset: {Chipset}\n" +
                $"Max RAM: {MaxRAM.ToString()}GB";
        }
    }
}