﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PC_Builder_mk2
{
    public partial class Form1 : Form
    {
        #region All hardware
        List<Hardware> Cases = new List<Hardware>();
        private void CreateCases()
        {
            Cases.Add(new Case("Corsair", "Obsidian 500D", "Mid Tower", 1911, 3, 5, true, "Black", "Corsair Obsidian 500d"));
            Cases.Add(new Case("NZXT", "H500", "Mid Tower", 699, 2, 4, true, "Black", "NZXT h500"));
            Cases.Add(new Case("Thermaltake", "Level 20 GT RGB", "Full Tower", 2165, 3, 9, true, "Steel grey", "Thermaltake-Level-20"));
            Cases.Add(new Case("Cooler Master", "MasterBox Lite 5", "Mid Tower", 450, 1, 3, true, "Black", "Coolermaster lite 5"));
        }

        List<Hardware> CPUs = new List<Hardware>();
        private void CreateCPU()
        {
            CPUs.Add(new CPU("Intel", "i5", "7400", 1549, 4, 4, 3000, Hardware.CpuSockets.LGA1151, "Intel i5 7000"));
            CPUs.Add(new CPU("Intel", "i5", "7500", 1686, 4, 4, 3400, Hardware.CpuSockets.LGA1151, "Intel i5 7000"));
            CPUs.Add(new CPU("Intel", "i5", "8400", 1849, 6, 6, 2800, Hardware.CpuSockets.LGA1151, "Intel i5 7000"));
            CPUs.Add(new CPU("Intel", "i5", "8500", 1997, 4, 4, 3000, Hardware.CpuSockets.LGA1151, "Intel i5 8000"));
            CPUs.Add(new CPU("Intel", "i5", "8600", 1899, 6, 6, 3100, Hardware.CpuSockets.LGA1151, "Intel i5 8000"));
            CPUs.Add(new CPU("Intel", "i5", "8600K", 2190, 6, 6, 3600, Hardware.CpuSockets.LGA1151, "Intel i5 8000k"));
            CPUs.Add(new CPU("Intel", "i5", "9600K", 2290, 6, 6, 3700, Hardware.CpuSockets.LGA1151, "Intel i5 9000"));
            CPUs.Add(new CPU("Intel", "i5", "8700K", 3290, 6, 12, 3700, Hardware.CpuSockets.LGA1151, "Intel i5 8000k"));
            CPUs.Add(new CPU("Intel", "i7", "8700", 2749, 6, 12, 3200, Hardware.CpuSockets.LGA1151, "Intel i7 8000"));
            CPUs.Add(new CPU("Intel", "i7", "7800X", 3149, 6, 12, 3500, Hardware.CpuSockets.LGA1151, "Intel i7 7000x"));
            CPUs.Add(new CPU("Intel", "i7", "7820X", 3699, 6, 12, 3600, Hardware.CpuSockets.LGA1151, "Intel i7 7000x"));
            CPUs.Add(new CPU("Intel", "i7", "8086K", 3599, 6, 12, 4000, Hardware.CpuSockets.LGA1151, "Intel i7 8086k"));
            CPUs.Add(new CPU("Intel", "i7", "9700K", 3349, 8, 8, 3400, Hardware.CpuSockets.LGA1151, "Intel i7 9000k"));
            CPUs.Add(new CPU("Intel", "i7", "9800X", 4990, 8, 16, 3800, Hardware.CpuSockets.LGA2066, "Intel i7 9000x"));

            CPUs.Add(new CPU("AMD", "Ryzen3", "1300X", 849, 4, 4, 3500, Hardware.CpuSockets.AM4, "Ryzen3 1300x"));
            CPUs.Add(new CPU("AMD", "Ryzen3", "2200G", 843, 4, 4, 3500, Hardware.CpuSockets.AM4, "Ryzen3 2200g"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1400", 1188, 4, 8, 3200, Hardware.CpuSockets.AM4, "Ryzen5 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1500X", 1396, 4, 8, 3600, Hardware.CpuSockets.AM4, "Ryzen5 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1600", 1570, 6, 12, 3200, Hardware.CpuSockets.AM4, "Ryzen5 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "1600X", 1712, 6, 12, 3600, Hardware.CpuSockets.AM4, "Ryzen5 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2400G", 1341, 4, 8, 3600, Hardware.CpuSockets.AM4, "Ryzen5 2400g"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2600", 1329, 6, 12, 3900, Hardware.CpuSockets.AM4, "Ryzen5 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen5", "2600X", 1799, 6, 12, 3600, Hardware.CpuSockets.AM4, "Ryzen5 2600x"));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1700", 1863, 8, 16, 3000, Hardware.CpuSockets.AM4, "Ryzen7 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1700X", 2679, 8, 16, 3400, Hardware.CpuSockets.AM4, "Ryzen7 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen7", "1800X", 2947, 8, 16, 3600, Hardware.CpuSockets.AM4, "Ryzen7 1000"));
            CPUs.Add(new CPU("AMD", "Ryzen7", "2700", 2404, 8, 16, 4100, Hardware.CpuSockets.AM4, "Ryzen7 2700"));
            CPUs.Add(new CPU("AMD", "Ryzen7", "2700X", 2690, 8, 16, 4350, Hardware.CpuSockets.AM4, "Ryzen7 2700x"));
            CPUs.Add(new CPU("AMD", "Ryzen Threadripper", "2990WX", 14962, 32, 64, 3600, Hardware.CpuSockets.AM4, "Threadripper"));
        }

        List<Hardware> Motherboards = new List<Hardware>();
        private void CreateMotherboards()
        {
            Motherboards.Add(new Motherboard("Gigabyte", "H310 D3", "ATX", 677, 64, "Intel H310", Hardware.CpuSockets.LGA1151, Hardware.RAMSocket.DDR4, "GigabyteH310"));
            Motherboards.Add(new Motherboard("Asus", "Prime Z370-P II", "ATX", 1041, 64, "Intel Z370", Hardware.CpuSockets.LGA1151, Hardware.RAMSocket.DDR4, "ASUS Prime Z370-P II"));
            Motherboards.Add(new Motherboard("MSI", "Z390-A PRO", "ATX", 1142, 64, "Intel Z390", Hardware.CpuSockets.LGA1151, Hardware.RAMSocket.DDR4, "MSI Z390-A PRO"));
            Motherboards.Add(new Motherboard("MSI", "X299 SLI PLUS", "ATX", 1891, 128, "Intel X299 Express", Hardware.CpuSockets.LGA2066, Hardware.RAMSocket.DDR4, "MSI X299 SLI Plus"));
            Motherboards.Add(new Motherboard("Asus", "Prime X299-A", "ATX", 2336, 128, "Intel X299 Express", Hardware.CpuSockets.LGA2066, Hardware.RAMSocket.DDR4, "ASUS Prime X299-A"));
            Motherboards.Add(new Motherboard("Asus", "ROG Strix X299-E", "ATX", 3154, 128, "Intel X299 Express", Hardware.CpuSockets.LGA2066, Hardware.RAMSocket.DDR4, "ASUS ROG Strix X299-E Gaming"));
            Motherboards.Add(new Motherboard("Asus", "ROG Strix B350-F", "ATX", 988, 64, "AMD B350", Hardware.CpuSockets.AM4, Hardware.RAMSocket.DDR4, "ASUS ROG Strix B350-F Gaming"));
            Motherboards.Add(new Motherboard("MSI", "B450 Gaming Pro Carbon AC", "ATX", 1214, 64, "AMD B450", Hardware.CpuSockets.AM4, Hardware.RAMSocket.DDR4, "MSI B450 GAMING PRO CARBON AC"));
            Motherboards.Add(new Motherboard("Asus", "ROG Crosshair VII Hero", "ATX", 2475, 64, "AMD X470", Hardware.CpuSockets.AM4, Hardware.RAMSocket.DDR4, "ASUS ROG CROSSHAIR VII HERO"));
        }

        List<Hardware> GPUs = new List<Hardware>();
        private void CreateGPUs()
        {
            GPUs.Add(new GPU("AMD", "RX", "Vega 64", 4972, 8, 1247, 1546, "Asus vega 64"));
            GPUs.Add(new GPU("AMD", "RX", "Vega 56", 3645, 8, 1156, 1471, "Asus vega 56"));
            GPUs.Add(new GPU("AMD", "RX", "580", 2624, 8, 1247, 1340,  "Asus rx 580"));
            GPUs.Add(new GPU("AMD", "RX", "570", 1500, 8, 1168, 1244,  "Asus rx 570"));

            GPUs.Add(new GPU("Nvidia", "GTX", "1050Ti", 1520, 4, 1290, 1392, "ASUS GeForce GTX 1050 Ti"));
            GPUs.Add(new GPU("Nvidia", "GTX", "1060 3GB", 1899, 3, 1594, 1809, "ASUS GeForce GTX 1060 3GB"));
            GPUs.Add(new GPU("Nvidia", "GTX", "1060 6GB", 2549, 6, 1594, 1809, "ASUS GeForce GTX 1060 6GB"));
            GPUs.Add(new GPU("Nvidia", "GTX", "1070", 3404, 8, 1582, 1771, "ASUS GeForce GTX 1070"));
            GPUs.Add(new GPU("Nvidia", "GTX", "1080", 5699, 8, 1607, 1733, "ASUS GeForce GTX 1080"));
            GPUs.Add(new GPU("Nvidia", "GTX", "1080TI", 6999, 11, 1480, 1600, "ASUS GeForce GTX 1080Ti"));
            GPUs.Add(new GPU("Nvidia", "RTX", "2070", 5049, 8, 1410, 1620, "ASUS GeForce RTX 2070"));
            GPUs.Add(new GPU("Nvidia", "RTX", "2080", 6989, 8, 1515, 1710, "ASUS GeForce RTX 2080 Ti"));
            GPUs.Add(new GPU("Nvidia", "RTX", "2080TI", 10499, 11, 1350, 1545, "ASUS GeForce RTX 2080"));
        }

        List<Hardware> PSUs = new List<Hardware>();
        private void CreatePSUs()
        {
            PSUs.Add(new PSU("Corsair", "CX450M", "Semi-Modular", 450, 446, "CX450m"));
            PSUs.Add(new PSU("Corsair", "CX750M", "Semi-Modular", 750, 748, "CX750m"));
            PSUs.Add(new PSU("Corsair", "CX850M", "Semi-Modular", 850, 780, "CX850m"));
            PSUs.Add(new PSU("Corsair", "RM1000i", "Modular", 1000, 1559, "RM100i"));
            PSUs.Add(new PSU("Corsair", "AX1200i", "Modular", 1200, 2731, "AX1200i"));
            PSUs.Add(new PSU("Corsair", "AX1600i", "Modular", 1600, 3772, "AX1600i"));
        }

        List<Hardware> RAM = new List<Hardware>();
        private void CreateRAM()
        {
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 349, 4, 1, 1600, Hardware.RAMSocket.DDR3, "HyperX Fury DDR3x1"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 593, 8, 1, 1600, Hardware.RAMSocket.DDR3, "HyperX Fury DDR3x1"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR3", 1212, 16, 2, 1600, Hardware.RAMSocket.DDR3, "HyperX Fury DDR3x2"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 696, 8, 1, 2400, Hardware.RAMSocket.DDR4, "HyperX Fury DDR4x1"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 1524, 16, 2, 2400, Hardware.RAMSocket.DDR4, "HyperX Fury DDR4x2"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 2715, 32, 2, 2400, Hardware.RAMSocket.DDR4, "HyperX Fury DDR4x2"));
            RAM.Add(new RAM("HyperX", "Fury", "DDR4", 5456, 64, 4, 2400, Hardware.RAMSocket.DDR4, "HyperX Fury DDR4x1"));

            RAM.Add(new RAM("Corsair", "Value S", "DDR3", 239, 4, 1, 1333, Hardware.RAMSocket.DDR3, "Corsair Value S"));
            RAM.Add(new RAM("Corsair", "Vengeance", "DDR3", 409, 8, 1, 1600, Hardware.RAMSocket.DDR3, "Corsair Vengeance DDR3"));
            RAM.Add(new RAM("Corsair", "Vengeance Pro", "DDR3", 899, 16, 1, 1600, Hardware.RAMSocket.DDR3, "Corsair Vengeance Pro DDR3"));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 549, 8, 1, 2400, Hardware.RAMSocket.DDR4, "Corsair Vengeance LPX DDR4x1"));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 1099, 16, 1, 2400, Hardware.RAMSocket.DDR4, "Corsair Vengeance LPX DDR4x1"));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 1999, 32, 2, 2400, Hardware.RAMSocket.DDR4, "Corsair Vengeance LPX DDR4x2"));
            RAM.Add(new RAM("Corsair", "Vengeance LPX", "DDR4", 3443, 64, 4, 2400, Hardware.RAMSocket.DDR4, "Corsair Vengeance LPX DDR4x4"));
            RAM.Add(new RAM("Corsair", "Vengeance LPX LED", "DDR4", 4299, 64, 4, 2666, Hardware.RAMSocket.DDR4, "Vengeance LPX LED"));
        }

        List<Hardware> CPUCooler = new List<Hardware>();
        private void CreateCPUCooler()
        {
            CPUCooler.Add(new CPUCooling("Cooler Master", "ML120L", "120mm", 469, 1, true, true, "CoolerMasterML120L"));
            CPUCooler.Add(new CPUCooling("Cooler Master", "ML240R", "120mm", 958, 2, true, true, "CoolerMasterML240R"));
            CPUCooler.Add(new CPUCooling("Cooler Master", "ML360R", "120mm", 1145, 3, true, true, "CoolerMasterML360R"));

            CPUCooler.Add(new CPUCooling("Cooler Master", "H412R", "90mm", 229, 1, false, false, "CoolerMasterH412R"));
            CPUCooler.Add(new CPUCooling("Cooler Master", "Hyper212", "120mm", 336, 2, false, true, "CoolerMasterHyper212"));
            CPUCooler.Add(new CPUCooling("Cooler Master", "MA620P", "120mm", 538, 2, false, true, "CoolerMasterMA620P"));
        }

        Dictionary<ComboBox, List<Hardware>> ComboBoxHardwareValuePairs = new Dictionary<ComboBox, List<Hardware>>();
        private void CreateComboBoxHardwareValuePairs()
        {
            ComboBoxHardwareValuePairs.Add(CB_CPU, CPUs);
            ComboBoxHardwareValuePairs.Add(CB_CPUCooler, CPUCooler);
            ComboBoxHardwareValuePairs.Add(CB_RAM, RAM);
            ComboBoxHardwareValuePairs.Add(CB_Motherboard, Motherboards);
            ComboBoxHardwareValuePairs.Add(CB_GPU, GPUs);
            ComboBoxHardwareValuePairs.Add(CB_PSU, PSUs);
            ComboBoxHardwareValuePairs.Add(CB_Case, Cases);
        }

        Dictionary<ComboBox, PictureBox> ComboBoxPixtureBoxValuePairs = new Dictionary<ComboBox, PictureBox>();
        private void CreateComboBoxPixtureBoxValuePairs()
        {
            ComboBoxPixtureBoxValuePairs.Add(CB_CPU, PB_CPU);
            ComboBoxPixtureBoxValuePairs.Add(CB_CPUCooler, PB_CPUCooler);
            ComboBoxPixtureBoxValuePairs.Add(CB_RAM, PB_RAM);
            ComboBoxPixtureBoxValuePairs.Add(CB_Motherboard, PB_Motherboard);
            ComboBoxPixtureBoxValuePairs.Add(CB_GPU, PB_GPU);
            ComboBoxPixtureBoxValuePairs.Add(CB_PSU, PB_PSU);
            ComboBoxPixtureBoxValuePairs.Add(CB_Case, PB_Case);
        }
        #endregion

        PC PC = new PC();

        public Form1()
        {
            InitializeComponent();

            CreateCPU();
            CreateCPUCooler();
            CreateRAM();
            CreateMotherboards();
            CreateGPUs();
            CreatePSUs();
            CreateCases();

            CreateComboBoxHardwareValuePairs();
            CreateComboBoxPixtureBoxValuePairs();

            SetComboBox();
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComboBox box = sender as ComboBox;
                List<Hardware> Items = ComboBoxHardwareValuePairs[box];
                PictureBox picture = ComboBoxPixtureBoxValuePairs[box];
                picture.Image = Items[box.SelectedIndex].Picture;

                PC.AddComponent(Items[box.SelectedIndex]);
                LB_Price.Text = PC.GetFullPrice().ToString();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
            }
        }

        private void SetComboBox()
        {
            foreach (var ComboBox in ComboBoxHardwareValuePairs)
            {
                ComboBox.Key.DropDownStyle = ComboBoxStyle.DropDownList;
                foreach (var hardware in ComboBox.Value)
                {
                    ComboBox.Key.Items.Add(hardware);
                }
                ComboBox.Key.DropDownWidth = DropDownWidth(ComboBox.Key);
            }
        }

        private int DropDownWidth(ComboBox myCombo)
        {
            int maxWidth = 0;
            int temp = 0;
            Label label1 = new Label();

            foreach (var obj in myCombo.Items)
            {
                label1.Text = obj.ToString();
                temp = label1.PreferredWidth;
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
            }
            label1.Dispose();
            return maxWidth;
        }

        private void PB_MouseHover(object sender, EventArgs e)
        {
            PictureBox pictureBox = sender as PictureBox;
            PB_Case.Image = (pictureBox).Image;

            Hardware ItemToGet = null;

            foreach (var item in ComboBoxPixtureBoxValuePairs)
            {
                if (item.Value == pictureBox)
                {
                    ComboBox comboBox = item.Key;

                    if (comboBox.SelectedIndex > -1)
                    {
                        List<Hardware> hardwares = ComboBoxHardwareValuePairs[comboBox];
                        ItemToGet = hardwares[comboBox.SelectedIndex];
                    }
                }
            }

            ToolTip tt = new ToolTip();

            if (ItemToGet != null)
                tt.SetToolTip(pictureBox as Control, (ItemToGet as ShopItems).GetDetails());

        }

        private void PB_MouseLeave(object sender, EventArgs e)
        {
            int Selected = CB_Case.SelectedIndex;
            if (Selected != -1)
            {
                PB_Case.Image = Cases[Selected].Picture;
            }
        }
    }
}
