﻿namespace PC_Builder_mk2
{
    class RAM : Hardware, ShopItems
    {
        public RAM(string brand, string name, string type, double price, int capacity, int sticks, double mhz, RAMSocket socket, string pictureName)
            : base(brand, name, type, price, pictureName)
        {
            Capacity = capacity;
            Mhz = mhz;
            Socket = socket;
            Sticks = sticks;
        }
        public int Capacity { get; protected set; }
        public double Mhz { get; protected set; }
        public int Sticks { get; protected set; }
        public RAMSocket Socket { get; protected set; }

        public override string ToString()
        {
            return $"{base.ToString()} {Capacity}GB";
        }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"Mhz: {Mhz}\n" +
                $"Capacity: {Capacity}\n" +
                $"RAM Socket: {Socket.ToString()}\n" +
                $"Sticks: {Sticks}";
        }
    }
}