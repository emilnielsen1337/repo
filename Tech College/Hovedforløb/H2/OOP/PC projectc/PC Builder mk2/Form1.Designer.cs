﻿namespace PC_Builder_mk2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PB_Case = new System.Windows.Forms.PictureBox();
            this.PB_CPU = new System.Windows.Forms.PictureBox();
            this.PB_CPUCooler = new System.Windows.Forms.PictureBox();
            this.PB_RAM = new System.Windows.Forms.PictureBox();
            this.PB_Motherboard = new System.Windows.Forms.PictureBox();
            this.PB_GPU = new System.Windows.Forms.PictureBox();
            this.PB_PSU = new System.Windows.Forms.PictureBox();
            this.CB_CPU = new System.Windows.Forms.ComboBox();
            this.CB_CPUCooler = new System.Windows.Forms.ComboBox();
            this.CB_RAM = new System.Windows.Forms.ComboBox();
            this.CB_Motherboard = new System.Windows.Forms.ComboBox();
            this.CB_GPU = new System.Windows.Forms.ComboBox();
            this.CB_PSU = new System.Windows.Forms.ComboBox();
            this.CB_Case = new System.Windows.Forms.ComboBox();
            this.LB_Price = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Case)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPUCooler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_RAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Motherboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_GPU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PSU)).BeginInit();
            this.SuspendLayout();
            // 
            // PB_Case
            // 
            this.PB_Case.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PB_Case.Location = new System.Drawing.Point(12, 12);
            this.PB_Case.Name = "PB_Case";
            this.PB_Case.Size = new System.Drawing.Size(560, 509);
            this.PB_Case.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Case.TabIndex = 0;
            this.PB_Case.TabStop = false;
            this.PB_Case.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_Case.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_CPU
            // 
            this.PB_CPU.Location = new System.Drawing.Point(578, 12);
            this.PB_CPU.Name = "PB_CPU";
            this.PB_CPU.Size = new System.Drawing.Size(80, 80);
            this.PB_CPU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_CPU.TabIndex = 1;
            this.PB_CPU.TabStop = false;
            this.PB_CPU.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_CPU.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_CPUCooler
            // 
            this.PB_CPUCooler.Location = new System.Drawing.Point(578, 98);
            this.PB_CPUCooler.Name = "PB_CPUCooler";
            this.PB_CPUCooler.Size = new System.Drawing.Size(80, 80);
            this.PB_CPUCooler.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_CPUCooler.TabIndex = 2;
            this.PB_CPUCooler.TabStop = false;
            this.PB_CPUCooler.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_CPUCooler.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_RAM
            // 
            this.PB_RAM.Location = new System.Drawing.Point(578, 184);
            this.PB_RAM.Name = "PB_RAM";
            this.PB_RAM.Size = new System.Drawing.Size(80, 80);
            this.PB_RAM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_RAM.TabIndex = 3;
            this.PB_RAM.TabStop = false;
            this.PB_RAM.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_RAM.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_Motherboard
            // 
            this.PB_Motherboard.Location = new System.Drawing.Point(578, 270);
            this.PB_Motherboard.Name = "PB_Motherboard";
            this.PB_Motherboard.Size = new System.Drawing.Size(80, 80);
            this.PB_Motherboard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Motherboard.TabIndex = 4;
            this.PB_Motherboard.TabStop = false;
            this.PB_Motherboard.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_Motherboard.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_GPU
            // 
            this.PB_GPU.Location = new System.Drawing.Point(578, 356);
            this.PB_GPU.Name = "PB_GPU";
            this.PB_GPU.Size = new System.Drawing.Size(80, 80);
            this.PB_GPU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_GPU.TabIndex = 5;
            this.PB_GPU.TabStop = false;
            this.PB_GPU.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_GPU.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // PB_PSU
            // 
            this.PB_PSU.Location = new System.Drawing.Point(578, 442);
            this.PB_PSU.Name = "PB_PSU";
            this.PB_PSU.Size = new System.Drawing.Size(80, 80);
            this.PB_PSU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_PSU.TabIndex = 6;
            this.PB_PSU.TabStop = false;
            this.PB_PSU.MouseLeave += new System.EventHandler(this.PB_MouseLeave);
            this.PB_PSU.MouseHover += new System.EventHandler(this.PB_MouseHover);
            // 
            // CB_CPU
            // 
            this.CB_CPU.FormattingEnabled = true;
            this.CB_CPU.Location = new System.Drawing.Point(664, 12);
            this.CB_CPU.Name = "CB_CPU";
            this.CB_CPU.Size = new System.Drawing.Size(121, 21);
            this.CB_CPU.TabIndex = 7;
            this.CB_CPU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_CPUCooler
            // 
            this.CB_CPUCooler.FormattingEnabled = true;
            this.CB_CPUCooler.Location = new System.Drawing.Point(664, 98);
            this.CB_CPUCooler.Name = "CB_CPUCooler";
            this.CB_CPUCooler.Size = new System.Drawing.Size(121, 21);
            this.CB_CPUCooler.TabIndex = 8;
            this.CB_CPUCooler.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_RAM
            // 
            this.CB_RAM.FormattingEnabled = true;
            this.CB_RAM.Location = new System.Drawing.Point(664, 184);
            this.CB_RAM.Name = "CB_RAM";
            this.CB_RAM.Size = new System.Drawing.Size(121, 21);
            this.CB_RAM.TabIndex = 9;
            this.CB_RAM.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_Motherboard
            // 
            this.CB_Motherboard.FormattingEnabled = true;
            this.CB_Motherboard.Location = new System.Drawing.Point(664, 270);
            this.CB_Motherboard.Name = "CB_Motherboard";
            this.CB_Motherboard.Size = new System.Drawing.Size(121, 21);
            this.CB_Motherboard.TabIndex = 10;
            this.CB_Motherboard.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_GPU
            // 
            this.CB_GPU.FormattingEnabled = true;
            this.CB_GPU.Location = new System.Drawing.Point(664, 356);
            this.CB_GPU.Name = "CB_GPU";
            this.CB_GPU.Size = new System.Drawing.Size(121, 21);
            this.CB_GPU.TabIndex = 11;
            this.CB_GPU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_PSU
            // 
            this.CB_PSU.FormattingEnabled = true;
            this.CB_PSU.Location = new System.Drawing.Point(664, 442);
            this.CB_PSU.Name = "CB_PSU";
            this.CB_PSU.Size = new System.Drawing.Size(121, 21);
            this.CB_PSU.TabIndex = 12;
            this.CB_PSU.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // CB_Case
            // 
            this.CB_Case.FormattingEnabled = true;
            this.CB_Case.Location = new System.Drawing.Point(451, 12);
            this.CB_Case.Name = "CB_Case";
            this.CB_Case.Size = new System.Drawing.Size(121, 21);
            this.CB_Case.TabIndex = 13;
            this.CB_Case.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // LB_Price
            // 
            this.LB_Price.AutoSize = true;
            this.LB_Price.Location = new System.Drawing.Point(661, 508);
            this.LB_Price.Name = "LB_Price";
            this.LB_Price.Size = new System.Drawing.Size(35, 13);
            this.LB_Price.TabIndex = 14;
            this.LB_Price.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(661, 495);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Price";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 539);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LB_Price);
            this.Controls.Add(this.CB_Case);
            this.Controls.Add(this.CB_PSU);
            this.Controls.Add(this.CB_GPU);
            this.Controls.Add(this.CB_Motherboard);
            this.Controls.Add(this.CB_RAM);
            this.Controls.Add(this.CB_CPUCooler);
            this.Controls.Add(this.CB_CPU);
            this.Controls.Add(this.PB_PSU);
            this.Controls.Add(this.PB_GPU);
            this.Controls.Add(this.PB_Motherboard);
            this.Controls.Add(this.PB_RAM);
            this.Controls.Add(this.PB_CPUCooler);
            this.Controls.Add(this.PB_CPU);
            this.Controls.Add(this.PB_Case);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PB_Case)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_CPUCooler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_RAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Motherboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_GPU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_PSU)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PB_Case;
        private System.Windows.Forms.PictureBox PB_CPU;
        private System.Windows.Forms.PictureBox PB_CPUCooler;
        private System.Windows.Forms.PictureBox PB_RAM;
        private System.Windows.Forms.PictureBox PB_Motherboard;
        private System.Windows.Forms.PictureBox PB_GPU;
        private System.Windows.Forms.PictureBox PB_PSU;
        private System.Windows.Forms.ComboBox CB_CPU;
        private System.Windows.Forms.ComboBox CB_CPUCooler;
        private System.Windows.Forms.ComboBox CB_RAM;
        private System.Windows.Forms.ComboBox CB_Motherboard;
        private System.Windows.Forms.ComboBox CB_GPU;
        private System.Windows.Forms.ComboBox CB_PSU;
        private System.Windows.Forms.ComboBox CB_Case;
        private System.Windows.Forms.Label LB_Price;
        private System.Windows.Forms.Label label2;
    }
}

