﻿using System.Collections.Generic;

namespace PC_Builder_mk2
{
    class CPUCooling : Hardware, ShopItems
    {
        public CPUCooling(string brand, string name, string type, double price, int fans, bool watercooling, bool rgb, string pictureName)
            : base(brand, name, type, price, pictureName)
        {
            Fans = fans;
            Watercooling = watercooling;
            RGB = rgb;

            CPUSockets.Add(CpuSockets.AM4);
            CPUSockets.Add(CpuSockets.LGA1151);
            CPUSockets.Add(CpuSockets.LGA2066);
        }
        protected List<CpuSockets> CPUSockets = new List<CpuSockets>();
        public int Fans { get; protected set; }
        public bool Watercooling { get; protected set; }
        public bool RGB { get; protected set; }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"Fans: {Fans.ToString()}\n" +
                $"Watercooling: {Tools.ConvertToYesNo(Watercooling)}\n" +
                $"RGB: {Tools.ConvertToYesNo(RGB)}\n";
        }

        
    }
}