﻿namespace PC_Builder_mk2
{
    class GPU : Hardware, ShopItems
    {
        public int DRAM { get; protected set; }
        public double BaseMhz { get; protected set; }
        public double BoostMhz { get; protected set; }
  
        public GPU(string brand, string name, string type, double price, int dram, double baseMhz, double boostMhz, string pictureName)
            :base(brand, name, type, price, pictureName)
        {
            DRAM = dram;
            BaseMhz = baseMhz;
            BoostMhz = boostMhz;
        }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"BaseMhz: {BaseMhz}\n" +
                $"boostMhz: {BoostMhz}\n" +
                $"RAM: {DRAM}\n";
        }
    }
}
