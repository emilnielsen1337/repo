﻿namespace PC_Builder_mk2
{
    class PSU : Hardware, ShopItems
    {
        public PSU(string brand, string name, string type, int wattage, double price, string pictureName)
            :base(brand, name, type, price, pictureName)
        {
            Wattage = wattage;
        }

        public override string ToString()
        {
            return $"{Brand} {Name} {Wattage} - {Price}kr";
        }

        public int Wattage { get; protected set; }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"Wattage: {Wattage}W";
        }
    }

}