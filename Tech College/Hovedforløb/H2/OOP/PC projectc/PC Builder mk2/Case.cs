﻿namespace PC_Builder_mk2
{
    class Case : Hardware, ShopItems
    {
        public Case(string brand, string name, string type, double price, int fansIncluded, int hddBays, bool sidewindow, string color, string pictureName)
            :base(brand, name, type, price, pictureName)
        {          
            FansIncluded = fansIncluded;
            HDDBays = hddBays;
            Sidewindow = sidewindow;
            Color = color;
        }
        public int FansIncluded;
        public int HDDBays;
        public bool Sidewindow;
        public string Color;

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"Fans included: {FansIncluded.ToString()}\n" +
                $"HDDBays: {HDDBays.ToString()}\n" +
                $"Sidewindow: {Tools.ConvertToYesNo(Sidewindow)}\n" +
                $"Color: {Color}";
        }
    }
}
