﻿using System.Drawing;

namespace PC_Builder_mk2
{
    class CPU : Hardware, ShopItems
    {
        public CPU(string brand, string name, string type, double price, int corecount, int threadcount, double mhz, CpuSockets socket, string pictureName)
            :base(brand, name, type, price, pictureName)
        {
            CoreCount = corecount;
            ThreadCount = threadcount;
            Mhz = mhz;
            Socket = socket;
        }
        public int CoreCount { get; protected set; }
        public int ThreadCount { get; protected set; }
        public double Mhz { get; protected set; }
        public CpuSockets Socket { get; protected set; }

        public string GetDetails()
        {
            return $"{base.ToString()}\n" +
                $"CoreCount: {CoreCount}\n" +
                $"ThreadCount: {ThreadCount}\n" +
                $"Ghz: {(Mhz / 1000).ToString()}\n" +
                $"Socket: {Socket.ToString()}";
        }
    }
}