﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrixBase_Admin
{
    class SQLHandler
    {
        SqlConnection Server;

        public SQLHandler(string Database)
        {
            Server = new SqlConnection(Database);
            Server.Open();
            Debug("Sql connection open");
        }

        public List<List<string>> GetData(string Table)
        {
            List<List<string>> Output = new List<List<string>>();

            string ColumsSQL = $"SELECT count(*)" +
                $"FROM information_schema.columns " +
                $"WHERE table_name = '{Table}'";

            string DataSQL = $"select * " +
                $"from {Table}";

            SqlDataReader AmountOfColums = new SqlCommand(ColumsSQL, Server).ExecuteReader();
            AmountOfColums.Read();

            int Colums = (int)AmountOfColums.GetValue(0);
            Debug($"Found {Colums} Colums");

            AmountOfColums.Close();

            SqlDataReader Data = new SqlCommand(DataSQL, Server).ExecuteReader();


            if (Data.HasRows)
            {
                while (Data.Read())
                {
                    List<string> tm = new List<string>();
                    for (int i = 0; i < Colums; i++)
                    {
                        tm.Add(Data.GetValue(i).ToString());
                    }
                    Output.Add(tm);
                }
            }
            else
            {
                Debug("No rows found");
            }

            return Output;
        }

        public void InsertInto(string Command)
        {
            SqlCommand cmd = new SqlCommand(Command, Server);
            cmd.ExecuteNonQuery();
        }

        public void CloseConnection()
        {
            Server.Close();
            Debug("Sql connection closed");
        }

        private void Debug(string Messege)
        {
            System.Diagnostics.Debug.WriteLine("SQLHandler: " + Messege);
        }
    }
}
