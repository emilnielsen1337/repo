﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BrixBase_Admin.Models
{
    class UsersModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            Debug(property + " Has been changed");
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void Debug(string Messege)
        {
            System.Diagnostics.Debug.WriteLine("Model: " + Messege);
        }

        private string username;
        private string password;
        private DateTime creatingDate;

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                if (username != value)
                {
                    username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (password != value)
                {
                    password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }
        public string CreatingDate
        {
            get
            {
                return creatingDate.ToLongDateString();
            }

            set
            {
                try
                {
                    DateTime time = Convert.ToDateTime(value);
                    if (time != creatingDate)
                    {
                        creatingDate = time;
                        RaisePropertyChanged("CreatingDate");
                    }
                }
                catch (Exception ex)
                {
                    Debug(ex.Message);
                }
            }
        }
    }
}
