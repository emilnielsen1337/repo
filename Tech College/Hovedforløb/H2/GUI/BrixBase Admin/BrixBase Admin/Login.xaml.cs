﻿using BrixBase_Admin.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrixBase_Admin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            username.Focus();
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            // SQL Connection with login
            SqlConnection conn = new SqlConnection(@"Data Source=BRIX-LAPTOP;User ID=brix;Password=shadow12;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=AdminDB");
            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                String query = "SELECT COUNT(1) FROM admins WHERE username=@username AND password=@password";
                SqlCommand sqlCmd = new SqlCommand(query, conn);
                sqlCmd.CommandType = System.Data.CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@username", username.Text);
                sqlCmd.Parameters.AddWithValue("@password", password.Password);
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());

                if(count == 1)
                {
                    AdminPanel admin = new AdminPanel();
                    App.Current.MainWindow = admin;
                    this.Close();
                    admin.Show();
                } else
                {
                    MessageBox.Show("Username or password is incorrect.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                conn.Close();
            }
        }
       
        //Enable enter button when logging in
        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnSubmit_Click(this, null);
            }
        }
    }
}
