﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Diagnostics;
using BrixBase_Admin.Modelviews;

namespace BrixBase_Admin.Views
{
    /// <summary>
    /// Interaction logic for AdminPanel.xaml
    /// </summary>
    public partial class AdminPanel : Window
    {
     
        public AdminPanel()
        {
            InitializeComponent();

            UsersViewModel users = new UsersViewModel();

            Admins.DataContext = users;

            SQLHandler sql = new SQLHandler(@"Data Source=BRIX-LAPTOP;User ID=brix;Password=shadow12;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=AdminDB");

            List<List<string>> vs = new List<List<string>>();

            vs = sql.GetData("admins");
        }

        public object Users { get; private set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
        }
    }
}
