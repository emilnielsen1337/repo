﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using BrixBase_Admin.Models;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Windows;

namespace BrixBase_Admin.Modelviews
{
    class UsersViewModel
    {
        public MyICommand Adduser { get; set; }
        public MyICommand DeleteItem { get; set; }
        private UsersModel selectedItem { get; set; }

        SqlConnection conn = new SqlConnection(@"Data Source=BRIX-LAPTOP;User ID=brix;Password=shadow12;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Initial Catalog=AdminDB");

        public UsersModel SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                DeleteItem.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<UsersModel> Users { get; set; }

        public string addusername { get; set; }
        public string addpassword { get; set; }

        public UsersViewModel()
        {
            Users = new ObservableCollection<UsersModel>();
            Adduser = new MyICommand(adduser);
            DeleteItem = new MyICommand(OnDelete, CanDelete);

            conn.Open();
            SqlDataReader dr = new SqlCommand("SELECT * FROM admins", conn).ExecuteReader();

            while (dr.Read())
            {
                Users.Add(new UsersModel { Username = dr.GetString(0), Password = dr.GetString(1), CreatingDate = dr[2].ToString() });
            }
        }

        private void adduser()
        {
            string query = $"INSERT INTO admins (username, password, datetime) VALUES ('{addusername}', '{addpassword}', Getdate())";
            SqlCommand cmd = new SqlCommand(query, conn);

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            cmd.ExecuteNonQuery();
            conn.Close();
           
            Debug.WriteLine($"User {addusername} added!");
            MessageBox.Show($"User {addusername} added!");
        }

        private bool CanDelete()
        {
            return SelectedItem != null;
        }

        private void OnDelete()
        {
            Users.Remove(SelectedItem);
        }

    }
}
