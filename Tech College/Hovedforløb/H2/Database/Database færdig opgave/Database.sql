USE [master]
GO
/****** Object:  Database [BrixH2]    Script Date: 2/12/2019 11:17:19 AM ******/
CREATE DATABASE [BrixH2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BrixH2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BrixH2.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BrixH2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BrixH2_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BrixH2] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BrixH2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BrixH2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BrixH2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BrixH2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BrixH2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BrixH2] SET ARITHABORT OFF 
GO
ALTER DATABASE [BrixH2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BrixH2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BrixH2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BrixH2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BrixH2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BrixH2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BrixH2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BrixH2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BrixH2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BrixH2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BrixH2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BrixH2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BrixH2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BrixH2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BrixH2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BrixH2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BrixH2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BrixH2] SET RECOVERY FULL 
GO
ALTER DATABASE [BrixH2] SET  MULTI_USER 
GO
ALTER DATABASE [BrixH2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BrixH2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BrixH2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BrixH2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BrixH2] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BrixH2', N'ON'
GO
ALTER DATABASE [BrixH2] SET QUERY_STORE = OFF
GO
USE [BrixH2]
GO
/****** Object:  Table [dbo].[Karakterer]    Script Date: 2/12/2019 11:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Karakterer](
	[id] [int] NOT NULL,
	[karakter] [int] NOT NULL,
	[ECTS] [char](10) NOT NULL,
 CONSTRAINT [PK_Karakterer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[karakterer/elev]    Script Date: 2/12/2019 11:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[karakterer/elev](
	[elev] [int] NULL,
	[karakterer] [int] NULL,
	[fag] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Elev]    Script Date: 2/12/2019 11:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Elev](
	[id] [int] NOT NULL,
	[fornavn] [varchar](50) NOT NULL,
	[efternavn] [varchar](50) NOT NULL,
	[initialer] [char](3) NULL,
	[hold] [varchar](50) NOT NULL,
	[alder] [int] NOT NULL,
	[adresse] [varchar](50) NOT NULL,
	[by] [varchar](50) NOT NULL,
	[telefonnummer] [float] NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK_Elev] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[HighestGrade]    Script Date: 2/12/2019 11:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[HighestGrade] AS 
SELECT fornavn, efternavn, hold, karakter, ECTS 
FROM [karakterer/elev]
join Elev ON Elev.id=[karakterer/elev].elev
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
WHERE karakterer < 1
GO
/****** Object:  Table [dbo].[elev/hold]    Script Date: 2/12/2019 11:17:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[elev/hold](
	[Elev] [int] NOT NULL,
	[hold] [int] NOT NULL,
 CONSTRAINT [PK_elev/hold] PRIMARY KEY CLUSTERED 
(
	[Elev] ASC,
	[hold] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fag]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fag](
	[id] [int] NOT NULL,
	[Navn] [varchar](50) NOT NULL,
	[niveau] [char](10) NOT NULL,
	[lærer] [int] NOT NULL,
 CONSTRAINT [PK_Fag] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[History]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[date] [datetime] NULL,
	[event] [nchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hold]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hold](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[holdnavn] [varchar](50) NOT NULL,
	[lokale] [int] NOT NULL,
 CONSTRAINT [PK_Hold] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hold/Fag]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hold/Fag](
	[Hold] [int] NOT NULL,
	[Fag] [int] NOT NULL,
 CONSTRAINT [PK_Hold/Fag] PRIMARY KEY CLUSTERED 
(
	[Hold] ASC,
	[Fag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lærer]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lærer](
	[id] [int] NOT NULL,
	[fornavn] [varchar](50) NOT NULL,
	[efternavn] [varchar](50) NOT NULL,
	[alder] [varchar](50) NOT NULL,
	[adresse] [varchar](50) NOT NULL,
	[by] [varchar](50) NOT NULL,
	[telefonnummer] [int] NOT NULL,
	[email] [varchar](50) NULL,
 CONSTRAINT [PK_Lærer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lærer/fag]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lærer/fag](
	[lærer] [int] NOT NULL,
	[fag] [int] NOT NULL,
 CONSTRAINT [PK_lærer/fag] PRIMARY KEY CLUSTERED 
(
	[lærer] ASC,
	[fag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lektioner]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lektioner](
	[Tidspunkt] [datetime] NOT NULL,
	[hold] [int] NOT NULL,
	[Fag] [int] NULL,
	[lærer] [int] NOT NULL,
 CONSTRAINT [PK_Lektioner] PRIMARY KEY CLUSTERED 
(
	[Tidspunkt] ASC,
	[hold] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lokale]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lokale](
	[lokalenr] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Lokale] PRIMARY KEY CLUSTERED 
(
	[lokalenr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SysLog]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[getdate] [datetime] NULL,
	[comment] [nvarchar](50) NULL,
 CONSTRAINT [PK_SysLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (0, N'Emil', N'Brix Nielsen ', N'EBN', N'PD01', 22, N'Tennisskoven 21', N'Hjallerup', 52304517, N'emilnielsen1337@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (1, N'Thomas', N'Thomassen', N'TT ', N'PD01', 43, N'Vestergade 39', N'Hjallerup', 87432421, N'Thomasthomassen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (2, N'Frank ', N'Franksen', N'FF ', N'PD01', 20, N'Østergade 42', N'Hjallerup', 87654321, N'Frankfranksen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (3, N'Benjamin  ', N'Benjaminsen', N'BB ', N'PD01', 18, N'Rævdalsparken 24', N'Dronninglund', 76352132, N'Benjaminbenjaminsen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (4, N'Lars ', N'Larsen', N'LL ', N'PD02', 32, N'Supervej 69 ', N'Superby', 87543902, N'Larslarsen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (5, N'Jesper', N'Jespersen', N'JJ ', N'PD02', 42, N'Hedevej 69', N'Hedeby', 69845582, N'Jesperjespersen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (6, N'Ida', N'Idasen', N'II ', N'PD02', 21, N'Falkevej 76', N'Aalborg', 41236587, N'Idaidasen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (7, N'Emma', N'Emmasen', N'EE ', N'PD02', 28, N'Falkevej 21', N'Aalborg', 74896532, N'Emmaemmasen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (8, N'Kristina', N'Kristinasen', N'KK ', N'PD03', 25, N'Hejrevej 42', N'Aalborg', 52694159, N'Kristinakristinasen@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (9, N'Zayden', N'Lugo', N'ZL ', N'PD03', 45, N'Hejrevej 11', N'Aalborg', 45256325, N'ZaydenL@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (10, N'Maverick', N'Robson', N'MR ', N'PD03', 23, N'Vibevej 28', N'Aalborg', 14465212, N'MaverickRob@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (11, N'Armani', N'Kay', N'AK ', N'PD03', 31, N'Vibevej 52', N'Aalborg', 54156421, N'ArmaniKay@gmail.com')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (12, N'Tobiass', N'STEFF', N'TS ', N'PD01', 83, N'The moon 2', N'Space', 1337, N'notamail@mail.dk')
INSERT [dbo].[Elev] ([id], [fornavn], [efternavn], [initialer], [hold], [alder], [adresse], [by], [telefonnummer], [Email]) VALUES (13, N'Gert', N'Gertsen', N'GG ', N'PD02', 16, N'The sun 5', N'Space', 88888888, N'GG@gmail.com')
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (0, N'Dansk', N'A         ', 6)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (1, N'Matematik', N'A         ', 4)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (2, N'Engelsk', N'A         ', 8)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (3, N'Tysk', N'C         ', 0)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (4, N'Fysik', N'B         ', 2)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (5, N'Biologi', N'C         ', 5)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (6, N'IT', N'A         ', 1)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (7, N'Billedekunst', N'C         ', 7)
INSERT [dbo].[Fag] ([id], [Navn], [niveau], [lærer]) VALUES (8, N'Religion', N'B         ', 3)
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-04T13:54:53.580' AS DateTime), N'fsd sdfs Inserted                                 ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:28:35.090' AS DateTime), N'Ida Idasen Inserted                               ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:29:24.867' AS DateTime), N'Emma Emmasen Inserted                             ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:30:27.550' AS DateTime), N'Kristina Kristinasen Inserted                     ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:34:06.773' AS DateTime), N'Zayden Lugo Inserted                              ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:35:59.760' AS DateTime), N'Maverick Robson Inserted                          ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-01-14T08:38:07.670' AS DateTime), N'Armani Kay Inserted                               ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-02-08T10:55:35.087' AS DateTime), N'Tobias STEFF Inserted                             ')
INSERT [dbo].[History] ([date], [event]) VALUES (CAST(N'2019-02-12T10:48:48.240' AS DateTime), N'Gert Gertsen Inserted                             ')
SET IDENTITY_INSERT [dbo].[Hold] ON 

INSERT [dbo].[Hold] ([id], [holdnavn], [lokale]) VALUES (1, N'PD01', 0)
INSERT [dbo].[Hold] ([id], [holdnavn], [lokale]) VALUES (2, N'PD02', 1)
INSERT [dbo].[Hold] ([id], [holdnavn], [lokale]) VALUES (3, N'PD03', 2)
SET IDENTITY_INSERT [dbo].[Hold] OFF
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (0, 12, N'A         ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (1, 10, N'B         ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (2, 7, N'C         ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (3, 4, N'D         ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (4, 2, N'E         ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (5, 20, N'FX        ')
INSERT [dbo].[Karakterer] ([id], [karakter], [ECTS]) VALUES (6, -3, N'F         ')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (0, 1, N'Dansk')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (0, 2, N'Matematik')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (0, 3, N'Biologi')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (0, 0, N'IT')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (1, 2, N'Fysik')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (1, 3, N'Engelsk')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (1, 4, N'Religion')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (1, 0, N'Dansk')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (2, 0, N'Engelsk')
INSERT [dbo].[karakterer/elev] ([elev], [karakterer], [fag]) VALUES (2, 0, N'Fysik')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (0, N'Bent', N'Bentsen', N'54', N'Jensvej 221', N'Aalborg', 98746325, N'Bentbentsen@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (1, N'Per', N'Persen', N'40', N'Skolevej 23', N'Aalborg', 41235874, N'Perpersen@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (2, N'Ole', N'Olesen', N'53', N'Solbærvej 52', N'Aalborg', 98780967, N'Oleolesen@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (3, N'Christian', N'Christiansen', N'27', N'Nytovet 43', N'Aalborg', 22314515, N'Christianchristiansen@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (4, N'Ebbe', N'Ebbesen', N'60', N'Tennisskoven 21', N'Hjallerup', 87356217, N'Ebbeebbesen@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (5, N'Frederik', N'Frederiksen', N'33', N'Dalgasgade 65', N'Aalborg', 87656789, N'FF@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (6, N'Lærke', N'Lærkesen', N'32', N'Dalgasgade 22', N'Aalborg', 97868822, N'LL@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (7, N'Aksel', N'Akselsen', N'40', N'Annebergvej 87', N'Aalborg', 24353456, N'AA@gmail.com')
INSERT [dbo].[Lærer] ([id], [fornavn], [efternavn], [alder], [adresse], [by], [telefonnummer], [email]) VALUES (8, N'Villum', N'Villumsen', N'47', N'Annebergvej 11', N'Aalborg', 64563853, N'VV@gmail.com')
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T08:15:00.000' AS DateTime), 1, 5, 5)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T08:15:00.000' AS DateTime), 2, 0, 6)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T09:15:00.000' AS DateTime), 1, 6, 1)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T09:15:00.000' AS DateTime), 2, 1, 4)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T10:15:00.000' AS DateTime), 1, 7, 7)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T10:15:00.000' AS DateTime), 2, 2, 8)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T11:15:00.000' AS DateTime), 1, 8, 3)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T11:15:00.000' AS DateTime), 2, 3, 0)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T12:15:00.000' AS DateTime), 1, 0, 6)
INSERT [dbo].[Lektioner] ([Tidspunkt], [hold], [Fag], [lærer]) VALUES (CAST(N'2019-01-14T12:15:00.000' AS DateTime), 2, 4, 2)
INSERT [dbo].[Lokale] ([lokalenr]) VALUES (N'0')
INSERT [dbo].[Lokale] ([lokalenr]) VALUES (N'1')
INSERT [dbo].[Lokale] ([lokalenr]) VALUES (N'2')
SET IDENTITY_INSERT [dbo].[SysLog] ON 

INSERT [dbo].[SysLog] ([id], [getdate], [comment]) VALUES (1, CAST(N'2019-02-08T10:55:35.090' AS DateTime), N'Tobias STEFF Inserted')
INSERT [dbo].[SysLog] ([id], [getdate], [comment]) VALUES (6, CAST(N'2019-02-08T11:02:00.363' AS DateTime), N'Tobiass STEFF PD01 83 notamail@mail.dk Updated')
INSERT [dbo].[SysLog] ([id], [getdate], [comment]) VALUES (7, CAST(N'2019-02-12T10:48:48.243' AS DateTime), N'Gert Gertsen Inserted BRIX-LAPTOP\Emil-PC')
SET IDENTITY_INSERT [dbo].[SysLog] OFF
ALTER TABLE [dbo].[elev/hold]  WITH CHECK ADD FOREIGN KEY([hold])
REFERENCES [dbo].[Hold] ([id])
GO
ALTER TABLE [dbo].[elev/hold]  WITH CHECK ADD  CONSTRAINT [FK_elev/hold_Elev] FOREIGN KEY([Elev])
REFERENCES [dbo].[Elev] ([id])
GO
ALTER TABLE [dbo].[elev/hold] CHECK CONSTRAINT [FK_elev/hold_Elev]
GO
ALTER TABLE [dbo].[Fag]  WITH CHECK ADD  CONSTRAINT [FK_Fag_Lærer] FOREIGN KEY([lærer])
REFERENCES [dbo].[Lærer] ([id])
GO
ALTER TABLE [dbo].[Fag] CHECK CONSTRAINT [FK_Fag_Lærer]
GO
ALTER TABLE [dbo].[Hold/Fag]  WITH CHECK ADD FOREIGN KEY([Hold])
REFERENCES [dbo].[Hold] ([id])
GO
ALTER TABLE [dbo].[Hold/Fag]  WITH CHECK ADD  CONSTRAINT [FK_Hold/Fag_Fag] FOREIGN KEY([Fag])
REFERENCES [dbo].[Fag] ([id])
GO
ALTER TABLE [dbo].[Hold/Fag] CHECK CONSTRAINT [FK_Hold/Fag_Fag]
GO
ALTER TABLE [dbo].[karakterer/elev]  WITH CHECK ADD  CONSTRAINT [FK_karakterer/elev_Elev] FOREIGN KEY([elev])
REFERENCES [dbo].[Elev] ([id])
GO
ALTER TABLE [dbo].[karakterer/elev] CHECK CONSTRAINT [FK_karakterer/elev_Elev]
GO
ALTER TABLE [dbo].[karakterer/elev]  WITH CHECK ADD  CONSTRAINT [FK_karakterer/elev_Karakterer] FOREIGN KEY([karakterer])
REFERENCES [dbo].[Karakterer] ([id])
GO
ALTER TABLE [dbo].[karakterer/elev] CHECK CONSTRAINT [FK_karakterer/elev_Karakterer]
GO
ALTER TABLE [dbo].[lærer/fag]  WITH CHECK ADD  CONSTRAINT [FK_lærer/fag_Fag] FOREIGN KEY([fag])
REFERENCES [dbo].[Fag] ([id])
GO
ALTER TABLE [dbo].[lærer/fag] CHECK CONSTRAINT [FK_lærer/fag_Fag]
GO
ALTER TABLE [dbo].[lærer/fag]  WITH CHECK ADD  CONSTRAINT [FK_lærer/fag_Lærer] FOREIGN KEY([lærer])
REFERENCES [dbo].[Lærer] ([id])
GO
ALTER TABLE [dbo].[lærer/fag] CHECK CONSTRAINT [FK_lærer/fag_Lærer]
GO
ALTER TABLE [dbo].[Lektioner]  WITH CHECK ADD  CONSTRAINT [FK_Lektioner_Fag] FOREIGN KEY([Fag])
REFERENCES [dbo].[Fag] ([id])
GO
ALTER TABLE [dbo].[Lektioner] CHECK CONSTRAINT [FK_Lektioner_Fag]
GO
ALTER TABLE [dbo].[Lektioner]  WITH CHECK ADD  CONSTRAINT [FK_Lektioner_Lærer] FOREIGN KEY([lærer])
REFERENCES [dbo].[Lærer] ([id])
GO
ALTER TABLE [dbo].[Lektioner] CHECK CONSTRAINT [FK_Lektioner_Lærer]
GO
/****** Object:  StoredProcedure [dbo].[DatabaseBackup]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DatabaseBackup]
AS
BEGIN
	BACKUP DATABASE testDB
	TO DISK = 'C:\DatabaseBackup\BrixDB.bak'
END;
GO
/****** Object:  StoredProcedure [dbo].[LærerSkema]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LærerSkema]
@Navn VARCHAR(50)
AS 
SELECT fornavn, efternavn, holdnavn, lokale, Navn, niveau, Tidspunkt
FROM Lektioner
join Hold ON Hold.id=Lektioner.hold
join Fag ON Fag.id=Lektioner.Fag
join Lærer ON Lærer.id=Fag.lærer
WHERE fornavn like @Navn + '%';
GO
/****** Object:  Trigger [dbo].[ElevInserted]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[ElevInserted] ON [dbo].[Elev] AFTER INSERT
AS 
BEGIN
	insert into History values (getdate(), (select fornavn + ' ' + efternavn from inserted) + ' Inserted')
END
GO
ALTER TABLE [dbo].[Elev] ENABLE TRIGGER [ElevInserted]
GO
/****** Object:  Trigger [dbo].[Inserted]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Inserted] ON [dbo].[Elev] AFTER INSERT
AS 
BEGIN
	insert into SysLog values (getdate(), (select fornavn + ' ' + efternavn from inserted) + ' Inserted ' +  SUSER_NAME())
END
GO
ALTER TABLE [dbo].[Elev] ENABLE TRIGGER [Inserted]
GO
/****** Object:  Trigger [dbo].[Updated]    Script Date: 2/12/2019 11:17:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Updated] ON [dbo].[Elev] AFTER Update
AS 
BEGIN
	insert into SysLog values (getdate(), (select fornavn + ' ' + efternavn + ' ' + hold + ' ' + CAST(alder as varchar(10)) + ' ' + Email from inserted) + ' Updated')
END
GO
ALTER TABLE [dbo].[Elev] ENABLE TRIGGER [Updated]
GO
USE [master]
GO
ALTER DATABASE [BrixH2] SET  READ_WRITE 
GO
