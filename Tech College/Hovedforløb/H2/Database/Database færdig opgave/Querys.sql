SELECT fornavn, efternavn, initialer, fag, karakter, ECTS 
FROM [karakterer/elev]
join Elev ON Elev.id=[karakterer/elev].elev
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
order by initialer;
/* 
	Formålet med denne forspørgsel er at man kan se hvem der givet karakterer til,
	og hvilke karakterer de har fået i de forskellige fag.
*/

SELECT Tidspunkt, holdnavn, lokale, Navn, niveau, fornavn, efternavn 
FROM Lektioner
join Hold ON Hold.id=Lektioner.hold
join Fag ON Fag.id=Lektioner.Fag
join Lærer ON Lærer.id=Fag.lærer
order by holdnavn;
/* 
	Med denne forespørgsel kan man se hvor og hvornår de forskellige lærere skal undervise.
*/

SELECT fornavn, efternavn, initialer, hold, alder, adresse, [by], telefonnummer, Email, lokale, fag, karakter, ECTS 
FROM Elev
join Hold ON hold.holdnavn=Elev.hold
join [karakterer/elev] ON [karakterer/elev].elev=elev.id
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
where fornavn = 'Emil';

SELECT fornavn, efternavn, initialer, hold, alder, adresse, [by], telefonnummer, Email, SUM(karakter) / COUNT(karakter) AS 'Karaktergennemsnit' 
FROM Elev
join Hold ON hold.holdnavn=Elev.hold
join [karakterer/elev] ON [karakterer/elev].elev=elev.id
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
where fornavn = 'Emil' AND efternavn = 'Brix Nielsen'
GROUP BY fornavn, efternavn, initialer, hold, alder, adresse, [by], telefonnummer, Email
/* 
	Formålet med denne forespørgsel er at man kan søge på den enkelte person, 
	og se alle informationer vi har om vedkommende.
*/

ALTER PROC LærerSkema
@Navn VARCHAR(50)
AS 
SELECT fornavn, efternavn, holdnavn, lokale, Navn, niveau, Tidspunkt
FROM Lektioner
join Hold ON Hold.id=Lektioner.hold
join Fag ON Fag.id=Lektioner.Fag
join Lærer ON Lærer.id=Fag.lærer
WHERE fornavn like @Navn + '%';

EXECUTE LærerSkema @Navn = 'Ebbe';
/* 
	Formålet med denne procedure er at man kan søge efter en specifik lærer,
	og kan se hvor og hvornår personen skal undervise.
*/
 
ALTER PROC DatabaseBackup
AS
BEGIN
	BACKUP DATABASE testDB
	TO DISK = 'C:\DatabaseBackup\'
END;
/* 
	Formålet med denne procedure er at man nemt kan tage en backup af sin database.
*/
EXECUTE DatabaseBackup;


ALTER TRIGGER Updated ON Elev AFTER Update
AS 
BEGIN
	insert into SysLog values (getdate(), (select fornavn + ' ' + efternavn + ' ' + hold + ' ' + CAST(alder as varchar(10)) + ' ' + Email from inserted) + ' Updated')
END
GO
/* 
	Hver gang der bliver ændret i Elev tabellen vil den logge ændringerne i min SysLog tabel.
*/

ALTER TRIGGER Inserted ON Elev AFTER INSERT
AS 
BEGIN
	insert into SysLog values (getdate(), (select fornavn + ' ' + efternavn from inserted) + ' Inserted ' +  SUSER_NAME())
END
GO
/* 
	Hver gang der bliver tilføjet en ny elev i elev tabellen, vil det også blive logged i SysLog tabellen. Den vil også skrive hvem der har lavet ændringen-
*/

ALTER VIEW HighestGrade AS 
SELECT fornavn, efternavn, hold, karakter, ECTS 
FROM [karakterer/elev]
join Elev ON Elev.id=[karakterer/elev].elev
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
WHERE karakterer < 1
/* 
	Formålet med dette view er at man se hvilke personer som har fået 12 i de forskellige fag.
*/


