CREATE TRIGGER ElevInserted ON Elev AFTER INSERT
AS 
BEGIN
	insert into History values (getdate(), (select fornavn + ' ' + efternavn from inserted) + ' Inserted')
END
GO