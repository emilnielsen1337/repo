SELECT karakter, ECTS, fag, fornavn, efternavn, initialer FROM [karakterer/elev]
join Elev ON Elev.id=[karakterer/elev].elev
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
order by initialer;

SELECT Tidspunkt, holdnavn, lokale, Navn, niveau, fornavn, efternavn FROM Lektioner
join Hold ON Hold.id=Lektioner.hold
join Fag ON Fag.id=Lektioner.Fag
join Lærer ON Lærer.id=Fag.lærer
order by holdnavn;

SELECT fornavn, efternavn, initialer, hold, alder, adresse, [by], telefonnummer, Email, lokale, fag, karakter, ECTS FROM Elev
join Hold ON hold.holdnavn=Elev.hold
join [karakterer/elev] ON [karakterer/elev].elev=elev.id
join Karakterer ON karakterer.id=[karakterer/elev].karakterer
where fornavn = 'Emil';

ALTER PROC LærerSkema
@Navn VARCHAR(50)
AS 
SELECT Tidspunkt, holdnavn, lokale, Navn, niveau, fornavn, efternavn FROM Lektioner
join Hold ON Hold.id=Lektioner.hold
join Fag ON Fag.id=Lektioner.Fag
join Lærer ON Lærer.id=Fag.lærer
WHERE fornavn = @Navn; 

