<?php
    session_start();
    if(isset($_SESSION['login_user'])) {
        unset($_SESSION['login_user']);
    }
    if(isset($_SESSION['LastPage'])) {
        unset($_SESSION['LastPage']);
    }
    
    if(isset($_SESSION['message'])) {
        unset($_SESSION['message']);
    }

    if(isset($_SESSION['isadmin'])) {
        unset($_SESSION['isadmin']);
    }
    
    unset($_SESSION['']);
    header("location: ../home.php");
?>