<?php
   require_once "Server.php";
   session_start();
   if($_SERVER["REQUEST_METHOD"] == "POST") 
   {
      $username = mysqli_real_escape_string($conn, $_POST['username']);
      $password = mysqli_real_escape_string($conn, $_POST['password']); 
   
      $checkhash = "SELECT Password FROM Users WHERE UserName = '$username'";
      $result = $conn->query($checkhash);
      $row = $result->fetch_assoc();

      if(empty($username) ||  empty($password))
      {
         header("location: ../login.php");
         exit();
      }

      if(password_verify($password, $row["Password"]))
      {
         $_SESSION['login_user'] = $username;

         $sql = "select Admin from Users where UserName='$username'";
         $result = $conn->query($sql);
         $row = $result->fetch_assoc();
         $IsAdmin = $row['Admin'];

         if($IsAdmin === "yes")
         {
            $_SESSION['isadmin'] = true;
         }
         else
         {
            $_SESSION['isadmin'] = false;
         }

         header("location: ../shop.php");
      } 
      else 
      {
         $_SESSION['message'] = "Your Login Name or Password is invalid";
         $_SESSION['LastPage'] = "login.php";
         
         header("location: ../status.php");
         exit();
      }
    }
?>