<?php 
$message = "";
if(isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
} 

$page = "index.php";
if(isset($_SESSION['LastPage'])) {
    $page = $_SESSION['LastPage'];
    unset($_SESSION['LastPage']);
}

$loading = "5";

header("refresh:$loading;url=$page");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="Stylesheets/stylesheet.css">
    <link rel="stylesheet" href="Stylesheets/styleStatus.css">
    <script type="text/javascript" src="Scripts\main.js"></script>
    <title></title>
</head>
<body>
    <h1><?php echo $message; ?></h1>
    <h5 id="timer">This page will redirect in <?php echo $loading ?> seconds...</h5>
    <p></p>
    <span id="time"></span>
    <form action="<?php echo $page; ?>">
    <input class="button" type="submit" value="Redirect now" />
    </form>
</body>
</html>
