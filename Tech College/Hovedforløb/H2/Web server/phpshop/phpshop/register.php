<?php 
    include "header.php";
    require_once "Scripts/Server.php";

    if(!isset($_SESSION['login_user']) || $_SESSION['isadmin'] === false)
    {
      header("location: ../shop.php");
      exit();
    }
?>
  <img src="Pictures\Logo.png" alt="Logo" class="center">
    <div>
    <form id="form" action="Scripts\registerscript.php" method="POST">
        <div class="form-group">
          <label class="label" for="formGroupExampleInput">Username:</label>
          <input type="text" class="input" name="username" placeholder="">
        </div>
        <div class="form-group" id="isadmin">
          <label class="label" for="formGroupExampleInput2">Password: </label>
          <input type="password" class="input" name="password" placeholder=""> <br>
          <label class="label" for="formGroupExampleInput2">Retype password: </label>
          <input type="password" class="input" name="password2" placeholder="">
          <p></p>
          <p></p>
          <label class="container">Admin
            <input type="checkbox" name="isadmin">
            <span class="checkmark"></span>
          </label>

          <button class="button" type="submit">Create</button>
        </div>
      </form>
    </div>
</body>
</html>