﻿let ButtonClickNumber: number = 0;
let ButtonClickArray = [];

class ButtonClickActivity {
    public ButtonClickNumber: number;
    public ButtonClickTime: Date;

    constructor(params: { ButtonClickNumber: number }) {
        this.ButtonClickNumber = params.ButtonClickNumber;
        this.ButtonClickTime = new Date();
    }

    public GetButtonClickTime() {
        return (this.ButtonClickTime);
    }

    // Nedennævnte funktion erklæring viser, at Function Overload ikke er mulig i TypeScript !!!
    //public GetButtonClickTime(Test : number) {
    //    return (this.ButtonClickTime);
    //}

    public GetButtonClickTimeAsString() {

    }

    public toString = (): string => {
        return `(${this.ButtonClickNumber})`;
    }
}

function DoTheShitTypeScript() {
    ButtonClickNumber++;
    ButtonClickArray.push(new ButtonClickActivity({ ButtonClickNumber: ButtonClickNumber}));
}

function GetLastButtonActivity() : string {
    return (ButtonClickArray[ButtonClickArray.length - 1]);
}