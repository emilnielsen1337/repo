var ButtonClickNumber = 0;
var ButtonClickArray = [];
var ButtonClickActivity = /** @class */ (function () {
    function ButtonClickActivity(params) {
        var _this = this;
        this.toString = function () {
            return "(" + _this.ButtonClickNumber + ")";
        };
        this.ButtonClickNumber = params.ButtonClickNumber;
        this.ButtonClickTime = new Date();
    }
    ButtonClickActivity.prototype.GetButtonClickTime = function () {
        return (this.ButtonClickTime);
    };
    // Nedennævnte funktion erklæring viser, at Function Overload ikke er mulig i TypeScript !!!
    //public GetButtonClickTime(Test : number) {
    //    return (this.ButtonClickTime);
    //}
    ButtonClickActivity.prototype.GetButtonClickTimeAsString = function () {
    };
    return ButtonClickActivity;
}());
function DoTheShitTypeScript() {
    ButtonClickNumber++;
    ButtonClickArray.push(new ButtonClickActivity({ ButtonClickNumber: ButtonClickNumber }));
}
function GetLastButtonActivity() {
    return (ButtonClickArray[ButtonClickArray.length - 1]);
}
//# sourceMappingURL=TypeScriptStuff.js.map