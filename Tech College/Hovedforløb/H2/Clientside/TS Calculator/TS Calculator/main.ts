﻿let facit;

function plus() {
    let tal1: number = parseFloat($("#tal1").val());
    let tal2: number = parseFloat($("#tal2").val());

    facit = tal1 + tal2;
    let lblfacit = `${tal1} * ${tal2} = ${facit}`;

    Facit();
}

function minus() {
    let tal1: number = parseFloat($("#tal1").val());
    let tal2: number = parseFloat($("#tal2").val());

    facit = tal1 - tal2;
    let lblfacit = `${tal1} * ${tal2} = ${facit}`;

    Facit();
}

function gange() {
    let tal1: number = parseFloat($("#tal1").val());
    let tal2: number = parseFloat($("#tal2").val());

    facit = tal1 * tal2;
    let lblfacit = `${tal1} * ${tal2} = ${facit}`;

    Facit();
}

function divider() {
    let tal1: number = parseFloat($("#tal1").val());
    let tal2: number = parseFloat($("#tal2").val());
    
    facit = tal1 / tal2;
    let lblfacit = `${tal1} * ${tal2} = ${facit}`;

    Facit();
}

function Facit() {
    let tal1: number = parseFloat($("#tal1").val());
    let tal2: number = parseFloat($("#tal2").val());

    facit = tal1 * tal2;
    let lblfacit = `${tal1} * ${tal2} = ${facit}`;

    $("#facitlabel").text(facit);
}