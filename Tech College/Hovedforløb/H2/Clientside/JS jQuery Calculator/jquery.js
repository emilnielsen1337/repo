$(document).ready(function () {
    var tal1 = parseInt($("#tal1").val());
    var tal2 = parseInt($("#tal2").val());
    select = document.getElementById('select');
    option = document.createElement('option');

    $("#plus").click(plus);
    $("#minus").click(minus);
    $("#gange").click(gange);
    $("#divider").click(divider);

    function plus() {
        if (tal1 || tal2 == "") {
            facit = tal1 + tal2;
            lblfacit = `${tal1} * ${tal2} = ${facit}`;
            Facit();
        } else {
            alert("Failed, Check your inputs")
        }
    }

    function minus() {
        if (tal1 || tal2 == "") {
            facit = tal1 - tal2;
            lblfacit = `${tal1} * ${tal2} = ${facit}`;
            Facit();
        } else {
            alert("Failed, Check your inputs")
        }
    }

    function gange() {
        if (tal1 || tal2 == "") {
            facit = tal1 * tal2;
            lblfacit = `${tal1} * ${tal2} = ${facit}`;
            Facit()
        } else {
            alert("Failed, Check your inputs")
        }
    }

    function divider() {
        if (tal1 || tal2 == "") {
            facit = tal1 / tal2;
            lblfacit = `${tal1} * ${tal2} = ${facit}`;
            Facit();
        } else {
            alert("Failed, Check your inputs")
        }
    }

    function Facit() {
        $("#facitlabel").text(facit);
            option.text = lblfacit;
            select.add(option);
    }
});