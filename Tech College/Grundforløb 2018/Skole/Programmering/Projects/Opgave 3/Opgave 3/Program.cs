﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string Fornavn;
            string Efternavn;
            string By;
            Int32 postnummer;
            Int32 klassenummer;


            Console.WriteLine("Indtast dit fornavn");
            Fornavn = Console.ReadLine();
            Console.WriteLine("Indtast dit efternavn");
            Efternavn = Console.ReadLine();
            Console.WriteLine("Hvilken by bor du i?");
            By = Console.ReadLine();
            Console.WriteLine("Hvad er byens postnummer?");
            postnummer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Indtast dit klassenummer");
            klassenummer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Hej " + Fornavn + " " + Efternavn + "!");
            Console.WriteLine("Du bor i " + By + ", " + postnummer + ".");
            Console.WriteLine("Dit klassenummer er: " + klassenummer);
            Console.ReadKey();
        }
    }
}
