﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VariabelBegreb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalculateAreaSquare_Click(object sender, RoutedEventArgs e)
        {
            double AreaSquare;
            double PerimeterSquare;
            double LengthSquare = Convert.ToDouble(txtLengthSquare.Text);
            double WidthSquare = Convert.ToDouble(txtWidthSquare.Text);
            double w = WidthSquare;
            // For at vise, at man også kan bruge korte variabelnavne.
            // Det kan ikke anbefales, da meningen med en vaiabel bliver
            // meget sværer at gennemskue !!!
            AreaSquare = LengthSquare * WidthSquare;

            //PerimeterSquare = Length + Length + Width + Width;
            //PerimeterSquare = 2 * Length + 2 * Width;
            PerimeterSquare = 2 * (LengthSquare + WidthSquare);

            txtAreaSquare.Text = AreaSquare.ToString();
            txtPerimeterSquare.Text = PerimeterSquare.ToString();
        }

        private void btnClearResult_Click(object sender, RoutedEventArgs e)
        {
            txtLengthSquare.Text = "";
            txtWidthSquare.Text = "";
            txtAreaSquare.Text = "";
            txtPerimeterSquare.Text = "";
        }

        private void btnClearCircleParameters_Click(object sender, RoutedEventArgs e)
        {
            txtAreaCircle.Text = "";
            txtRadiusCircle.Text = "";
        }

        private void btnCalculateCircleParameters_Click(object sender, RoutedEventArgs e)
        {
            double AreaCircle;
            double PerimeterCircle;
            double RadiusCircle = Convert.ToDouble(txtRadiusCircle.Text);

            AreaCircle = Math.PI * Math.Pow(RadiusCircle, 2);
            PerimeterCircle = Math.PI * RadiusCircle * 2;
            txtAreaCircle.Text = AreaCircle.ToString();
            txtPerimeterCircle.Text = PerimeterCircle.ToString();
        }

        private void btnCalculatePowerAndRoot_Click(object sender, RoutedEventArgs e)
        {
            double XRaisedToPowerY;
            double XRaisedToPowerOneDividedByY;
            double XRaisedToPowerMinusY;
            double XRaisedToPowerMinusOneDividedByY;

            double YRootOfX;
            double OneDividedByYRootOfX;
            double MinusYRootOfX;
            double OneDividedByMinusYRootOfX;

            double RadixNumber;
            double PowerRootNumber;
            
            RadixNumber = Convert.ToDouble(txtRadixNumber.Text);
            PowerRootNumber = Convert.ToDouble(txtPowerRootNumber.Text);

            XRaisedToPowerY = Math.Pow(RadixNumber, PowerRootNumber);
            XRaisedToPowerOneDividedByY = Math.Pow(RadixNumber, 1/PowerRootNumber);
            XRaisedToPowerMinusY = Math.Pow(RadixNumber, -PowerRootNumber);
            XRaisedToPowerMinusOneDividedByY = Math.Pow(RadixNumber, -1/PowerRootNumber);

            YRootOfX = Math.Pow(RadixNumber, 1/PowerRootNumber);
            OneDividedByYRootOfX = Math.Pow(RadixNumber, PowerRootNumber);
            MinusYRootOfX = 1 / Math.Pow(RadixNumber, 1 / PowerRootNumber);
            OneDividedByMinusYRootOfX = 1 / Math.Pow(RadixNumber, PowerRootNumber);

            txtRadixNumberInPower.Text = XRaisedToPowerY.ToString();
            txtRadixNumberInDividedPower.Text = XRaisedToPowerOneDividedByY.ToString();
            txtRadixNumberInMinusPower.Text = XRaisedToPowerMinusY.ToString();
            txtRadixNumberInMinusDividedPower.Text = XRaisedToPowerMinusOneDividedByY.ToString();

            txtRadixNumberInRoot.Text = YRootOfX.ToString();
            txtRadixNumberInDividedRoot.Text = OneDividedByYRootOfX.ToString();
            txtRadixNumberInMinusRoot.Text = MinusYRootOfX.ToString();
            txtRadixNumberInMinusDividedRoot.Text = OneDividedByMinusYRootOfX.ToString();

            lblRadixNumberInPower.Content = RadixNumber.ToString() + " opløftet i " +
                                            PowerRootNumber.ToString() + " potens : ";
            lblRadixNumberInDividedPower.Content = RadixNumber.ToString() + " opløftet i 1/" +
                                                   PowerRootNumber.ToString() + " potens : ";
            lblRadixNumberInMinusPower.Content = RadixNumber.ToString() +  " opløftet i -" +
                                                 PowerRootNumber.ToString() + " potens : ";
            lblRadixNumberInMinusDividedPower.Content = RadixNumber.ToString() + " opløftet i -1/" +
                                                        PowerRootNumber.ToString() + " potens : ";

            lblRadixNumberInRoot.Content = PowerRootNumber.ToString() + "'rod af " +
                                           RadixNumber.ToString() + " : ";
            lblRadixNumberInDividedRoot.Content = "1/" + PowerRootNumber.ToString() + "'rod af " +
                                           RadixNumber.ToString() + " : ";
            lblRadixNumberInMinusRoot.Content = "-" + PowerRootNumber.ToString() + "'rod af " +
                                           RadixNumber.ToString() + " : ";
            lblRadixNumberInMinusDividedRoot.Content = "-1/" + PowerRootNumber.ToString() + "'rod af " +
                                           RadixNumber.ToString() + " : ";
        }

        private void btnClearPowerAndRootParameters_Click(object sender, RoutedEventArgs e)
        {
            txtRadixNumber.Text = "";
            txtPowerRootNumber.Text = "";

            txtRadixNumberInPower.Text = "";
            txtRadixNumberInDividedPower.Text = "";
            txtRadixNumberInMinusPower.Text = "";
            txtRadixNumberInMinusDividedPower.Text = "";

            txtRadixNumberInRoot.Text = "";
            txtRadixNumberInDividedRoot.Text = "";
            txtRadixNumberInMinusRoot.Text = "";
            txtRadixNumberInMinusDividedRoot.Text = "";

            lblRadixNumberInPower.Content = "x opløftet i y potens : ";
            lblRadixNumberInDividedPower.Content = "x opløftet i 1/y potens : ";
            lblRadixNumberInMinusPower.Content = "x opløftet i -y potens : ";
            lblRadixNumberInMinusDividedPower.Content = "x opløftet i -1/y potens : ";

            lblRadixNumberInRoot.Content = "y'rod af x : ";
            lblRadixNumberInDividedRoot.Content = "1/y'rod af x : ";
            lblRadixNumberInMinusRoot.Content = "-y'rod af x : ";
            lblRadixNumberInMinusDividedRoot.Content = "y'rod af x : ";
        }
    }
}
