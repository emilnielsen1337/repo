﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regnestykke_generator
{
    class Program
    {
        static void Main(string[] args)
        {

            Random numberGenerator = new Random();
            int point = 0;



            while (true)
            {
                var textcolor = ConsoleColor.Green;
                var textcolor2 = ConsoleColor.Red;
                var textcolor3 = ConsoleColor.Yellow;
                var textcolor4 = ConsoleColor.Blue;

                Console.BackgroundColor = textcolor4;




                int num01 = numberGenerator.Next(1, 11);
                int num02 = numberGenerator.Next(1, 11);
                int svar = num01 * num02;

                Console.WriteLine("Hvad er " + num01 + " * " + num02 + "?");

                Console.ResetColor();
                int answer = Convert.ToInt32(Console.ReadLine());
                

                if (answer == num01 * num02)
                {
                    int responseIndex = numberGenerator.Next(1, 4);
                    point++;


                    switch (responseIndex)
                    {
                        case 1:
                            Console.ForegroundColor = textcolor2;
                            Console.BackgroundColor = textcolor;
                            Console.WriteLine("Rigtigt, godt gået! Du har nu " + point + " points!");
                            Console.ResetColor();
                            break;
                        case 2:
                            Console.ForegroundColor = textcolor2;
                            Console.BackgroundColor = textcolor;
                            Console.WriteLine("Korrekt! Du har nu " + point + " points!");
                            Console.ResetColor();
                            break;
                        default:
                            Console.ForegroundColor = textcolor2;
                            Console.BackgroundColor = textcolor;
                            Console.WriteLine("Korrekt! Det rigtige svar er " + svar + " Og du har nu " + point + "point!");
                            Console.ResetColor();
                            break;
                    }
                }
                else
                {
                    int forskel = Math.Abs(answer - (num01 * num02));
                    if (forskel == 1)
                    {
                        Console.BackgroundColor = textcolor2;
                        Console.WriteLine("Dit svar var næsten rigtigt! " + "Resultatet var: " + svar + "Du fik " + point + " points");
                        Console.ResetColor();

                    }
                    else if (forskel <= 10)
                    {
                        Console.BackgroundColor = textcolor2;
                        Console.WriteLine("Det kan du gøre bedere!" + "Resultatet var " + svar + "Du fik " + point + " points");
                        Console.ResetColor();

                    }
                    else
                    {
                        Console.BackgroundColor = textcolor2;
                        Console.WriteLine("Prøver du overhovedet? " + "Resultatet var " + svar + "Du fik " + point + " points");
                        Console.ResetColor();
                        Console.WriteLine("");
                    }
                    Console.ReadKey();
                    Environment.Exit(0);

                }
            }


        }
    }
}