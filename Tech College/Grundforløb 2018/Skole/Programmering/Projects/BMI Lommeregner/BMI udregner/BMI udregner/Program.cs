﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {

                    var textcolor4 = ConsoleColor.Red;
                    var textcolor = ConsoleColor.Cyan;
                    var textcolor2 = ConsoleColor.Yellow;
                    var textcolor3 = ConsoleColor.Blue;
                    var textcolor5 = ConsoleColor.Green;

                    Console.ForegroundColor = textcolor4;
                    Console.WriteLine("BMI Lommeregner");
                    Console.WriteLine("----------------");
                    Console.WriteLine();

                    Console.ForegroundColor = textcolor;
                    Console.Write("Vægt i kg: ");
                    int kg;




                    Console.ResetColor();
                    kg = Convert.ToInt32(Console.ReadLine());

                    Console.ForegroundColor = textcolor2;
                    Console.Write("Højde i cm: ");
                    int m;

                    Console.ResetColor();
                    m = Convert.ToInt32(Console.ReadLine());

                    Console.ForegroundColor = textcolor3;
                    Console.Write("Køn (m/k)?: ");

                    Console.ResetColor();
                    string Gender = Console.ReadLine();

                    double BMI = kg / ((m / 100.0) * (m / 100.0));
                    
                    Console.ForegroundColor = textcolor5;
                    if (BMI < 19 & Gender == "k")
                    { Console.WriteLine("->  Undervægtig"); }
                    if (BMI >= 19 & BMI <= 24 & Gender == "k")
                    { Console.WriteLine("-> Normal"); }
                    if (BMI > 24 & Gender == "k")
                    { Console.WriteLine("-> Overvægtig"); }

                    Console.ForegroundColor = textcolor5;
                    if (BMI < 20 & Gender == "m")
                    { Console.WriteLine("-> Undervægtig"); }
                    if (BMI >= 20 & BMI <= 25 & Gender == "m")
                    { Console.WriteLine("-> Normal"); }
                    if (BMI > 25 & Gender == "m")
                    { Console.WriteLine("-> Overvægtig"); }

                }

                catch
                {
                    var textcolor6 = ConsoleColor.DarkCyan;

                    Console.ForegroundColor = textcolor6;
                    Console.WriteLine("Tag dig sammen! Du kan ikke indtaste bogstaver i det her program!");
                    Console.ReadLine();
                    Console.Clear();

                }
            }
        }
    }
}
            

                
            
        

    

