﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regnestykke_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Random numberGenerator = new Random();

            int num01 = numberGenerator.Next(1, 11);
            int num02 = numberGenerator.Next(1, 11);
            int svar = num01 * num02;

            Console.WriteLine("Hvad er " + num01 + " * " + num02 + "?");

            int answer = Convert.ToInt32(Console.ReadLine());

            if (answer == num01 * num02)
            {
                int responseIndex = numberGenerator.Next(1, 4);

                switch (responseIndex)
                {
                    case 1:
                        Console.WriteLine("Rigtigt, godt gået!");
                        break;
                    case 2:
                        Console.WriteLine("Korrekt!");
                        break;
                    default:
                        Console.WriteLine("Korrekt! Det rigtige svar er " + svar);
                        break;
                }
            }
            else
            {
                int forskel = Math.Abs(answer - (num01 * num02));
                if (forskel == 1)
                {
                    Console.WriteLine("Tæt på!");
                }
                else if (forskel <= 10)
                {
                    Console.WriteLine("Det kan du gøre bedere!");
                }
                else
                {
                    Console.WriteLine("Prøver du overhovedet?");
                }
            }
            Console.ReadKey();

        }
    }
}