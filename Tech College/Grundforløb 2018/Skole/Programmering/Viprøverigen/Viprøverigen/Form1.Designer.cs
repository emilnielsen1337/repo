﻿namespace Viprøverigen
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label LabelTemp;
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rødPic = new System.Windows.Forms.PictureBox();
            this.gulPic = new System.Windows.Forms.PictureBox();
            this.grønPic = new System.Windows.Forms.PictureBox();
            LabelTemp = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rødPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gulPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grønPic)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelTemp
            // 
            LabelTemp.AutoSize = true;
            LabelTemp.Location = new System.Drawing.Point(162, 333);
            LabelTemp.Name = "LabelTemp";
            LabelTemp.Size = new System.Drawing.Size(0, 13);
            LabelTemp.TabIndex = 1;
            // 
            // serialPort1
            // 
            this.serialPort1.PortName = "COM3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(293, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // rødPic
            // 
            this.rødPic.BackColor = System.Drawing.Color.Red;
            this.rødPic.Location = new System.Drawing.Point(359, 12);
            this.rødPic.Name = "rødPic";
            this.rødPic.Size = new System.Drawing.Size(23, 22);
            this.rødPic.TabIndex = 6;
            this.rødPic.TabStop = false;
            // 
            // gulPic
            // 
            this.gulPic.BackColor = System.Drawing.Color.Yellow;
            this.gulPic.Location = new System.Drawing.Point(359, 12);
            this.gulPic.Name = "gulPic";
            this.gulPic.Size = new System.Drawing.Size(23, 21);
            this.gulPic.TabIndex = 7;
            this.gulPic.TabStop = false;
            // 
            // grønPic
            // 
            this.grønPic.BackColor = System.Drawing.Color.Lime;
            this.grønPic.Location = new System.Drawing.Point(359, 12);
            this.grønPic.Name = "grønPic";
            this.grønPic.Size = new System.Drawing.Size(23, 21);
            this.grønPic.TabIndex = 8;
            this.grønPic.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 377);
            this.Controls.Add(this.grønPic);
            this.Controls.Add(this.gulPic);
            this.Controls.Add(this.rødPic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(LabelTemp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rødPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gulPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grønPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.PictureBox rødPic;
        public System.Windows.Forms.PictureBox gulPic;
        public System.Windows.Forms.PictureBox grønPic;
        public System.Windows.Forms.Label label2;
    }
}

