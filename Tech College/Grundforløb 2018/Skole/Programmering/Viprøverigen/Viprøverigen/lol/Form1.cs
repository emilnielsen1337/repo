﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Globalization;

namespace Viprøverigen
{
    public partial class Form1 : Form
    {
        public SerialPort SP { get; }

        public Form1()
        {
            InitializeComponent();
            SP = new SerialPort();
            SP.PortName = "COM3";
            SP.BaudRate = 9600;
            SP.WriteTimeout = 2000;
            SP.Open();
        }

        public void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text1 = SP.ReadLine();
            int.TryParse(text1, out int intTemp);

            while (true)
            {
                Application.DoEvents();
                label2.Text = SP.ReadLine();

                if (intTemp < 20)
                {
                    rødPic.Enabled = false;
                    gulPic.Enabled = false;
                    grønPic.Enabled = true;

                }
                else if (intTemp > 20 && intTemp < 24)
                {
                    rødPic.Enabled = false;
                    gulPic.Enabled = true;
                    grønPic.Enabled = false;
                }
                else if (intTemp > 24)
                {
                    rødPic.Enabled{set false};
                    gulPic.
                    gulPic.Enabled = false;
                    grønPic.Enabled = false;
                    
                }
            }
        }
    }
}
   

