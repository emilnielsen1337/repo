float temp;      //En variable som kan indeholde den læste temperatur
int tempPin = 1;    //En variable ved navn temPin som sidder på port 1 på boardet
unsigned long time_now;
float period = 1000;
void setup()
{
  Serial.begin(9600);   //Sætter BAUD raten til 9600
}
void loop()
{
  time_now = millis();
  temp = analogRead(tempPin); //Giver variablen temp, den læste værdi fra LM35’eren
  temp = temp * 0.48828125;   //Ganger værdien med 0.48828125
  while (millis() < time_now + period) {
  Serial.println(temp);   //Skriver værdien af temp på skærmen
  //delay(50);      //Forsinkelse på 1 sekund
  }
}

