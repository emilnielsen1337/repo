#include <EEPROM.h>
#include <SPI.h>
#include <MFRC522.h>

int timeDelay = 600;

const int ledRed = 8;
const int ledYellow = 7;
const int ledGreen = 4;

constexpr uint8_t relay = 4;
constexpr uint8_t wipeB = 3;

bool programMode = false;

uint8_t successRead;

byte storedCard[4];
byte readCard[4];
byte masterCard[4];

constexpr uint8_t RST_PIN = 9;
constexpr uint8_t SS_PIN = 10;

MFRC522 mfrc522(SS_PIN, RST_PIN);

void setup() {
  pinMode(ledRed, OUTPUT);
  pinMode(ledYellow, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  Serial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();

  if (EEPROM.read(1) != 143) {
    Serial.println(F("No Master Card Defined"));
    Serial.println(F("Scan A PICC to Define as Master Card"));
    do {
      successRead = getID();
    }
    while (!successRead);
    for ( uint8_t j = 0; j < 4; j++ ) {
      EEPROM.write( 2 + j, readCard[j] );
    }
    EEPROM.write(1, 143);
    Serial.println(F("Master Card Defined"));
  }
  Serial.print(F("Master Card's UID: "));
  for ( uint8_t i = 0; i < 4; i++ ) {
    masterCard[i] = EEPROM.read(2 + i);
    Serial.print(masterCard[i], HEX);
  }
  Serial.println("");
  Serial.println(F("-------------------"));
  Serial.println(F("Everything is ready"));
  Serial.println(F("Waiting for RFID to be scanned"));
  Serial.println();
  Blink();
}

void loop () {
  do {
    successRead = getID();
  } while (!successRead);
  if (programMode) {
    if ( isMaster(readCard) ) {
      Serial.println(F("Master card scanned"));
      Serial.println(F("Exiting Edit Mode"));
      Serial.println(F("-----------------------------"));
      Serial.println();
      masterLed();
      programMode = false;
      return;
    }
    else {
      if ( findID(readCard) ) {
        Serial.println(F("RFID card already added, removing card... "));
        digitalWrite(ledRed, HIGH);
        deleteID(readCard);
        digitalWrite(ledRed, LOW);
        Serial.println("-----------------------------");
        Serial.println(F("Scan a RFID card to ADD or REMOVE"));
        Serial.println();
        delay(2000);
      }
      else {
        Serial.println(F("New RFID card detected, adding..."));
        digitalWrite(ledGreen, HIGH);
        writeID(readCard);
        digitalWrite(ledGreen, LOW);
        Serial.println(F("-----------------------------"));
        Serial.println(F("Scan a RFID card to ADD or REMOVE"));
        Serial.println();
        delay(2000);
      }
    }
  }
  else {
    if ( isMaster(readCard)) {
      programMode = true;
      Serial.println(F("Master card scanned, entering Edit Mode"));
      masterLed();
      Serial.println();
      uint8_t count = EEPROM.read(0);
      Serial.print(F("I have "));
      Serial.print(count);
      Serial.print(F(" record(s) on EEPROM"));
      Serial.println("");
      Serial.println(F("Scan a RFID card to ADD or REMOVE"));
      Serial.println(F("Scan Master Card again to Exit Edit Mode"));
      Serial.println(F("-----------------------------"));
      Serial.println();
      delay(2000);
    }
    else {
      if ( findID(readCard) ) {
        Serial.println(F("RFID card accepted. Welcome!"));
        Serial.println();
        cardAccepted();
      }
      else {
        Serial.println(F("RFID card denied. No access."));
        Serial.println();
        Error();
      }
    }
  }
}

uint8_t getID() {
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }
  Serial.print(F("RFID card UID: "));
  for ( uint8_t i = 0; i < 4; i++) {
    readCard[i] = mfrc522.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  Serial.println("");
  mfrc522.PICC_HaltA();
  return 1;
}
void readID( uint8_t number ) {
  uint8_t start = (number * 4 ) + 2;
  for ( uint8_t i = 0; i < 4; i++ ) {
    storedCard[i] = EEPROM.read(start + i);
  }
}

void writeID( byte a[] ) {
  if ( !findID( a ) ) {
    uint8_t num = EEPROM.read(0);
    uint8_t start = ( num * 4 ) + 6;
    num++;
    EEPROM.write( 0, num );
    for ( uint8_t j = 0; j < 4; j++ ) {
      EEPROM.write( start + j, a[j] );
    }
    Serial.println(F("Succesfully added RFID card"));
    cardAccepted();
  }
  else {
    Serial.println(F("Failed! There is something wrong with RFID or bad EEPROM"));
    Error();
  }
}

void deleteID( byte a[] ) {
  if ( !findID( a ) ) {
    Serial.println(F("Failed! There is something wrong with RFID or bad EEPROM"));
    Error();
  }
  else {
    uint8_t num = EEPROM.read(0);
    uint8_t slot;
    uint8_t start;
    uint8_t looping;
    uint8_t j;
    uint8_t count = EEPROM.read(0);
    slot = findIDSLOT( a );
    start = (slot * 4) + 2;
    looping = ((num - slot) * 4);
    num--;
    EEPROM.write( 0, num );
    for ( j = 0; j < looping; j++ ) {
      EEPROM.write( start + j, EEPROM.read(start + 4 + j));
    }
    for ( uint8_t k = 0; k < 4; k++ ) {
      EEPROM.write( start + j + k, 0);
    }
    Serial.println(F("Succesfully removed RFID."));
    Error();
  }
}

bool checkTwo ( byte a[], byte b[] ) {
  for ( uint8_t k = 0; k < 4; k++ ) {
    if ( a[k] != b[k] ) {
       return false;
    }
  }
  return true;
}

uint8_t findIDSLOT( byte find[] ) {
  uint8_t count = EEPROM.read(0);
  for ( uint8_t i = 1; i <= count; i++ ) {
    readID(i);
    if ( checkTwo( find, storedCard ) ) {
      return i;
    }
  }
}

bool findID( byte find[] ) {
  uint8_t count = EEPROM.read(0);
  for ( uint8_t i = 1; i < count; i++ ) {
    readID(i);
    if ( checkTwo( find, storedCard ) ) {
      return true;
    }
    else {
    }
  }
  return false;
}

bool isMaster( byte test[] ) {
  return checkTwo(test, masterCard);
}

bool monitorWipeButton(uint32_t interval) {
  uint32_t now = (uint32_t)millis();
  while ((uint32_t)millis() - now < interval)  {
    // check on every half a second
    if (((uint32_t)millis() % 500) == 0) {
      if (digitalRead(wipeB) != LOW)
        return false;
    }
  }
  return true;
}

void Blink() {
  digitalWrite(ledRed, HIGH);
  digitalWrite(ledYellow, HIGH);
  digitalWrite(ledGreen, HIGH);
  delay(1000);
  digitalWrite(ledRed, LOW);
  digitalWrite(ledYellow, LOW);
  digitalWrite(ledGreen, LOW);

}

void masterLed() {
  digitalWrite(ledGreen, HIGH);
  digitalWrite(ledYellow, HIGH);
  delay(timeDelay);
  digitalWrite(ledGreen, LOW);
  digitalWrite(ledYellow, LOW);
  }

void cardAccepted() {
  digitalWrite(ledGreen, HIGH);
  delay(timeDelay);
  digitalWrite(ledGreen, LOW);
}

void Error() {
  digitalWrite(ledRed, HIGH);
  delay(timeDelay);
  digitalWrite(ledRed, LOW);
}

