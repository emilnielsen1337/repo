﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // denne command er for kunne læse/skrive dokumenter 
using System.IO.Ports; // denne command er for kunne bruge porte

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        static SerialPort Sp;

        string vinnavn;// vin navn bliver gemt i denne variable
        int årstal;   // årstal bliver gemt i denne variable
        int antal; // antal bliver gemt i denne variable
        const string hylde1 = @"C:\Lager\hylde1.txt"; // data på hylde 1 bliver gemt i denne string/variable
        const string hylde2 = @"C:\Lager\hylde2.txt";// data på hylde 2 bliver gemt i denne string/variable
        const string templog = @"C:\Lager\Temperatur Log.txt"; // data fra temploggen bliver gemt her 
       
        
        public Form2() // denne del er hele forms vinduet alt hvad der skal køre uden tryk på knapper osv skal skrives her 
        {
            
            InitializeComponent();

            Sp = new SerialPort(); //denne del af koden er for skabe fobindelse med arduino 
            Sp.PortName = "com4"; // dette er for sætte hvilken port arduinoen er connected til
            Sp.BaudRate = 9600; // dette er refresh rate
            Sp.ReadTimeout = 500; // dette er en delay
            Sp.Open(); // i dunno 


          
            using (StreamReader read = new StreamReader(hylde2)) // denne command er for at kunne læse linjer fra text dokumentet "hylde2"
            using (StreamReader read2 = new StreamReader(hylde1)) // denne command er for kunne læse linjer fra text dokumentet "hylde1"
            using (StreamReader read3 = new StreamReader(templog)) // denne command er for kunne læse linjer fra "templog"

            {
                string line; // denne variable gemmer Linjer fra text filen "hylde2"
                string line2; // denne variable gemmer linjer fra text filen "hylde1"
                string line3;
                while ((line = read.ReadLine()) != null) // mens line variablen er = læse commandoen "read" og der er data(!= null) så køren den koden i {}
                {
                    Hylde_2.Items.Add(line); // denne kode tilføjer en linje fra txt filen "hylde2" til listbox2
                }
                while ((line2 = read2.ReadLine()) != null) // mens line variablen er = læse commandoen "read" og der er data(!= null) så køren den koden i {}
                {
                    Hylde_1.Items.Add(line2); // denne kode tilføjer en linje fra txt filen "hylde1" til listbox1
                }
                while ((line3 = read3.ReadLine()) != null) // mens line variablen er = læse commandoen "read" og der er data(!= null) så køren den koden i {}
                {
                    listBox3.Items.Add(line3); // denne kode tilføjer en linje fra txt filen "hylde2" til listbox2
                }
            }
        }

        private void button1_Click(object sender, EventArgs e) // denne del af koden bliver aktiveret når du trykker på "tilføj vin"
        {

            vinnavn = textBox1.Text; // denne kode sætter vinnavn variablen til hvad du skriver i textbox1
            årstal = Convert.ToInt16(textBox2.Text); // denne kode sætter årstal variablen til hvad du skriver i textbox2
            antal = Convert.ToInt16(textBox3.Text); // denne kode sætter antal variablen til hvad du skriver i textbox3
           
           

            if (årstal >= 2000) // vis årstal er højre eller lig med 2000, tilføj til listbox1
            {

                Hylde_1.Items.Add("navn - " + vinnavn + "- årstal - " + årstal + " - Antal - " + antal);

            }
            else // vis årstal er under 2000 tilføj til listbox2
            {
                Hylde_2.Items.Add("navn - " + vinnavn + "- årstal - " + årstal + " - Antal - " + antal);
            }

        }

        private void button2_Click(object sender, EventArgs e) // når du kliker på "gem data" køre den koden neden under
        {

            
           
            System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(hylde1); // her skaber vi en save metode savefile er navnet på variablen du bruger for til gemme med
            foreach (var item in Hylde_1.Items) // for være item på hylde1 lave koden neden under
            {
                
                SaveFile.WriteLine(item); // denne del skriver en linje i dit txt
            }
            System.IO.StreamWriter SaveFile_ = new System.IO.StreamWriter(hylde2);
            foreach (var item in Hylde_2.Items)
            {
                
                SaveFile_.WriteLine(item);
            }


            System.IO.StreamWriter SaveFile__ = new System.IO.StreamWriter(templog);
            foreach (var item in listBox3.Items)
            {

                SaveFile__.WriteLine(item);
            }


            SaveFile.Close(); // her afslutter du gemme funktionen
            SaveFile_.Close();
            SaveFile__.Close();

            MessageBox.Show("Programs saved!"); // viser en messagebox vis programmet er gemt
        }

        private void button3_Click(object sender, EventArgs e) // når du kliker på "fjern vin" kanppen køre du koden neden under
        {
            int selectedIndex1 = Hylde_1.SelectedIndex; // int selectedindex1 er lige med den vin du har valgt på listen
            if (selectedIndex1 >= 0) // vis vin valgt er større end eller lig med 0 køre kode neden under
            {
                Hylde_1.Items.RemoveAt(selectedIndex1); // fjern selected vin
            }

               
            int selectedIndex = Hylde_2.SelectedIndex;
            if (selectedIndex >= 0)
              
            {
                Hylde_2.Items.RemoveAt(selectedIndex);
            }

            MessageBox.Show("the selected items has been cleared"); // messagebox viser vis du har gemt 
        }

        private void button4_Click(object sender, EventArgs e) // denne del af koden køre når du trykker på "refresh temp" knappen
        {
          
            textBox4.Text = Sp.ReadLine(); // sætter textbox til det som arduinoen sender 
            listBox3.Items.Add(textBox4.Text + " - " + DateTime.Now); // tilføjer en linje i listbox3 med temperaturen fra arduinoen og dato + tid 
            label6.Text = "Klik for at få en ny måling"; // skifter label text ved tryk på knappe 

        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox3.Items.Clear();
        }

        private void Hylde_1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
