﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] Brugernavn = { "Admin", "Admin2" };
            string[] Kode = { "Gvh32ret", "kode123" };
            string text1;
            string text2;
            text1 = textBox1.Text;
            text2 = textBox2.Text;
            if (text1 == Brugernavn[0] && text2 == Kode[0])
            {

                Form2 op = new Form2(); // form2 er det næste vindue der åbner når koden og bruger er tastet ind
                op.Show();
                this.Hide();
                op.FormClosing += window_closing;
            }
            else
            {
                if (text1 == Brugernavn[1] && text2 == Kode[1])
                {
                    Form2 op = new Form2(); // form2 er det næste vindue der åbner når koden og bruger er tastet ind
                    op.Show();
                    this.Hide();
                    op.FormClosing += window_closing;
                }
                else
                {
                    Environment.Exit(0);
                }
            }
                
            


        
        }
        private void window_closing(object sender, FormClosingEventArgs e) // når du lukker det vindue der kommer efter du loger ind, åbner den det forrige vindue
        {
            this.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
